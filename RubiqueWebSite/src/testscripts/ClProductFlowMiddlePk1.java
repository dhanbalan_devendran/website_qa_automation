package testscripts;

import functions.ClFunctions;

public class ClProductFlowMiddlePk1 extends ClFunctions {

	public static void main(String[] args) throws Exception {
		ClProductFlowMiddlePk1 t1 = new ClProductFlowMiddlePk1();
		driver = t1.fn_setUpChrome();
		t1.fn_openUrl("https://beta.rubique.com/");

		// LOGIN
		t1.fn_homeLoginClick("#log-btn-header");
		Thread.sleep(2000);
		t1.fn_emailId("#login-email", "praveen.kumar@rubique.com");
		Thread.sleep(1000);
		t1.fn_password("#login-pwd", "praveen@123");
		Thread.sleep(1000);
		t1.fn_login("#login-button");
		// t1.fn_login("#login-button");
		Thread.sleep(8000);
		// @SLECTED CAR LOAN FROM MIDDLE OF THE WEB
		t1.fn_selectCarLoan(".col-lg-7 > ul:nth-child(2) > li:nth-child(4) > a:nth-child(1) > img:nth-child(1)");
		// @BASIC INFO
		Thread.sleep(1000);
		t1.fn_completeFirstStepLoanAmount("#visible-loan-amount", "150000");
		t1.fn_completeFirstStepEmploymentType("label.btn:nth-child(3)");
		t1.fn_completeFirstStepCompany_Organization("#current-company", "TATA AIG");
		t1.fn_completeFirstStepMonthly_Income("#visible-monthly-income", "80000");
		t1.fn_completeFirstStepTenure("input.fieldNumber:nth-child(1)", "5");
		t1.fn_completeFirstStepNext("#search-step-one-submit-button");
		t1.fn_selectSecondStagemobile("input[id='5'][column-name='phone']", "8600741820");
		t1.fn_selectSecondStageEmailId("input[id='6'][column-name='email']", "praveen.kumar@rubique.com");
		t1.fn_selectSecondStageNext("#form_group_3 > div:nth-child(3) > button:nth-child(2)");
		// @Product Match LIST AS PER BASIC INFO

		t1.fn_completeFirstStepProductMatch(
				"tr.rowShadow:nth-child(1) > td:nth-child(5) > div:nth-child(1) > button:nth-child(1)");
		// Contact_information click continue button
		Thread.sleep(2000);
		t1.fn_completeSecondStepFullName("input[id='2'][name='2']", "testerpk");
		t1.fn_stepTwoSelectGenderId("#form_group_4 > div:nth-child(2) > div:nth-child(1) > select:nth-child(2)",
				"Male");
		t1.fn_stepTwoSelectDOB("#fieldNumber8", "1990-06-09");
		t1.fn_stepTwoSelectMarital("#form_group_4 > div:nth-child(4) > div:nth-child(1) > select:nth-child(2)",
				"Single");
		t1.fn_stepTwoSelectNationality("#form_group_4 > div:nth-child(5) > div:nth-child(1) > select:nth-child(2)",
				"INDIAN");

		t1.fn_completeSecondStagePan("input[column-name='pan_id'][maxlength='10']", "DTUPK6334K");
		t1.fn_stepTwoSelectAddress_Line_1("input[column-name='current_address.address1'][maxlength='30']", "test");
		t1.fn_stepTwoSelectAddress_Line_2("input[column-name='current_address.address2'][id='21']", "tester");
		t1.fn_stepTwoSelectCity("#form_group_4 > div:nth-child(12) > div:nth-child(1) > select:nth-child(2)", "Mumbai");
		t1.fn_stepTwoSelectPincode("input[id='23'][column-name='current_address.pincode']", "421503");
		t1.fn_stepTwoSelectYears_at_current_residence("input[id='29'][column-name='residence_year']", "12");
		t1.fn_stepTwoSelectType_of_Accommodation(
				"#form_group_4 > div:nth-child(16) > div:nth-child(1) > select:nth-child(2)", "Owned");
		t1.fn_stepTwoSelectContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[2]");

		Thread.sleep(2000);
		// Loan Details
		t1.fn_completeThirdStageContinue("#form_group_2 > div:nth-child(1)");
		t1.fn_completeThirdStageContinue1("#form_group_2 > div:nth-child(3) > button:nth-child(2)");

		// Applicant Information
		Thread.sleep(2000);
		t1.fn_completeFourthStageDo_you_have_income_proof_document(
				"div.parent-id-38:nth-child(4) > div:nth-child(1) > select:nth-child(2)", "Yes");
		Thread.sleep(1000);
		t1.fn_completeFourthStageNumber_of_Years_in_Current_Work(
				"div.parent-id-38:nth-child(7) > div:nth-child(1) > input:nth-child(3)", "7");
		Thread.sleep(1000);
		t1.fn_completeFourthStageTotal_Number_of_Years_in_Work(
				"div.parent-id-38:nth-child(8) > div:nth-child(1) > input:nth-child(3)", "7");
		Thread.sleep(1000);
		t1.fn_completeFourthStageContinue("div.col-sm-12:nth-child(18) > button:nth-child(2)");
		Thread.sleep(1000);

	}

}
