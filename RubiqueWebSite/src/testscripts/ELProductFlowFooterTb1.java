package testscripts;

/** 
 * Author: Trupti Bhosale
 * This file contains all functions for all fields needed for executing Business loan product flow
 * */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import functions.ELFunctions;


public class ELProductFlowFooterTb1 {
	WebDriver driver;
	String Actualtext;
	ELFunctions objectR=PageFactory.initElements(driver, ELFunctions.class);
	
		
	@Test
	public void fn_viewProductOffers() throws Exception
	{
		objectR.fn_openUrl("https://m.rubique.com");
		Thread.sleep(1000);	
		objectR.fn_loginButton("#log-btn-header");
		Thread.sleep(1000);	
		objectR.fn_LoginEmail("#login-email");
		Thread.sleep(1000);	
		objectR.fn_Password("#login-pwd","praveen@123");
		Thread.sleep(1000);	
		objectR.fn_loginButton("#login-button");
		Thread.sleep(1000);	
		objectR.fn_productLink("(//*[@title='Education Loan'])[2]");
		Thread.sleep(1000);	
		objectR.fn_individualProduct("//*[@class='viewFullDetail purple-btn'][@product-id='1337']");
		Thread.sleep(1000);	
		objectR.fn_checkEligibility(".rb-prdct-s-instant");
		Thread.sleep(1000);	
		objectR.fn_stepOneMobileNumber("input[id='5'][class='form-control value-field custom-class-mobile']","8898988888");
		Thread.sleep(1000);	
		objectR.fn_stepOneLeadEmail("input[id='6']");
		Thread.sleep(1000);	
		objectR.fn_stepTwoSelectCityId("[column-name='current_city_id'][name='25']","Mumbai");
		Thread.sleep(1000);	
		objectR.fn_stepContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[1]");
		Thread.sleep(1000);	
		
		objectR.fn_stepTwoFullName("input[id='2'][name='2']","Education laon user");
		Thread.sleep(1000);	
		objectR.fn_stepTwoDOB("input[id='fieldNumber8'][name='8']","14-05-1992");
		Thread.sleep(1000);	
		objectR.fn_stepTwoSelectGenderId("[class='form-control value-field'][column-name='gender_id'][name='7']","Male");
		Thread.sleep(1000);	
		objectR.fn_stepTwoSelectMaritalStatusId("[class='form-control value-field'][column-name='marital_status_id'][name='9']","Married");
		Thread.sleep(1000);	
		objectR.fn_stepTwoSelectNationality("[class='form-control value-field'][column-name='nationality'][name='12']","INDIAN");
		Thread.sleep(1000);	
		objectR.fn_stepTwoPAN("[column-name='pan_id'][name='15']","AQPPX2345F");
		Thread.sleep(1000);	
		objectR.fn_stepTwoPAN("[column-name='aadhaar_id'][name='17']","asdfgh234566");
		Thread.sleep(1000);	
		objectR.fn_stepTwoAddress("[column-name='current_address.address1'][name='20']","address1");
		Thread.sleep(1000);	
		objectR.fn_stepTwoAddress("[column-name='current_address.address2'][name='21']","address 2");
		Thread.sleep(1000);	
		objectR.fn_stepTwoAddress("[column-name='current_address.address3'][name='22']","address 3");
		Thread.sleep(1000);	
		
		objectR.fn_stepTwoPincode("[column-name='current_address.pincode'][name='23']","414141");
		Thread.sleep(1000);	
		objectR.fn_stepContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[2]");
		Thread.sleep(1000);	
		objectR.fn_stepThreeLoanAmount("[id='115'][column-name='loan_amount'][name='115']","2000000");
		Thread.sleep(1000);	
		objectR.fn_stepThreeTenureYears("#form_group_2 > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > input:nth-child(1)","10");
		Thread.sleep(1000);	
		objectR.fn_stepThreeTenureMonths("#form_group_2 > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > input:nth-child(2)","0");
		Thread.sleep(1000);	
		objectR.fn_stepContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[3]");
		Thread.sleep(1000);	
		objectR.fn_stepFourCompetitiveExamAppeared("[name='649']","Master's Degree");
		Thread.sleep(1000);	
		objectR.fn_stepFourMarks("[id='651'][name='651']","70");
		Thread.sleep(1000);	
		objectR.fn_stepFourCountryStudy("[column-name='country_of_study'][name='657']","India");
		Thread.sleep(1000);	
		objectR.fn_stepFourAdmissionstatus("[column-name='admission_status'][name='653']","Confirmed");
		Thread.sleep(1000);	
		objectR.fn_stepFourCourseName("[column-name='name_of_course'][name='654']","Doctoral Degree");
		Thread.sleep(1000);	
		objectR.fn_stepFourCollegeName("#form_group_201 > div:nth-child(6) > div:nth-child(1) > select:nth-child(2)","Bryn Mawr College");
		Thread.sleep(1000);	
		objectR.fn_stepFourCollegeCity("[name='656'][id='656']","Mumbai");
		Thread.sleep(1000);	
		objectR.fn_stepThreeTenureYears("#form_group_201 > div:nth-child(8) > div:nth-child(1) > div:nth-child(2) > input:nth-child(1)","2");
		Thread.sleep(1000);	
		objectR.fn_stepThreeTenureMonths("#form_group_201 > div:nth-child(8) > div:nth-child(1) > div:nth-child(2) > input:nth-child(2)","0");
		Thread.sleep(1000);	
		objectR.fn_stepFourCourseNature("#form_group_201 > div:nth-child(9) > div:nth-child(1) > select:nth-child(2)","Full Time");
		Thread.sleep(1000);	
		objectR.fn_stepFourTypeCourse("#form_group_201 > div:nth-child(10) > div:nth-child(1) > select:nth-child(2)","MBA");
		Thread.sleep(1000);	
		objectR.fn_stepOneEmail("input[id='661'][name='661']");
		Thread.sleep(1000);	
		objectR.fn_stepThreeLoanAmount("input[id='662'][name='662']","2000000");
		Thread.sleep(1000);	
		objectR.fn_stepThreeLoanAmount("input[id='663'][name='663']","1500000");
		Thread.sleep(1000);	
		objectR.fn_stepContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[4]");
		Thread.sleep(1000);	
		objectR.fn_YesNo("#form_group_46 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Yes");
		Thread.sleep(1000);	
		objectR.fn_stepFifthSecurityOffered(".parent-id-668 > div:nth-child(1) > select:nth-child(2)","Plot");
		Thread.sleep(1000);	
		objectR.fn_stepContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[5]");
		Thread.sleep(1000);	
		objectR.fn_stepSixthOccupation("#form_group_5 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Salaried");
		Thread.sleep(1000);	
		objectR.fn_stepSixthCompanyName("#fieldNumber40","TATA ADVANCED MATERIAL PRIVATE LIMITED");
		Thread.sleep(1000);	
		objectR.fn_stepsixthgrossMonthlyIncome("div.parent-id-38:nth-child(3) > div:nth-child(1) > input:nth-child(2)","50000");
		Thread.sleep(1000);	
		objectR.fn_stepThreeTenureYears("div.parent-id-38:nth-child(4) > div:nth-child(1) > input:nth-child(2)","11");
		Thread.sleep(1000);	
		//objectR.fn_stepSixthCompanyName("div.parent-id-38:nth-child(5) > div:nth-child(1) > input:nth-child(3)","TATA ADVANCED MATERIAL PRIVATE LIMITED");
		//Thread.sleep(1000);	
		//objectR.fn_stepsixthgrossMonthlyIncome("input[id='44'][name='44']","50000");
		Thread.sleep(1000);	
	//	objectR.fn_stepThreeTenureYears("input[id='62'][name='62']","11");
		//Thread.sleep(1000);	
	//	objectR.fn_stepSixthCompanyName("div.parent-id-38:nth-child(8) > div:nth-child(1) > input:nth-child(3)","TATA ADVANCED MATERIAL PRIVATE LIMITED");
		//Thread.sleep(1000);	
	//	objectR.fn_stepsixthgrossMonthlyIncome("input[id='42'][name='42']","50000");
	//	Thread.sleep(1000);	
		objectR.fn_stepContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[6]");
		Thread.sleep(1000);	
		objectR.fn_YesNo("#form_group_10 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Yes");
		Thread.sleep(1000);	
		objectR.fn_stepSevenExistingEMI("input[id='90'][name='90']","2500");
		Thread.sleep(1000);	
		objectR.fn_stepContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[7]");
		Thread.sleep(1000);	
		objectR.fn_YesNo("#form_group_11 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Yes");
		Thread.sleep(1000);	
		objectR.fn_stepTwoFullName("input[id='98'][name='98']","co applicant");
		Thread.sleep(1000);	
		objectR.fn_stepEightcoApplicantRelationship("div.parent-id-96:nth-child(3) > div:nth-child(1) > select:nth-child(2)","Father");
		Thread.sleep(1000);	
		objectR.fn_stepTwoDOB("#fieldNumber99","14-09-1955");
		Thread.sleep(1000);	
		Thread.sleep(1000);	
		objectR.fn_stepOneEmail("input[id='100'][name='100']");
		Thread.sleep(1000);	
		objectR.fn_stepOneMobileNumber("input[id='101'][name='101']","8888888888");
		Thread.sleep(1000);	
		objectR.fn_stepTwoSelectGenderId("div.parent-id-96:nth-child(7) > div:nth-child(1) > select:nth-child(2)","Male");
		Thread.sleep(1000);	
		objectR.fn_stepTwoPAN("input[id='241'][name='241']","AXCCP1234F");
		Thread.sleep(1000);	
		objectR.fn_stepSixthOccupation("div.parent-id-96:nth-child(9) > div:nth-child(1) > select:nth-child(2)","Salaried");
		Thread.sleep(1000);	
		objectR.fn_stepsixthgrossMonthlyIncome("input[id='103'][name='103']","55000");
		Thread.sleep(1000);	
		objectR.fn_YesNo("div.parent-id-96:nth-child(11) > div:nth-child(1) > select:nth-child(2)","Yes");
		Thread.sleep(1000);	
		objectR.fn_stepTwoSelectCityId("div.formOneBlock:nth-child(13) > div:nth-child(1) > select:nth-child(2)","Mumbai");
		Thread.sleep(1000);	
		objectR.fn_stepSevenExistingEMI("input[id='108'][name='108']","2500");
		Thread.sleep(1000);	
		objectR.fn_stepContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[8]");
		Thread.sleep(1000);	
		objectR.fn_stepNinePrimaryExistingBank("#form_group_6 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","ICICI Bank");
		Thread.sleep(1000);	
	//	objectR.fn_dbVerificationcode("jdbc:mysql://jun20.cib48esswkls.ap-south-1.rds.amazonaws.com");
	//	Thread.sleep(1000);	
		objectR.fn_stepContinueButton("//*[@name='submit' and @class='hex-hor-button submit_application_button pull-right']");

	
		
		
	}	
			
	
	
	
	

}
