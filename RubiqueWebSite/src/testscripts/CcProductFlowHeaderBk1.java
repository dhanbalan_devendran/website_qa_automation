package testscripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import functions.CcFunctions;

public class CcProductFlowHeaderBk1 extends CcFunctions {
	public static WebDriver driver;

	public static void main(String[] args) throws Exception {
		CcProductFlowHeaderBk1 t1 = new CcProductFlowHeaderBk1();
		driver = t1.fn_setUpChrome();
		t1.fn_openUrl("https://m.rubique.com/");
		Thread.sleep(1000);
		t1.fn_homeLoginClick("#log-btn-header");
		Thread.sleep(1500);
		t1.fn_emailId("#login-email", "praveen.kumar@rubique.com");
		Thread.sleep(1500);
		t1.fn_password("#login-pwd", "praveen@123");
		Thread.sleep(1500);
		t1.fn_login("button[id=\"login-button\"][type=\"button\"]");
		
		Thread.sleep(7000);
		t1.fn_Product("(//span[@class='ubermenu-target-title ubermenu-target-text'])[2]");
		Thread.sleep(1000);
		// t1.fn_ProductCarloan("li.ubermenu-tab:nth-child(2) > a:nth-child(1) >
		// span:nth-child(1)");
		Thread.sleep(1000);
		t1.fn_selectCreditcardHeader("(//span[@class='ubermenu-target-title ubermenu-target-text'])[10]");
		Thread.sleep(2000);
		t1.fn_checkEligibility(".rb-prdct-s-instant");
		Thread.sleep(2000);
		t1.fn_completeFirstStepMobile("input[id='5'][column-name='phone']", "9665377429");
		Thread.sleep(2000);
		t1.fn_completeFirstStepEmail("input[id=\"6\"][column-name]", "praveen.kumar@rubique.com");
		Thread.sleep(1000);
		t1.stepTwoSelectCity("select[column-name='current_city_id']", "Mumbai");
		t1.fn_completeFirstStepContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[1]");
		Thread.sleep(2000);
		t1.fn_completeSecondStepFullName("input[id=\"2\"][column-name]", "Bhupendra");// full
																						// name
		t1.fn_stepTwoSelectGenderId("#form_group_4 > div:nth-child(2) > div:nth-child(1) > select:nth-child(2)","Male");
		t1.stepTwoSelectDOB("#fieldNumber8", "1990-06-09"); // DOB
		t1.stepTwoSelectMarital("#form_group_4 > div:nth-child(4) > div:nth-child(1) > select:nth-child(2)", "Married"); // unmarried
		t1.fn_stepTwoSelectNationality("#form_group_4 > div:nth-child(5) > div:nth-child(1) > select:nth-child(2)","INDIAN"); // nationality
		t1.fn_completeSecondStagePan("input[id=\"15\"][column-name=\"pan_id\"]", "cdnpp8846b");
		t1.fn_stepTwoSelectAddress_Line1("input[id=\"20\"][name=\"20\"]", "abcd");
		t1.fn_StepTwoSelectAddress_Line2("input[id=\"21\"][name=\"21\"]", "xyz");
		t1.stepTwoSelectPincode("input[id=\"23\"][name=\"23\"]", "425401");
		//t1.fn_adressProvidedCategory("#form_group_4 > div:nth-child(14) > div > select","Permanent Address");
		t1.stepTwoSelectYears_at_Current_Residence("input[id=\"29\"][name=\"29\"]", "12");
		t1.stepTwoSelectContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[2]");
		Thread.sleep(2000);
		t1.fn_completeThirdStageOccuoation("select[class='form-control value-field'][column-name='occupation_id']","Salaried");

		t1.fn_completeFourthStageCompany_Name("input[id='fieldNumber40'][column-name='company_id']","TATA AIG");
		t1.fn_completeFourthStageGross_Monthly_Income("input[id='42'][column-name='gross_monthly_income'][maxlength='20']", "80000");		//t1.fn_completeForthStageModeOF_salary("div.parent-id-38:nth-child(4) > div:nth-child(1) > select:nth-child(2)","Cash Payment");
		t1.fn_EnterNetMonthlyIncome("input[column-name='net_monthly_income']", "95000");
		t1.fn_completeForthStageModeOF_salary("//select[@column-name=\"salary_mode_id\"]","Cash Payment");
		t1.fn_forthStageProfessionType("//select[@column-name='profession_type_id']","Others");
		// t1.fn_completeFourthStageDo_you_have_income_proof_document("div.parent-id-38:nth-child(4)
		// > div:nth-child(1) > select:nth-child(2)", 1);
		t1.fn_completeFourthStageNumber_of_Years_in_Current_Work("//input[@column-name='current_company_experience']", "10");
		t1.fn_completeFourthStageTotal_Number_of_Years_in_Work("//input[@column-name='total_working_experience']", "11");
		t1.fn_completeForthStageOffice_Address1("//input[@column-name='office_address.address1']","Saki naka");
		t1.fn_completeForthStageOffice_Address2("//input[@column-name='office_address.address2']","krislon house");
		t1.fn_completeForthStageOffice_city("//select[@column-name='office_address.city']","Pune");
		t1.fn_completeForthStageOffice_Pincode("(//input[@column-name='office_address.pincode'])[1]","425401");
		t1.fn_completeForthStageOffice_Phone("(//input[@column-name='office_phone.phone_no'])[1]","025512522");
		t1.fn_completeFourthStageContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[3]");
		Thread.sleep(2000);
		t1.fn_completeFifthStageDoYouHave_Any_Existing_CC("//select[@column-name='any_existing_credit_card']", "No");
		Thread.sleep(2000);
		t1.fn_completeFinalSubmit("button[class='hex-hor-button submit_application_button pull-right']");
	}
}
