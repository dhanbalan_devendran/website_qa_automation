package testscripts;

import functions.SmeFunctions;
  
public class SmeProductFlowBk1 {

	 public static void main(String[] args) throws InterruptedException
	 {      
			 SmeFunctions t1 = new SmeFunctions();
			t1.fn_setUpChrome();
			t1.fn_openUrl("https://m.rubique.com");
			 Thread.sleep(1000);
		    t1.fn_clickSME("(//span[@class='ubermenu-target-title ubermenu-target-text'])[177]");
		    //t1.fn_clickSME("(//a[@href='/sme'])[2]");
		    Thread.sleep(1000);
		    t1.fn_clickApplyHere("//div[@class='apply-header']");
		    Thread.sleep(1000);
		    t1.fn_EnterName("//input[@column-name='name']","Bhupendra");
		    Thread.sleep(1000);
		    t1.fn_EnterMail("(//input[@type='email'])[1]","tester@test.com");
		    Thread.sleep(1000);
		    t1.fn_EnterMobile("(//input[@type='mobile'])[1]","9665377429");
		    Thread.sleep(1000);
		    t1.fn_EnterCity("(//input[@type='text'])[2]","Mumbai");
		    Thread.sleep(1000);
		    t1.fn_SelectProduct("(//select[@column-name='product_type_id'])[1]","Loan Against Property");
		    Thread.sleep(1000);
		    t1.fn_EnterComment("(//textarea[@id='q-comment'])[1]","Need Information");
		    Thread.sleep(1000);
		    t1.fn_ClickSubmit("(//button[@type='submit'])[2]");
		    Thread.sleep(1000);
		    t1.fn_ClickClosebutton("//*[@id=\"apply-form\"]/button/span");
		    Thread.sleep(1000);
		    //t1.fn_clickSME("(//a[@href='/sme'])[2]");

	}
}
