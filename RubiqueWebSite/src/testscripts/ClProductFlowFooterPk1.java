package testscripts;
import org.openqa.selenium.WebDriver;
import functions.ClFunctions;

public class ClProductFlowFooterPk1 extends ClFunctions
{
	public static WebDriver driver;
	
	public static void main(String[] args) throws Exception 
	{
		ClProductFlowFooterPk1 t1 = new ClProductFlowFooterPk1();
		driver= t1.fn_setUpChrome();
		t1.fn_openUrl("https://beta.rubique.com/");
		//LOGIN
				t1.fn_homeLoginClick("#log-btn-header");
				Thread.sleep(2000);
				t1.fn_emailId("#login-email", "praveen.kumar@rubique.com");
			    Thread.sleep(1000);
				t1.fn_password("#login-pwd", "praveen@123");
			    Thread.sleep(1000);
			    t1.fn_login("#login-button");
	    Thread.sleep(1000);
	    t1.fn_clickProduct("//a[@class='footerLoanLinks'][@href='/car-loans-and-vehicle-finance']");
	    Thread.sleep(1000);
	    t1.fn_selectCarLoan("(//a[@product-id='1309'][@href='/car-loans-and-vehicle-finance/au-small-finance-bank-car-loan'])[1]");
	    Thread.sleep(1000);
        t1.fn_clickEligibility(".rb-prdct-s-instant.check-eligibility-product-btn");
	    Thread.sleep(1000);
        t1.fn_completeFirstStepMobile("//input[@column-name='phone'][@name='5']","8600741820");
        t1.fn_completeFirstStepEmail("//input[@column-name='email'][@type='email']","praveen.kumar@rubique.com");
        t1.fn_completeFirstStepContinue("//*[@id='form_group_3']/div[3]/button[2]/span");
        Thread.sleep(1000);
        t1.fn_completeSecondStepFullName("input[id='2'][name='2']", "testerpk");
        t1.fn_stepTwoSelectGenderId("#form_group_4 > div:nth-child(2) > div:nth-child(1) > select:nth-child(2)", "Male");
        t1.fn_stepTwoSelectDOB("#fieldNumber8", "1990-06-09");
        t1.fn_stepTwoSelectMarital("#form_group_4 > div:nth-child(4) > div:nth-child(1) > select:nth-child(2)", "Single");
        t1.fn_stepTwoSelectNationality("#form_group_4 > div:nth-child(5) > div:nth-child(1) > select:nth-child(2)", "INDIAN");
        t1.fn_completeSecondStagePan("//*[@id='15']", "DTUPK6334K");
        t1.fn_stepTwoSelectAddress_Line_1("//*[@id='20']","test");
        t1.fn_stepTwoSelectAddress_Line_2("//*[@id='21']", "tester");
        t1.fn_stepTwoSelectCity("#form_group_4 > div:nth-child(12) > div:nth-child(1) > select:nth-child(2)","Mumbai");
        t1.fn_stepTwoSelectPincode("//*[@id='23']", "421503");
        t1.fn_stepTwoSelectYears_at_current_residence("//*[@id='29']", "12");
        t1.fn_stepTwoSelectType_of_Accommodation("#form_group_4 > div:nth-child(16) > div:nth-child(1) > select:nth-child(2)", "Owned");
        t1.fn_stepTwoSelectContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[2])");
        Thread.sleep(1000);
        t1.fn_completeThirdStageLoan_Amount_Required("//*[@id='115']", "200000");
        Thread.sleep(1000);
        t1.fn_completeThirdStageTenure("#form_group_2 > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > input:nth-child(1)", "7");
        Thread.sleep(1000);
        t1.fn_completeThirdStageContinue("#form_group_2 > div:nth-child(3) > button:nth-child(2)");
        Thread.sleep(1000);
        t1.fn_completeFourthStageOccupation("#form_group_5 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)", "Salaried");
        t1.fn_completeFourthStageCompany_Name("div.parent-id-38:nth-child(2) > div:nth-child(1) > input:nth-child(3)","TATA AIG");
        t1.fn_completeFourthStageGross_Monthly_Income("div.parent-id-38:nth-child(3) > div:nth-child(1) > input:nth-child(3)", "180000");
        t1.fn_completeFourthStageDo_you_have_income_proof_document("div.parent-id-38:nth-child(4) > div:nth-child(1) > select:nth-child(2)", "Yes");
        t1.fn_completeFourthStageNumber_of_Years_in_Current_Work("div.parent-id-38:nth-child(5) > div:nth-child(1) > input:nth-child(3)", "7");
        t1.fn_completeFourthStageTotal_Number_of_Years_in_Work("div.parent-id-38:nth-child(6) > div:nth-child(1) > input:nth-child(3)", "7");
        t1.fn_completeFourthStageContinue("div.col-sm-12:nth-child(14) > button:nth-child(2)");
        Thread.sleep(1000);
        t1.fn_completeFifthStageAny_Existing_Loan_with_Bank("#form_group_10 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","15");
        t1.fn_completeFifthStageContinue("#form_group_10 > div:nth-child(3) > button:nth-child(2)");
        Thread.sleep(1000);
        t1.fn_completeSixthStageBanking_Since("#form_group_6 > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > input:nth-child(1)", "15");
        t1.fn_completeSixthStageContinue("div.f1-buttons:nth-child(2) > button:nth-child(2)");
        Thread.sleep(1000);
       // t1.fn_completeSeventhStageVehicle_Type("#form_group_8 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Honda");
        t1.fn_completeSeventhStageManufacturer("#form_group_8 > div:nth-child(3) > div:nth-child(1) > select:nth-child(2)","Honda");
        t1.fn_completeSeventhStageModel("div.formOneBlock:nth-child(18) > div:nth-child(1) > select:nth-child(2)", "City VX Diesel");
        t1.fn_completeSeventhStageEx_showroom_Price("input[id='139'][column-name='details.ex_showroom_price']", "170000");
        t1.fn_completeSeventhStageAgeofVehicle("//*[@id='140']","0");
        t1.fn_completeFinalSubmit(".submit_application_button");
        Thread.sleep(6000);
	}
}
