package testscripts;

import org.openqa.selenium.chrome.ChromeDriver;
import functions.UclFunctions;

public class UclProductFlowHeaderPk1 {
	public static ChromeDriver driver;

	public static void main(String[] args) throws Exception {
		UclFunctions t1 = new UclFunctions();
		t1.fn_setUpChrome();
		t1.fn_openUrl("https://m.rubique.com/");
		Thread.sleep(1000);
		// @LOGIN
		t1.fn_homeLoginClick("//*[@id='menu-item-7']/p/span");
		Thread.sleep(2000);
		t1.fn_emailId("//*[@id='login-email']", "praveen.kumar@rubique.com");
		Thread.sleep(1000);
		t1.fn_password("//*[@id='login-pwd']", "praveen@123");
		Thread.sleep(1000);
		t1.fn_login("//*[@id='login-button']");
		// @HEADER FLOW
		Thread.sleep(2000);
		t1.fn_Product("Products");
		Thread.sleep(2000);
		t1.fn_ConsumerLoan("Consumer Loans");
		Thread.sleep(2000);
		t1.fn_HeaderUsedCarLoanClick("#menu-item-2221 > a:nth-child(1) > span:nth-child(1)");
		// @Eligibility
		Thread.sleep(1000);
		t1.fn_clickEligibility(".rb-prdct-s-instant");
		// @Contact Information
		Thread.sleep(1000);
		t1.fn_selectSecondStagemobile(
				"input[class='form-control value-field custom-class-mobile'][column-name='phone']", "8600741820");
		t1.fn_selectSecondStageEmailId("input[class='form-control value-field custom-class-email'][ui-type='email']",
				"praveen.kumar@rubique.com");
		t1.fn_selectSecondStageNext(
				"(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[1]");
		// @Personal Information
		Thread.sleep(1000);
		t1.fn_completeSecondStepFullName("input[id='2'][name='2']", "testerpk");
		t1.fn_stepTwoSelectGenderId("select[column-name='gender_id'][ui-type='dropdown']", "Male");
		t1.fn_stepTwoSelectDOB(
				"input[column-name='dob'][class='form-control datepicker-dob value-field hasDatepicker']",
				"1990-06-09");
		t1.fn_stepTwoSelectMarital("select[column-name='marital_status_id'][class='form-control value-field']",
				"Married");
		t1.fn_completeSecondStagePan("input[column-name='pan_id'][maxlength='10']", "DTUPK6334K");
		t1.fn_stepTwoSelectAddressLine1("input[column-name='current_address.address1'][maxlength='30']", "test");
		t1.fn_stepTwoSelectAddressLine2("input[column-name='current_address.address2'][id='21']", "tester");
		t1.fn_stepTwoSelectCity("select[column-name='current_city_id'][class='form-control value-field']", "Mumbai");
		t1.fn_stepTwoSelectYearsatcurrentresidence("input[id='29'][column-name='residence_year']", "12");
		t1.fn_stepTwoSelectTypeofAccommodation(
				"select[class='form-control value-field'][column-name='accomodation_type']", "Owned");
		t1.fn_stepTwoSelectContinue(
				"(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[2]");
		// Loan Details
		Thread.sleep(1000);
		t1.fn_completeThirdStageLoanAmountRequired(
				"input[class='form-control value-field custom-class-formatted-number slider-inp-value'][column-name='loan_amount']",
				"500000");
		Thread.sleep(1000);
		t1.fn_completeThirdStageTenure("(//input[@class='form-control'][@name='years'])[1]", "2");
		Thread.sleep(1000);
		t1.fn_completeThirdStageContinue(
				"(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[3]");
		Thread.sleep(1000);
		// Applicant Information
		t1.fn_completeFourthStageOccupation("select[class='form-control value-field'][column-name='occupation_id']",
				"Salaried");
		Thread.sleep(1000);
		t1.fn_completeFourthStageCompanyName("(//input[@column-name='company_id'])[1]", "TATA AIG");
		Thread.sleep(1000);
		t1.fn_completeFourthStageGrossMonthlyIncome("(//input[@column-name='gross_monthly_income'])[1]", "180000");
		Thread.sleep(1000);
		t1.fn_completeFourthStageDoyouhaveincomeproofdocument(
				"(//select[@class='form-control value-field'][@column-name='has_income_proof'])[1]", "Yes");
		Thread.sleep(1000);
		t1.fn_completeFourthStageNumberofYearsinCurrentWork(
				"(//input[@class='form-control value-field custom-class-durationy'][@column-name='current_company_experience'])[1]",
				"7");
		Thread.sleep(1000);
		t1.fn_completeFourthStageTotalNumberofYearsinWork(
				"(//input[@class='form-control value-field custom-class-durationy'][@column-name='total_working_experience'])[1]",
				"7");
		Thread.sleep(1000);
		t1.fn_completeFourthStageContinue(
				"(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[4]");
		Thread.sleep(1000);
		// Existing Loan Details
		t1.fn_completeFifthStageAnyExistingLoanwithBank(
				"select[class='form-control value-field'][column-name='is_existing_loan']", "No");
		t1.fn_completeFifthStageContinue(
				"(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[5]");
		Thread.sleep(2000);
		// Financial Information
		Thread.sleep(2000);
		t1.fn_completeSixthStageBankingSince("(//input[@name='years'])[2]", "15");
		t1.fn_completeSixthStageContinue(
				"(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[6]");
		// Vehicle Details
		Thread.sleep(2000);
		t1.fn_completeSeventhStageManufacturer(
				"select[class='form-control value-field'][column-name='details.manufacturer']", "Maruti Suzuki");
		Thread.sleep(2000);
		t1.fn_completeSeventhStageModel("div.formOneBlock:nth-child(24) > div:nth-child(1) > select:nth-child(2)",
				"Swift DZire Automatic");
		t1.fn_completeSeventhStageExshowroomPrice("input[id='139'][column-name='details.ex_showroom_price']",
				"1500000");
		t1.fn_completeSeventhStageAgeofVehicle(
				"input[class='form-control value-field custom-class-durationy'][column-name='details.manufacture_year']",
				"7");
		t1.fn_completeFinalStage(
				"button[class='hex-hor-button submit_application_button pull-right'][type='submit'][name='submit']");
		Thread.sleep(6000);

	}
}
