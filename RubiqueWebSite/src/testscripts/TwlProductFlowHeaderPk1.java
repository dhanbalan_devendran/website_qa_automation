package testscripts;
import org.openqa.selenium.chrome.ChromeDriver;

import functions.TwlFunctions;

public class TwlProductFlowHeaderPk1 {
	public static ChromeDriver driver;
	
	public static void main(String[] args) throws Exception 
	{
		TwlFunctions t1 = new TwlFunctions();
		t1.fn_setUpChrome();
		t1.fn_openUrl("https://m.rubique.com/");Thread.sleep(1000);
		
		
		
	//LOGIN
		t1.fn_homeLoginClick("//*[@id='menu-item-7']/p/span");
		Thread.sleep(2000);
		t1.fn_emailId("//*[@id='login-email']", "praveen.kumar@rubique.com");
	    Thread.sleep(1000);
		t1.fn_password("//*[@id='login-pwd']", "praveen@123");
	    Thread.sleep(1000);
	    t1.fn_login("//*[@id='login-button']");
	    Thread.sleep(1000);
	    //
	    t1.fn_Product("li.ubermenu-item-object-page:nth-child(2) > a:nth-child(1) > span:nth-child(1)");
	    Thread.sleep(1000);
	    t1.fn_ProductTWLheader("li.ubermenu-tab:nth-child(2) > a:nth-child(1) > span:nth-child(1)");
	    Thread.sleep(1000);
	    t1.fn_ProductCarloanHDFCBank("li.ubermenu-item-190:nth-child(1) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1) > span:nth-child(1)");
	    Thread.sleep(1000);
	   //Eligibility 
        t1.fn_clickEligibility(".rb-prdct-s-instant");
	    Thread.sleep(1000);
	    
	  //Contact informationn
	    t1.fn_selectSecondStagemobile("input[id='5'][column-name='phone']", "8600741820");
	    t1.fn_selectSecondStageEmailId("input[id='6'][column-name='email']", "praveen.kumar@rubique.com");
	    t1.fn_selectSecondStageNext("#form_group_3 > div:nth-child(3) > button:nth-child(2)");
	    Thread.sleep(1000);
	   // Personal Information
	    
	    Thread.sleep(1000);
	    t1.fn_completeSecondStepFullName("input[id='2'][name='2']", "testerpk");
        t1.fn_stepTwoSelectGenderId("#form_group_4 > div:nth-child(2) > div:nth-child(1) > select:nth-child(2)","Male");
        t1.fn_stepTwoSelectDOB("#fieldNumber8", "1990-06-09");
        t1.fn_stepTwoSelectMarital("#form_group_4 > div:nth-child(4) > div:nth-child(1) > select:nth-child(2)","Married");
        t1.fn_stepTwoSelectNationality("#form_group_4 > div:nth-child(5) > div:nth-child(1) > select:nth-child(2)","INDIAN"); 
        t1.fn_completeSecondStagePan("input[column-name='pan_id'][maxlength='10']", "DTUPK6334K");
        t1.fn_stepTwoSelectAddress_Line_1("input[column-name='current_address.address1'][maxlength='30']","test");
        t1.fn_stepTwoSelectAddress_Line_2("input[column-name='current_address.address2'][id='21']", "tester");
        t1.fn_stepTwoSelectCity("#form_group_4 > div:nth-child(12) > div:nth-child(1) > select:nth-child(2)","Mumbai");
        t1.fn_stepTwoSelectPincode("input[id='23'][column-name='current_address.pincode']", "421503");
        t1.fn_stepTwoSelectYears_at_current_residence("input[id='29'][column-name='residence_year']", "12");
        t1.fn_stepTwoSelectType_of_Accommodation("#form_group_4 > div:nth-child(16) > div:nth-child(1) > select:nth-child(2)","Owned");
        t1.fn_stepTwoSelectContinue("div.col-sm-12:nth-child(17) > button:nth-child(2)");
        //Loan Details
        Thread.sleep(1000);

        t1.fn_completeThirdStageLoan_Amount_Required("input[id='115'][column-name='loan_amount']", "200000");
        Thread.sleep(1000);
        t1.fn_completeThirdStageTenure("#form_group_2 > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > input:nth-child(1)", "7");
        Thread.sleep(1000);
        t1.fn_completeThirdStageContinue("#form_group_2 > div:nth-child(3) > button:nth-child(2)");
        Thread.sleep(1000);
       // Applicant Information
        t1.fn_completeFourthStageOccupation("#form_group_5 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Salaried");
        Thread.sleep(1000);
        t1.fn_completeFourthStageCompany_Name("div.parent-id-38:nth-child(2) > div:nth-child(1) > input:nth-child(3)","TATA AIG");        Thread.sleep(1000);
        t1.fn_completeFourthStageGross_Monthly_Income("div.parent-id-38:nth-child(3) > div:nth-child(1) > input:nth-child(3)", "180000");        Thread.sleep(1000);
        t1.fn_completeFourthStageType_of_Company("div.parent-id-38:nth-child(4) > div:nth-child(1) > select:nth-child(2)", "Private Limited Company");        Thread.sleep(1000);
        t1.fn_completeFourthStageProfession_Type("div.parent-id-38:nth-child(5) > div:nth-child(1) > select:nth-child(2)","Others");        Thread.sleep(1000);
        t1.fn_completeFourthStageDo_you_have_income_proof_document("div.parent-id-38:nth-child(6) > div:nth-child(1) > select:nth-child(2)","Yes");        Thread.sleep(1000);
        t1.fn_completeFourthStageNumber_of_Years_in_Current_Work("div.parent-id-38:nth-child(7) > div:nth-child(1) > input:nth-child(3)", "7");        Thread.sleep(1000);
        t1.fn_completeFourthStageTotal_Number_of_Years_in_Work("div.parent-id-38:nth-child(8) > div:nth-child(1) > input:nth-child(3)", "7");        Thread.sleep(1000);
        t1.fn_completeFourthStageContinue("(//*[@class='hex-hor-button continue_button pull-right'])[4]");
        Thread.sleep(1000);
        //Existing Loan Details
        t1.fn_completeFifthStageAny_Existing_Loan_with_Bank("#form_group_10 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","No");
        t1.fn_completeFifthStageContinue("#form_group_10 > div:nth-child(3) > button:nth-child(2)");
        Thread.sleep(2000);
        //Co-Applicant's Information
        t1.fn_AddCoapplicant("//*[@id='form_group_11']/div[1]/div/select","Yes");
        Thread.sleep(1000);
        t1.fn_RelationwithPrimaryApplicant("//*[@id='form_group_11']/div[2]/div/select","Others");
        t1.fn_CoapplicantsMonthlyincome("//*[@id='103']","850000");
        
        
        //Financial Information
        t1.fn_completeSixthStagePrimary_Existing_Bank_Name("#form_group_6 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Axis Bank");
        t1.fn_completeSixthStageBanking_Since("#form_group_6 > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > input:nth-child(1)", "15");
        t1.fn_completeSixthStageContinue("#form_group_6 > div:nth-child(3) > button:nth-child(2)");
        
        Thread.sleep(1000);
        //Vehicle Details
        t1.fn_completeSeventhStageManufacturer("#form_group_8 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","TVS");
        Thread.sleep(2000);
        t1.fn_completeSeventhStageModel("div.formOneBlock:nth-child(18) > div:nth-child(1) > select:nth-child(2)","APPACHE RTR 160 CC");
        t1.fn_completeSeventhStageEx_showroom_Price("input[id='139'][column-name='details.ex_showroom_price']", "150000");
       t1.fn_completeFinalStage(".submit_application_button");
        Thread.sleep(6000);
	} 

}
