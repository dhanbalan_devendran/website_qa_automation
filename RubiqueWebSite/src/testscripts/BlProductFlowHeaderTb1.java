package testscripts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import functions.BlFunctions;

public class BlProductFlowHeaderTb1 
{
	WebDriver driver;
	String Actualtext;
	BlFunctions objectR=PageFactory.initElements(driver, BlFunctions.class);
	
		
	@Test
	public void fn_viewProductOffers() throws InterruptedException, ClassNotFoundException, SQLException
	{
		boolean Flag = true;
	//boolean productFlag = false;
		int flagLAP = 1;
		 
		//driver = objectR.fn_setUpChrome();
	
	//	objectR.fn_openUrl("http://beta.rubique.com");
		if(Flag == true) //
		{	
			
			objectR.fn_openUrl("https://m.rubique.com");
			Thread.sleep(1000);	
			objectR.fn_loginButton("#log-btn-header");
			Thread.sleep(1000);	
			objectR.fn_LoginEmail("#login-email");
			Thread.sleep(1000);	
			objectR.fn_Password("#login-pwd","praveen@123");
			Thread.sleep(1000);	
			objectR.fn_loginButton("#login-button");
			Thread.sleep(1000);	
			Thread.sleep(5000);
			objectR.fn_HeaderFlow("li.ubermenu-item-object-page:nth-child(2) > a:nth-child(1) > span:nth-child(1)",
					"#menu-item-183 > a:nth-child(1) > span:nth-child(1)",
					"li.ubermenu-item-213:nth-child(1) > a:nth-child(1) > span:nth-child(1)");
		
			Thread.sleep(1000);
			objectR.fn_checkEligibility(".rb-prdct-s-instant");
			Thread.sleep(1000);			
			objectR.fn_completeFirstStep();			
			Thread.sleep(1000);
			objectR.fn_completeSecondStage();
    		Thread.sleep(1000);
    		objectR.fn_completeThirdStage();
    		Thread.sleep(1000);
    		objectR.fn_completeFourthStage();
    		Thread.sleep(1000);
    		objectR.fn_completeFifthStage();
    		Thread.sleep(1000);
    		objectR.fn_completeSixthStage();
    		Thread.sleep(1000);
    		objectR.fn_stepSeventhprimaryExistingBank("[name='80']", "ICICI Bank");
    		Thread.sleep(1000);
    		objectR.fn_stepOneContinueButton("(//*[@type='submit' and @name='submit'])[7]");
    		//Thread.sleep(1000);
    		//driver.navigate().to("http://m.rubique.com/business-loan-india");
		}else
		{
			objectR.fn_productLink("a[href*='business-loan-india'][class='footerLoanLinks']");
			Thread.sleep(10000);
			List <WebElement> button = driver.findElements(By.xpath("//*[@class ='viewFullDetail purple-btn']"));
			System.out.println(button);
		
			int lenght=button.size();
			System.out.println(button.size());
					
			
			for (int i=0; i<lenght; i++) 
			{      
			    try {
			    	
			    		Thread.sleep(1000);
			    		button.get(i).click();
			    		
			    		System.out.println("i value :"+i+ "|title is :" +driver.getTitle() + "|Item Url is :"+driver.getCurrentUrl());
			    		Thread.sleep(1000);
			    			    			    		  			    		
			    		objectR.fn_checkEligibility(".rb-prdct-s-instant");
			    		Thread.sleep(1000);
			    		objectR.fn_completeFirstStep();
			    		Thread.sleep(1000);
			    		//objectR.fn_completeSecondStage();
			    		objectR.fn_stepTwoFullName("input[id='2'][name='2']", "BL automation user");
					 	Thread.sleep(1000);
					 	objectR.fn_stepTwoSelectGenderId("[class='form-control value-field'][column-name='gender_id'][name='7']","Male");
					 	Thread.sleep(1000);
					 	objectR.fn_stepTwoDOB("input[id='fieldNumber8'][name='8']", "01-05-1999");
					 	Thread.sleep(1000);
					 	objectR.fn_stepTwoSelectMaritalStatusId("[class='form-control value-field'][column-name='marital_status_id'][name='9']", "Married");
					 	Thread.sleep(1000);
					 	objectR.fn_stepTwoSelectNationalityId("[class='form-control value-field'][column-name='nationality'][name='12']", "INDIAN");
					 	Thread.sleep(1000);
					// 	objectR.fn_stepTwoResidentalStatus("[class ='form-control value-field'][name='14']",1);
					 	
					 	Thread.sleep(1000);
					 	objectR.fn_stepTwoPAN("[column-name='pan_id'][name='15']", "AKUPD2578C");
						Thread.sleep(1000);
						objectR.fn_stepTwoAddress1("[column-name='current_address.address1'][name='20']", "Sion Mumbai" );
						Thread.sleep(1000);
						objectR.fn_stepTwoAddress1("[column-name='current_address.address2'][name='21']", "andheri mumbai ");
						objectR.fn_stepTwoState("[class ='form-control value-field'][name='24']","Maharashtra");
					 	
						objectR.fn_stepTwoSelectCityId("[column-name='current_city_id'][name='25']", "Mumbai");
						Thread.sleep(1000);
						objectR.fn_stepTwoPincode("[column-name='current_address.pincode'][name='23']", "400001");
						Thread.sleep(1000);
						objectR.fn_stepTwoYearOfResidence("[class='form-control value-field custom-class-durationy'][column-name='residence_year'][name='29']", "14");
						Thread.sleep(1000);
						//fn_stepTwoSelectAccomodationYearsId("//*[@id='29']", "4");
						objectR.fn_stepTwoSelectAccomodationTypeId("[class='form-control value-field'][column-name='accomodation_type'][name='30']", "Owned");	
						Thread.sleep(1000);
						objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[2]");
						//fn_stepOneContinueButton("div.f1-buttons:nth-child(33) > button:nth-child(2)");
						//fn_stepOneContinueButton("buttons[1]");
						System.out.println("fn_completeSecondStage");	

			    		Thread.sleep(1000);
			    		objectR.fn_stepThreeLoanAmount("[id='115'][column-name='loan_amount'][name='115']" ,"20000");
			    		Thread.sleep(1000);
			    		objectR.fn_stepThreeTenureYears("[name = 'years']" ,"3");
			    		Thread.sleep(1000);
			    		objectR.fn_stepThreeTenureMonths("[name = 'months']" ,"0");
			    		objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[3]");
			    		System.out.println("fn_completeThirdStage");	
			    		Thread.sleep(1000);
			    		objectR.fn_completeFourthStage();
			    		Thread.sleep(1000);
			    		objectR.fn_completeFifthStage();
			    		Thread.sleep(1000);
			    		objectR.fn_completeSixthStage();
			    		Thread.sleep(1000);
			    	//	objectR.fn_primaryExistingBank();  
			    		Thread.sleep(1000);
			    			    		
			    		objectR.fn_completeSeventhStage("jdbc:mysql://jun20.cib48esswkls.ap-south-1.rds.amazonaws.com"); 
			    		Thread.sleep(1000);
			    		objectR.fn_thankYouPage();
			    		Thread.sleep(3000);
			    		driver.navigate().to("http://website.rubique.com/business-loan-india");
			    		
			    		Thread.sleep(1000);
			    		button = driver.findElements(By.xpath("//*[@class ='viewFullDetail purple-btn']"));
			    		Thread.sleep(3000);
			    
			    } catch (Exception e) 
			    	{
			    		e.printStackTrace();
			    	}
			   
			}
		
		}	
			
	}


}
