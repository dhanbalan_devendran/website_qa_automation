package testscripts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import functions.BlFunctions;

public class BlProductFlowMiddleTb1 {

	WebDriver driver;
	String Actualtext;
	BlFunctions objectR = PageFactory.initElements(driver, BlFunctions.class);

	@Test
	public void fn_viewProductOffers() throws InterruptedException, ClassNotFoundException, SQLException
	{
		boolean Flag = true;
		boolean productFlag = true;
		int productFlow = 2;
		
		//driver = objectR.fn_setUpChrome();
	
	//	objectR.fn_openUrl("http://beta.rubique.com");
		
		//if (productFlow ==2)
		//{
			if (productFlag == true)
			{		
				
				objectR.fn_openUrl("https://m.rubique.com");
				Thread.sleep(1000);	
				objectR.fn_loginButton("#log-btn-header");
				Thread.sleep(1000);	
				objectR.fn_LoginEmail("#login-email");
				Thread.sleep(1000);	
				objectR.fn_Password("#login-pwd","praveen@123");
				Thread.sleep(1000);	
				objectR.fn_loginButton("#login-button");
				Thread.sleep(1000);	
				objectR.fn_productLink("//*[@id='productsJump']/div[2]/ul/li[1]/a/p[2]");
			
				objectR.fn_basicInfo();
				objectR.fn_loaderwait("button.apply-button");
				objectR.fn_productMatch("button.apply-button");
				objectR.fn_loaderwait("input[name='5']");
				objectR.fn_completeFirstStep();			
				Thread.sleep(1000);
				objectR.fn_completeSecondStage();
	    		Thread.sleep(1000);
	    		//objectR.fn_completeThirdStage1();
	    		objectR.fn_stepThreeloanBasis("[column-name='other_details.loan_basis'][name='595']","On Existing Swipe Machine");
	    		objectR.fn_stepThreeswipeMachinesince("[column-name='other_details.swipe_machine_since'][name='597'] " ,"6 Months");
	    		objectR.fn_stepThreeSwipeMonth("[id='596'][class='form-control value-field custom-class-text'][name='596'] ","6");
	    		Thread.sleep(1000);
	    		objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[3]");
	   		 	System.out.println("fn_completeThirdStage");	
	    		Thread.sleep(1000);
	    	
	    									
	    		objectR.fn_stepFourIndustryType("(//*[@name='53'])[1]" , "Manufacturing");
	    		objectR.fn_stepFourprofessionType("(//*[@name='54'])[1]" ,"Others");
	    		objectR.fn_stepTwoPermanentAddress("(//*[@id='70'])[1]", "office address 1");
	    		objectR.fn_stepTwoPermanentAddress("(//*[@id='71'])[1]", "office address 2");		
				
	    		objectR.fn_stepTwoLandmark("input[id='72'][name='72']","landmark");
	    		objectR.fn_stepTwoSelectCityId("#form_group_9 > div:nth-child(9) > div > select", "Mumbai");
	    		objectR.fn_stepTwoPincode("div.parent-id-38:nth-child(10) > div:nth-child(1) > input:nth-child(2)", "400001");
	    		objectR.fn_stepFourofficePhone("div.parent-id-38:nth-child(11) > div:nth-child(1) > input:nth-child(2)", "022654585");
	    		objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[4]");	
			 	System.out.println("fn_completeFourthStage");				 	 
	    		Thread.sleep(1000);
	    		objectR.fn_completeFifthStage();
	    		Thread.sleep(1000);
	    		objectR.fn_completeSixthStage();
	    		//	objectR.fn_completeSeventhStage("jdbc:mysql://jun20.cib48esswkls.ap-south-1.rds.amazonaws.com"); 
	    		//objectR.fn_thankYouPage();
	    		objectR.fn_stepSeventhprimaryExistingBank("[name='80']", "ICICI Bank");
	    		Thread.sleep(1000);
	    		//objectR.fn_thankYouPage();
	    		objectR.fn_stepOneContinueButton("(//*[@type='submit' and @name='submit'])[7]");
	    	//	driver.navigate().to("http://m.rubique.com/business-loan-india");
			}else if (productFlag == false)	
			{
				
				objectR.fn_productLink(".col-lg-5 > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1) > p:nth-child(3)");
				objectR.fn_basicInfo();
			//	objectR.fn_loaderwait()
				objectR.fn_loaderwait("tr.rowShadow:nth-child(1) > td:nth-child(5) > div:nth-child(1) > button:nth-child(1)");
				objectR.fn_productMatch("tr.rowShadow:nth-child(1) > td:nth-child(5) > div:nth-child(1) > button:nth-child(1)");
				objectR.fn_completeFirstStep();
				//objectR.fn_completeSecondStage();	
				Thread.sleep(1000);
				objectR.fn_stepTwoFullName("input[id='2'][name='2']", "BL automation user");
			 	Thread.sleep(1000);
			 //	objectR.fn_stepTwoSelectGenderId("[class='form-control value-field'][column-name='gender_id'][name='7']", 1);
			 	Thread.sleep(1000);
			 	objectR.fn_stepTwoDOB("input[id='fieldNumber8'][name='8']", "01-05-1999");
			 	Thread.sleep(1000);
			// 	objectR.fn_stepTwoSelectMaritalStatusId("[class='form-control value-field'][column-name='marital_status_id'][name='9']", 1);
			 	Thread.sleep(1000);
			// 	objectR.fn_stepTwoSelectNationalityId("[class='form-control value-field'][column-name='nationality'][name='12']", 1);
			 	Thread.sleep(1000);
			 //	objectR.fn_stepTwoResidentalStatus("[class ='form-control value-field'][name='14']",1);
			 	Thread.sleep(1000);
			 	objectR.fn_stepTwoPAN("[column-name='pan_id'][name='15']", "AKUPD2578C");
				Thread.sleep(1000);
				objectR.fn_stepTwoAddress1("[column-name='current_address.address1'][name='20']", "Sion Mumbai" );
				Thread.sleep(1000);
				objectR.fn_stepTwoAddress1("[column-name='current_address.address2'][name='21']", "andheri mumbai ");
				Thread.sleep(1000);
				Thread.sleep(1000);
				objectR.fn_stepTwoPincode("[column-name='current_address.pincode'][name='23']", "400001");
				Thread.sleep(1000);
				objectR.fn_stepTwoYearOfResidence("[class='form-control value-field custom-class-durationy'][column-name='residence_year'][name='29']", "14");
				Thread.sleep(1000);
				Thread.sleep(1000);
				objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[2]");
				
	    		Thread.sleep(1000);
	    		objectR.fn_stepThreeLAP();	
	    		Thread.sleep(1000);    		    		
	    		objectR.fn_stepOneContinueButton("//*[@id='form_group_2']/div[3]/button[2]/span");
	    		Thread.sleep(1000);
	    		objectR.fn_stepFourCurrentWorkYears("[class='form-control value-field custom-class-durationy'][name='60']" ,"6");
	    		objectR.fn_stepFourTotalWorkYears("[class='form-control value-field custom-class-durationy'][name='62']" ,"10");
	    		objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[4]");
	    		Thread.sleep(1000);
	    		
	    	//	objectR.fn_YesNoLap("#form_group_10 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)", 2);
	    		Thread.sleep(1000);
	    		objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[5]");
	    		Thread.sleep(1000);
	    	//	objectR.fn_completeSixthStage();
	    		//objectR.fn_YesNo("#form_group_10 > div.formOneBlock.col-xs-12.col-sm-6.col-md-6.col-lg-6 > div > select", 2);
	    		Thread.sleep(1000);
	    		objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[6]");
	    		Thread.sleep(1000);    		
	    		//objectR.fn_completeSeventhStage(); 
	    		 Thread.sleep(2000);
	    		 objectR.fn_stepThreeTenureYears("#form_group_6 > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > input:nth-child(1)" ,"3");
	    		 Thread.sleep(2000);
	    		 objectR.fn_stepThreeTenureMonths("#form_group_6 > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > input:nth-child(2)" ,"0");
	    		 Thread.sleep(2000);
	    		// objectR.fn_YesNo("[class='form-control value-field'][name='94']",2);
	    		 Thread.sleep(1000);
	    		// fn_stepOneContinueButton("#form_group_6 > div:nth-child(3) > button:nth-child(2)");
	    		objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[7]");
	    		objectR.fn_thankYouPage();
	    		Thread.sleep(1000);
	    		driver.navigate().to("http://website.rubique.com");
	    		
			}
	
	
	}		
	
	

}
