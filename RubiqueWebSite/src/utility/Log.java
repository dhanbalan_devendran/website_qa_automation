package utility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Log {
	public static String workingDir = System.getProperty("user.dir");
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	public static String log4jPath1 = workingDir+"//src//utility//log4j.xml" ;
	public static String log4jName =workingDir + "\\logs\\"+ "log4j"+timeStamp+".log" ;
		
	public static void setLogfilePath() throws SAXException, IOException, ParserConfigurationException, XPathExpressionException, TransformerFactoryConfigurationError, TransformerException
	{
		
		System.out.println("Scripts setLogfilePath started:-->"+LocalDateTime.now());
		System.out.println("log4jPath1"+log4jPath1);
		System.out.println("log4jName"+log4jName);
	// 1- Build the doc from the XML file
	Document doc = DocumentBuilderFactory.newInstance()
	            .newDocumentBuilder().parse(new InputSource(log4jPath1));

	// 2- Locate the node(s) with xpath
	XPath xpath = XPathFactory.newInstance().newXPath();
	NodeList nodes = (NodeList)xpath.evaluate("//*[contains(@name, 'File')]",
	                                          doc, XPathConstants.NODESET);
      System.out.println("nodelist->"+nodes);
	// 3- Make the change on the selected nodes
	for (int idx = 0; idx < nodes.getLength(); idx++) {
	    Node value = nodes.item(idx).getAttributes().getNamedItem("value");
	    System.out.println("value-/>"+value);
	    String valbefore = value.getNodeValue();
	    System.out.println("log4jName-/>"+log4jName);
	    String finalLogName = log4jName.replace("\\", "\\\\");
	    System.out.println("finalLogName-/>"+finalLogName);
	    value.setNodeValue(finalLogName);
	    String valAfter= value.getNodeValue();
	    System.out.println("valAfter"+valAfter);
	  	}
	// 4- Save the result to a new XML doc
		Transformer xformer = TransformerFactory.newInstance().newTransformer();
		xformer.transform(new DOMSource(doc), new StreamResult(new File(log4jPath1)));
	//	xformer.setParameter("File", value);
		DOMConfigurator.configure(log4jPath1) ;
}
	
	
//Initialize Log4j logs

	 private static Logger Log = Logger.getLogger(Log.class.getName());//

// This is to print log for the beginning of the test case, as we usually run so many test cases as a test suite

public static void startTestCase(String sTestCaseName) throws XPathExpressionException, SAXException, IOException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException{
	
	System.out.println("Scripts startTestCase started:-->"+LocalDateTime.now());
	setLogfilePath();
	Log.info("****************************************************************************************");

	Log.info("****************************************************************************************");

	Log.info("$$$$$$$$$$$$$$$$$$$$$                 "+sTestCaseName+ "       $$$$$$$$$$$$$$$$$$$$$$$$$");

	Log.info("****************************************************************************************");

	Log.info("****************************************************************************************");

	}

	//This is to print log for the ending of the test case

public static void endTestCase(String sTestCaseName){
	System.out.println("Scripts endTestCase started:-->"+LocalDateTime.now());
	Log.info("XXXXXXXXXXXXXXXXXXXXXXX             "+"-E---N---D-"+"             XXXXXXXXXXXXXXXXXXXXXX");

	Log.info("X");

	Log.info("X");

	Log.info("X");

	Log.info("X");

	}

	// Need to create these methods, so that they can be called  

public static void infoLog(String message) {
	System.out.println("Scripts info started:-->"+LocalDateTime.now());
		Log.info(message);

		}

public static void warn(String message) {
	System.out.println("Scripts warn started:-->"+LocalDateTime.now());
   Log.warn(message);

	}

public static void error(String message) {
	System.out.println("Scripts error started:-->"+LocalDateTime.now());
   Log.error(message);

	}

public static void fatal(String message) {
	System.out.println("Scripts fatal started:-->"+LocalDateTime.now());
   Log.fatal(message);

	}

public static void debug(String message) {
	System.out.println("Scripts debug started:-->"+LocalDateTime.now());
   Log.debug(message);

	}

}