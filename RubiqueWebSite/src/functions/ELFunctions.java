package functions;

/** 
 * Author: Trupti Bhosale created on 2017-06-17
 * This file contains all functions for all fields needed for executing Business loan product flow
 * */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.io.FileInputStream;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Hashtable;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ELFunctions
{
	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileIBl1 = workingDir + "/RubiqueWebSite/inputFiles/BLtestdataFooterFlow.xls";
	public static String OutputSnapshoots= workingDir + "/outputFiles" ;
	public static WebDriver driver;
	boolean present;
	String Actualtext,Actualtext1;
	String LaonAmount,years,months,companName,occupation;
	public String locatorValue;
	String dataValue;
	boolean productFlag = true;
	static String emailid;
	static String Leademailid;
	String mobileNumber1;
	public static Workbook wb;
	public static Sheet ws;
	public static Sheet ws1;
	public static Sheet ws2;
	public static Sheet ws3;
	public static FileInputStream fs;
	//public  locatorValue;
	
	
	public ELFunctions(WebDriver ELDriver)
	{
		
		ELFunctions.driver = driver;
	}
	
	public ELFunctions() {
	}
	
	
	public  WebDriver fn_setUpChrome()
	{
		
		workingDir = cromeDrivePath;
		System.out.println(workingDir);
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		
		//Initialize browser
		 driver=new ChromeDriver();
		driver.manage().window().maximize();
		System.out.println("fn_setUpChrome");
		return driver ;
	}
	
	
	public  void fn_openUrl(String URL)
	{
		
		fn_setUpChrome();
		driver.get(URL);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		
			e.printStackTrace();
		}
		System.out.println("fn_openUrl");
		
	}
	
	
	public void fn_loginButton(String locator) throws InterruptedException
	{
		locator=locator.replaceAll("^\"|\"$", "");
		Thread.sleep(5000);		
		WebElement login= driver.findElement(By.cssSelector(locator));
		login.click();	
		System.out.println("fn_loginButton");		
	}
	
	public void fn_LoginEmail(String locator) throws InterruptedException
	{
		Thread.sleep(5000);	
		String Login="Praveen.kumar@rubique.com";
		locator=locator.replaceAll("^\"|\"$", "");		
		WebElement emailIds= driver.findElement(By.cssSelector(locator));
		emailIds.sendKeys(Login);	
		System.out.println("fn_LoginEmail");
	}
	
	public void fn_Password(String locator, String Pswd) throws InterruptedException 
	{
			Thread.sleep(3000);
			Pswd=Pswd.replaceAll("^\"|\"$", "");
			WebElement Password= driver.findElement(By.cssSelector(locator));
			Password.sendKeys(Pswd);		
			System.out.println("fn_Password");
	}
	
	 public void fn_completeFirstStepEmploymentType(String locator)
	 {
			WebElement EmploymentType = driver.findElement(By.cssSelector(locator));
			EmploymentType.click();
			System.out.println("fn_stepEightPropertyArea");
	}
	///////First STEP
	public void fn_productLink(String locator) throws InterruptedException
	{
		locator=locator.replaceAll("^\"|\"$", "");
		Thread.sleep(5000);		
		WebElement bl_footerLink= driver.findElement(By.xpath(locator));
		bl_footerLink.click();	
		System.out.println("fn_productLink");		
	}
	
	
    public void fn_individualProduct(String locator) throws InterruptedException
	{
				 	
			 locator = locator.replaceAll("^\"|\"$", ""); 
			 Thread.sleep(5000);
			 WebElement Product= driver.findElement(By.xpath(locator));
			 Product.click();				
	}
		 
	public void fn_checkEligibility(String locator) throws InterruptedException
	{
		locator=locator.replaceAll("^\"|\"$", "");
		Thread.sleep(1000);	
		WebElement checkEligibilityButton= driver.findElement(By.cssSelector(locator));
		checkEligibilityButton.click();	
		System.out.println("fn_checkEligibility");
	}
	


	public void fn_stepOneMobileNumber(String locator, String mobileNumber) throws InterruptedException 
	{
			Thread.sleep(3000);
			mobileNumber=mobileNumber.replaceAll("^\"|\"$", "");
			WebElement mbNumber= driver.findElement(By.cssSelector(locator));
			mbNumber.clear();
			mbNumber.sendKeys(mobileNumber);		
			System.out.println("fn_stepOneMobileNumber");
	}
	
	public void fn_stepOneEmail(String locator) throws InterruptedException
	{
		Thread.sleep(5000);	
		emailid=randomEmail(); 		
		locator=locator.replaceAll("^\"|\"$", "");		
		WebElement emailIds= driver.findElement(By.cssSelector(locator));
		emailIds.sendKeys(emailid);	
		System.out.println("fn_stepOneEmail");
	}
	
	public void fn_stepOneLeadEmail(String locator) throws InterruptedException
	{
		
		Leademailid=randomEmail(); 		
		locator=locator.replaceAll("^\"|\"$", "");		
		WebElement emailIds= driver.findElement(By.cssSelector(locator));
		emailIds.clear();
		emailIds.sendKeys(Leademailid);	
		System.out.println("fn_stepOneLeadEmail");
	}
	
	 public static String randomEmail() 
	 {	
		 Random randomGenerator = new Random();  
		 int randomInt = randomGenerator.nextInt(1000);  
		 return "username"+ randomInt +"@gmail.com";  
	 } 


	public void fn_stepContinueButton(String locator) throws InterruptedException
	{
		Thread.sleep(1000);	
		locator=locator.replaceAll("^\"|\"$", "");
		
		WebElement submitButton= driver.findElement(By.xpath(locator));
		submitButton.click();		
		System.out.println("fn_stepContinueButton");
	}
	
	public void fn_stepTwoFullName(String locator , String fullname ) throws InterruptedException
	{
			
				locator=locator.replaceAll("^\"|\"$", "");
				fullname=fullname.replaceAll("^\"|\"$", "");	
				Thread.sleep(1000);	
				WebElement fullName= driver.findElement(By.cssSelector(locator));
				fullName.sendKeys(fullname);
				Thread.sleep(1000);	
				System.out.println("fn_stepTwoFullName");
	}
	
	public void fn_stepTwoDOB(String locator , String dob ) throws InterruptedException
	{
		          
			locator=locator.replaceAll("^\"|\"$", "");
			dob=dob.replaceAll("^\"|\"$", "");		   
			WebElement DOB= driver.findElement(By.cssSelector(locator));
			DOB.sendKeys(dob);
			Thread.sleep(1000);	
			System.out.println("fn_stepTwoDOB");
	}
	
	public void fn_stepTwoSelectGenderId(String locator , String gender ) throws InterruptedException
	{
		locator=locator.replaceAll("^\"|\"$", "");
		gender=gender.replaceAll("^\"|\"$", "");	
		Select  Gender= new Select ( driver.findElement( By.cssSelector(locator) ) ); 
		Gender.selectByVisibleText(gender);
		Thread.sleep(1000);	
		System.out.println("fn_stepTwoSelectGenderId");
	}
	
	public void fn_stepTwoSelectMaritalStatusId(String locator , String marital ) throws InterruptedException
	{
		locator=locator.replaceAll("^\"|\"$", "");
		marital=marital.replaceAll("^\"|\"$", "");
		Select MartialStatus= new Select ( driver.findElement(By.cssSelector(locator)));
		MartialStatus.selectByVisibleText(marital);
		Thread.sleep(1000);	
		System.out.println("fn_stepTwoSelectMaritalStatusId");
	}
	
	public void fn_stepTwoSelectNationality(String locator , String nationality ) throws InterruptedException
	{
		locator=locator.replaceAll("^\"|\"$", "");
		nationality=nationality.replaceAll("^\"|\"$", ""); 
		Select Nationality=  new Select ( driver.findElement(By.cssSelector(locator)));
		Nationality.selectByVisibleText(nationality);
		Thread.sleep(1000);	
		System.out.println("fn_stepTwoSelectNationality");
	}
	
	public void fn_stepTwoPAN(String locator , String pan ) throws InterruptedException
	{
		locator=locator.replaceAll("^\"|\"$", "");
		pan=pan.replaceAll("^\"|\"$", "");	
		WebElement PANindia= driver.findElement(By.cssSelector(locator));
		PANindia.sendKeys(pan);
		Thread.sleep(1000);	
		System.out.println("fn_stepTwoPAN");
	}
	
	public void fn_stepTwoAddress(String locator , String address ) throws InterruptedException
	{
			Thread.sleep(1000);	
			locator=locator.replaceAll("^\"|\"$", "");
			address=address.replaceAll("^\"|\"$", "");	
			WebElement Address1= driver.findElement(By.cssSelector(locator));
			Address1.sendKeys(address);
			Thread.sleep(1000);	
			System.out.println("fn_stepTwoAddress");
	}
	
	public void fn_stepTwoPermanentAddress(String locator , String address ) throws InterruptedException
	{
			Thread.sleep(1000);	
			locator=locator.replaceAll("^\"|\"$", "");
			address=address.replaceAll("^\"|\"$", "");	
			WebElement Address1= driver.findElement(By.xpath(locator));
			Address1.clear();
			Address1.sendKeys(address);
			Thread.sleep(1000);	
			System.out.println("fn_stepTwoAddress");
	}
	
	public void fn_stepTwoSelectCityId(String locator , String city ) throws InterruptedException
	{
				locator=locator.replaceAll("^\"|\"$", "");
				city=city.replaceAll("^\"|\"$", ""); 
				Select City=  new Select ( driver.findElement(By.cssSelector(locator)));				
				City.selectByVisibleText(city);				
				Thread.sleep(2000);	
				System.out.println("fn_stepTwoSelectCityId");
	}
	
	public void fn_stepTwoPincode(String locator , String pincode ) throws InterruptedException
	{
			locator=locator.replaceAll("^\"|\"$", "");
			pincode=pincode.replaceAll("^\"|\"$", "");	
			WebElement Pincode= driver.findElement(By.cssSelector(locator));
			Pincode.sendKeys(pincode);
			Thread.sleep(1000);	
			System.out.println("fn_stepTwoPincode");
	}
	

	/////////Third STEP
	public void fn_stepThreeLoanAmount(String locator , String loanAmounts ) throws InterruptedException
	 {
				Thread.sleep(1000); 
				locator=locator.replaceAll("^\"|\"$", "");
				 loanAmounts=loanAmounts.replaceAll("^\"|\"$", ""); 				
				 WebElement loanAmount= driver.findElement(By.cssSelector(locator));	
				 loanAmount.clear();
				 loanAmount.sendKeys(loanAmounts);	 
				 Thread.sleep(1000);	
				 System.out.println("fn_stepThreeLoanAmount");
	} 
	 public void fn_stepThreeTenureYears(String locator , String yearsTenure ) throws InterruptedException
	 {
		 			locator=locator.replaceAll("^\"|\"$", "");
		 			yearsTenure=yearsTenure.replaceAll("^\"|\"$", ""); 
					WebElement tenureYears= driver.findElement(By.cssSelector(locator));					
					tenureYears.clear();
					tenureYears.sendKeys(yearsTenure);
					years= driver.findElement(By.cssSelector(locator)).getAttribute(yearsTenure);
					Thread.sleep(1000);	
					System.out.println("fn_stepThreeTenureYears");
	} 
	 public void fn_stepThreeTenureMonths(String locator , String yearsMonth ) throws InterruptedException
	 {
		 			locator=locator.replaceAll("^\"|\"$", "");
		 			yearsMonth=yearsMonth.replaceAll("^\"|\"$", ""); 
					WebElement tenureMonths= driver.findElement(By.cssSelector(locator));				
					tenureMonths.clear();
					tenureMonths.sendKeys(yearsMonth);
					months= driver.findElement(By.cssSelector(locator)).getAttribute(yearsMonth);
					Thread.sleep(1000);	
					System.out.println("fn_stepThreeTenureMonths");
	} 
	 
	
	//////////STEP Four
	
	 public void fn_stepFourCompetitiveExamAppeared(String locator , String Exam ) throws InterruptedException
		{
		 		Thread.sleep(1000);	
		 		locator=locator.replaceAll("^\"|\"$", "");
				Exam=Exam.replaceAll("^\"|\"$", ""); 
				Select ExamAppeared=  new Select ( driver.findElement(By.cssSelector(locator)));
				ExamAppeared.selectByVisibleText(Exam);
				Thread.sleep(1000);	
				System.out.println("fn_stepFourCompetitiveExamAppeared");
		}
	
	 public void fn_stepFourMarks(String locator, String Mark) throws InterruptedException 
		{
				//Thread.sleep(3000);
				locator=locator.replaceAll("^\"|\"$", "");
				Mark=Mark.replaceAll("^\"|\"$", "");
				WebElement marks= driver.findElement(By.cssSelector(locator));
				marks.sendKeys(Mark);		
				System.out.println("fn_stepFourMarks");
		}
	
	 public void fn_stepFourCountryStudy(String locator , String Study ) throws InterruptedException
		{
				locator=locator.replaceAll("^\"|\"$", "");
				Study=Study.replaceAll("^\"|\"$", ""); 
				Select CountryStudy=  new Select ( driver.findElement(By.cssSelector(locator)));
				CountryStudy.selectByVisibleText(Study);
				Thread.sleep(1000);	
				System.out.println("fn_stepFourCountryStudy");
		}
	
	 public void fn_stepFourAdmissionstatus(String locator , String status ) throws InterruptedException
		{
				locator=locator.replaceAll("^\"|\"$", "");
				status=status.replaceAll("^\"|\"$", ""); 
				Select Admissionstatus=  new Select ( driver.findElement(By.cssSelector(locator)));
				Admissionstatus.selectByVisibleText(status);
				Thread.sleep(1000);	
				System.out.println("fn_stepFourAdmissionstatus");
		}
	
	 public void fn_stepFourCourseName(String locator , String name ) throws InterruptedException
		{
				locator=locator.replaceAll("^\"|\"$", "");
				name=name.replaceAll("^\"|\"$", ""); 
				Select CourseName=  new Select ( driver.findElement(By.cssSelector(locator)));
				CourseName.selectByVisibleText(name);
				Thread.sleep(1000);	
				System.out.println("fn_stepFourCourseName");
		}
	
	 public void fn_stepFourCollegeName(String locator , String name ) throws InterruptedException
		{
				locator=locator.replaceAll("^\"|\"$", "");
				name=name.replaceAll("^\"|\"$", ""); 
				Select CollegeName=  new Select ( driver.findElement(By.cssSelector(locator)));
				CollegeName.selectByVisibleText(name);
				Thread.sleep(1000);	
				System.out.println("fn_stepFourCollegeName");
		}
	 
	 public void fn_stepFourCollegeCity(String locator , String City) throws InterruptedException
		{
				Thread.sleep(1000);	
				locator=locator.replaceAll("^\"|\"$", "");
				City=City.replaceAll("^\"|\"$", "");	
				WebElement CollegdeCity= driver.findElement(By.cssSelector(locator));
				CollegdeCity.sendKeys(City);
				Thread.sleep(1000);	
				System.out.println("fn_stepFourCollegdeCity");
		}
	 
	 public void fn_stepFourCourseNature(String locator , String nature) throws InterruptedException
		{
				locator=locator.replaceAll("^\"|\"$", "");
				nature=nature.replaceAll("^\"|\"$", ""); 
				Select CourseNature=  new Select ( driver.findElement(By.cssSelector(locator)));
				CourseNature.selectByVisibleText(nature);
				Thread.sleep(1000);	
				System.out.println("fn_stepFourCourseNature");
		}
	 
	 public void fn_stepFourTypeCourse(String locator , String Type) throws InterruptedException
		{
				locator=locator.replaceAll("^\"|\"$", "");
				Type=Type.replaceAll("^\"|\"$", ""); 
				Select TypeCourse=  new Select ( driver.findElement(By.cssSelector(locator)));
				TypeCourse.selectByVisibleText(Type);
				Thread.sleep(1000);	
				System.out.println("fn_stepFourTypeCourse");
		}
	 
	 ///Fifth step
	 public void fn_YesNo(String locator,String YesNo) throws InterruptedException
		{	
			
		  	Thread.sleep(1000);
		  	locator=locator.replaceAll("^\"|\"$", "");
			YesNo=YesNo.replaceAll("^\"|\"$", ""); 
	 		Select YesNoans =new Select(driver.findElement(By.cssSelector(locator))); //ChequeBounce
	 		YesNoans.selectByVisibleText(YesNo);
	 		System.out.println("fn_YesNo");
		}
	 
	 public void fn_stepFifthSecurityOffered(String locator , String Security) throws InterruptedException
		{
				locator=locator.replaceAll("^\"|\"$", "");
				Security=Security.replaceAll("^\"|\"$", ""); 
				Select SecurityOffered=  new Select ( driver.findElement(By.cssSelector(locator)));
				SecurityOffered.selectByVisibleText(Security);
				Thread.sleep(1000);	
				System.out.println("fn_stepFifthSecurityOffered");
		}
	 
	 /////sixth Step
	 public void fn_stepSixthOccupation(String locator , String Occupations) throws InterruptedException
	 {
		 		 
		 Thread.sleep(1000);	
		 locator=locator.replaceAll("^\"|\"$", "");
		 Occupations=Occupations.replaceAll("^\"|\"$", ""); 
		 Select Occupation=new Select(driver.findElement(By.cssSelector(locator)));
		 Occupation.selectByVisibleText(Occupations);
 		 System.out.println("fn_stepSixthOccupation");
	 }	
	 
	 public void fn_stepSixthCompanyName(String locator , String companyName) throws InterruptedException
	 {
		 Thread.sleep(1000);
		 locator=locator.replaceAll("^\"|\"$", "");
		 companyName=companyName.replaceAll("^\"|\"$", ""); 		 
		 WebElement companyNames= driver.findElement(By.cssSelector(locator));	
		// companyNames.clear();
		 companyNames.sendKeys(companyName);
		 System.out.println("fn_stepSixthCompanyName");
	 } 
	 
	 public void fn_stepsixthgrossMonthlyIncome(String locator , String Income) throws InterruptedException
	 {
		 Thread.sleep(1000);
		 locator=locator.replaceAll("^\"|\"$", "");
		 Income=Income.replaceAll("^\"|\"$", ""); 
		 WebElement grossMonthlyIncome= driver.findElement(By.cssSelector(locator));
		 grossMonthlyIncome.clear();
		 grossMonthlyIncome.sendKeys(Income);		
		 System.out.println("fn_stepsixthgrossMonthlyIncome");
	 } 
	 
	 ///// seventh step
	 public void fn_stepSevenExistingEMI(String locator , String EMI ) throws InterruptedException
	 {
		 	Thread.sleep(1000);
		 	locator=locator.replaceAll("^\"|\"$", "");
		 	EMI=EMI.replaceAll("^\"|\"$", ""); 
			WebElement ExistingEMI= driver.findElement(By.cssSelector(locator));
			ExistingEMI.sendKeys(EMI);			
			System.out.println("fn_stepSevenExistingEMI");
	 }  
	 
	 /////eight step
	 
	 public void fn_stepEightcoApplicantRelationship(String locator , String Relation)
		{
		 	locator=locator.replaceAll("^\"|\"$", "");
		 	Relation=Relation.replaceAll("^\"|\"$", ""); 
		 	Select Relationship =new Select(driver.findElement(By.cssSelector(locator))); //CoApplicant
		 	Relationship.selectByVisibleText(Relation);
	 		System.out.println("fn_stepEightcoApplicantRelationship");
		}
	 
	 ///nine step
	 public void fn_stepNinePrimaryExistingBank(String locator , String PExistingBank) throws InterruptedException, ClassNotFoundException, SQLException
		{
		 Thread.sleep(1000);
		 locator=locator.replaceAll("^\"|\"$", "");
		 PExistingBank=PExistingBank.replaceAll("^\"|\"$", ""); 
		 Select PrimaryExistingBank =new Select(driver.findElement(By.cssSelector(locator))); //PrimaryExistingBank
		 PrimaryExistingBank.selectByVisibleText(PExistingBank);	
		 System.out.println("fn_stepNinePrimaryExistingBank");
		
		}
	 
	 public void fn_HeaderFlow(String Productlocator,String LoanLocator,String locator) throws InterruptedException
		{
			
			Thread.sleep(5000);
		//	driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);
			//Thread.sleep(1000);
			Actions action = new Actions(driver);
					
			WebElement Product = driver.findElement(By.cssSelector(Productlocator));
			action.moveToElement(Product).click().build().perform();
			
			driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);
		
			WebElement SMELoan = driver.findElement(By.cssSelector(LoanLocator));
			
			WebElement LoanType = driver.findElement(By.cssSelector(locator));
			Thread.sleep(5000);
			//action.moveToElement(SMELoan).moveToElement(driver.findElement(By.cssSelector("#menu-item-183 > ul:nth-child(2) > li:nth-child(1) > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1) > span:nth-child(1)"))).click().build().perform();
			driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);
			action.moveToElement(SMELoan).moveToElement(LoanType).click().build().perform();
			
			
			
			 
		}
	 
	 
	 public void fn_dbVerificationcode(String dbName) throws ClassNotFoundException, SQLException, InterruptedException
	 {
		 Thread.sleep(30000);
		  if (driver.findElement(By.cssSelector("[id='verify-lead']")).isDisplayed())
		  {		  
				String dbUrl = dbName;	
				//Database Username		
				String username = "root";  
		        
				//Database Password		
				String password = "Rub!cR00t"; 			
	
				String query = "SELECT email_otp FROM rubique_api.login  where email = ?;";
				
		 	    //Load mysql jdbc driver		
				 Class.forName("com.mysql.jdbc.Driver");			
		   
		   		//Create Connection to DB		
		    	Connection con = DriverManager.getConnection(dbUrl,username,password);
		    	
		    	PreparedStatement ps = con.prepareStatement(query);
		    	ps.setString(1,Leademailid);  
		    	

				System.out.println("lead email :" +ps);
		    	
		         ResultSet rs = ps.executeQuery();
		    	
		         
		  		//Create Statement Object		
		    	Statement stmt = con.createStatement();					
	
					// Execute the SQL Query. Store results in ResultSet		
		 	
		 		 while (rs.next())
		 		 {
				       String VerID = rs.getString("email_otp"); 
				       
				     
				       System.out.println("Verification Code is :" +VerID);
				
				       driver.findElement(By.cssSelector("[id='verify-lead']")).sendKeys(VerID);
				       System.out.println("Element is Visible");
		         }
		 	
		 		Thread.sleep(1000);
				con.close();
				
				driver.getCurrentUrl();							
		  }	
				  		
			  Thread.sleep(1000);
		  	
		  	
	 }
	 
	 public void fn_productMatch(String locator) throws InterruptedException
	 {
		 WebDriverWait wait = new WebDriverWait(driver,30);
		 wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));  
		
		 WebElement ProductMatch= driver.findElement(By.cssSelector(locator));
		 ProductMatch.click();
	 }
	 
}

	 