package functions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PlFunctions {
	static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFilePL1 = workingDir + "/RubiqueWebSite/inputFiles/pl_data_productflow_20170620_bk_01.xls";
	public static WebDriver driver;
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	public static String screenSnapsPath = workingDir + "/outputFiles/" + "pl" + timeStamp + ".png";
	
	
	public WebDriver fn_setUpChrome() {

		workingDir = cromeDrivePath;
		System.out.println(workingDir);
		// Setting the property to enable the chrome driver
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		// Initialize browser
		driver = new ChromeDriver();

		// wait = new WebDriverWait(driver, 5);
		driver.manage().window().maximize();
		System.out.println("fn_setUpChrome");
		return driver;
	}

	public void fn_takeScreenshot(WebDriver driver, String path) {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(path));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("fn_takeScreenshot taken check at: " + path);
	}
	
	public void fn_openUrl(String URL) {
		driver.get(URL);
		// Maximize browser
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("fn_openUrl");

	}

	public PlFunctions() {

	}

	public PlFunctions(WebDriver driver) {

		PlFunctions.driver = driver;
	}
	
	public void fn_homeLoginClick(String locator) // click on homepage login
	{
		System.out.println("locator"+locator);
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.click();
        /*WebElement Next = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		Next.click();*/
	}

	public void fn_emailId(String locator, String email) throws InterruptedException// filling email
	{
		Thread.sleep(1000);
		System.out.println("email"+email);
		System.out.println("locator"+locator);
		WebElement emailid = driver.findElement(By.cssSelector(locator));
		emailid.sendKeys(email);
	}

	public void fn_password(String locator, String pass) // filli g password
	{
		System.out.println("pass"+pass);
		System.out.println("locator"+locator);
		WebElement password = driver.findElement(By.cssSelector(locator));
		password.sendKeys(pass);
	}

	public void fn_login(String locator) // click on login button
	{
		System.out.println("locator"+locator);
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.click();
	}

	public void fn_clickProduct(String linkText) throws InterruptedException 
	{
	
		Thread.sleep(2500);
		WebElement clickProduct = driver.findElement(By.cssSelector(linkText));
		clickProduct.click();
	}

	//////..................................Header flow..........................................
    

	public void fn_Product(String locator) throws InterruptedException
	{
		Thread.sleep(2000);
		 WebElement clickproduct = driver.findElement(By.xpath(locator));
		 clickproduct.click();
		System.out.println("HeaderDropDownProduct");
	}	
	
	
	public void fn_ProductConsumerheader(String PL) throws InterruptedException{
		Thread.sleep(1000);
		WebElement subMenu = driver.findElement(By.xpath(PL));
		Actions act =new Actions(driver);
	    act.moveToElement(subMenu);
	    act.moveToElement(subMenu).build().perform();
	    
	}  
	

	 public void fn_selectPLHeader(String Goupward) throws InterruptedException // click on HDFC CC
	 {
		 Thread.sleep(2000);
		 WebElement selectCreditcard = driver.findElement(By.linkText(Goupward));
		 selectCreditcard.click();
	 }
	 public void fn_mainDropDownProduct(String mainProd, String subProd, String prod) throws InterruptedException {
		 Thread.sleep(5000);
			System.out.println("mainProd + subProd + prod"+mainProd + subProd + prod);
			// Default set the locator
			if (mainProd.equals(null)) {
				System.out.println(" mainProd is null ");
				// mainProd= "#menu-item-7 > a > span" ;
				mainProd = "Products";
			}
			if (subProd.equals(null)) {
				System.out.println(" subProd is null ");
				subProd = "Consumer Loans";
			}
			if (prod.equals(null)) {
				System.out.println(" subProd is null ");
				prod = "Aditya Birla Finance Limited";
			}
			// WebElement c2=
			// step 1
			Actions actionProduct = new Actions(driver);
			WebElement c1 = driver.findElement(By.partialLinkText(mainProd));
			if (c1.isDisplayed()) {
				System.out.println("Element1 is Visible");
				actionProduct.clickAndHold(c1).perform();
				Thread.sleep(4000);
			} else {
				System.out.println("Element1 is InVisible");
			}

			// step 2
			WebElement c2 = driver.findElement(By.linkText(subProd));
			if (c2.isDisplayed()) {
				System.out.println("Element2 is Visible");
				actionProduct.clickAndHold(c2).perform();
				Thread.sleep(5000);
			} else {
				System.out.println("Element2 is InVisible");
			}
			// step 3
			WebElement c3 = driver.findElement(By.partialLinkText(prod));
			if (c3.isDisplayed()) {
				System.out.println("Element3 is Visible");
				actionProduct.click(c3).perform();
				Thread.sleep(4000);
			} else {
				System.out.println("Element3 is InVisible");
			}

			System.out.println("fn_mainDropDownProduct");
	 }
	 
	 //...............................................................................
	 //....................Middle Flow........................
	 
	 public void  fn_middlePL(String locator) throws InterruptedException 
	 {
		 Thread.sleep(5000);
		 WebElement middlePL = driver.findElement(By.xpath(locator));
		 middlePL.click();
	 }
	 
	 public void fn_LoanAmount(String locator, String amount) throws InterruptedException
	 {
		 Thread.sleep(5000);
		 WebElement loanAmount= driver.findElement(By.xpath(locator));
		 loanAmount.sendKeys(amount);
	 }
	 
	 public void fn_clickNext(String locator)
	 {
		 WebElement clickNext = driver.findElement(By.xpath(locator));
		 clickNext.click();
				 
	 }
	
	 public void fn_selectPLdMiddle(String locator) throws InterruptedException
	 {
		 WebDriverWait wait = new WebDriverWait(driver,30);
		 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));  
		 /*examining the css for a search     
		 box*/
		 driver.findElement(By.xpath(locator)).click(); 
		 }
		 /*WebElement selectPlMiddle = driver.findElement(By.xpath(locator));
		 selectPlMiddle.click();*/
	 
	
		
	
	
	public void fn_selectPl(String locator) throws InterruptedException // select pl offer
	{
		Thread.sleep(1500);
		WebElement selectPl = driver.findElement(By.cssSelector(locator));
		selectPl.click();
	}

	public void fn_checkEligibility(String RBLBank) throws InterruptedException // click on eligibility
	{
		Thread.sleep(1500);
		WebElement checkEligibility = driver.findElement(By.cssSelector(RBLBank));
		checkEligibility.click();
	}

	public void fn_completeFirstStepMobile(String locator, String Mobile) throws InterruptedException
	{
		Thread.sleep(5000);
		WebElement completeFirstStepMobile = driver.findElement(By.cssSelector(locator));
		completeFirstStepMobile.clear();
		completeFirstStepMobile.sendKeys(Mobile);
	}

	public void fn_completeFirstStepEmail(String locator, String Email) throws InterruptedException// enter
																		// mail
	{
		Thread.sleep(1000);
		WebElement completeFirstStepEmail = driver.findElement(By.cssSelector(locator));
		completeFirstStepEmail.clear();
		completeFirstStepEmail.sendKeys(Email);
	}

	
	public void fn_completeFirstStepContinue(String click) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver,30);
		   wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(click)));  
		   driver.findElement(By.xpath(click)).click();

		}
	
	public void fn_completeSecondStepFullName(String locator, String pass) throws InterruptedException
	{
		Thread.sleep(2000);
		WebElement completeSecondStepFullName = driver.findElement(By.cssSelector(locator));
		completeSecondStepFullName.sendKeys(pass);
		
		 }
	

	public void fn_stepTwoSelectGenderId(String locator, String gender) throws InterruptedException 
	{
		Thread.sleep(1500);
		Select drop0 = new Select(driver.findElement(By.xpath(locator)));
		drop0.selectByVisibleText(gender);
	}

	public void stepTwoSelectDOB(String locator, String pss) throws InterruptedException 
	{
		Thread.sleep(1500);
		WebElement stepTwoSelectDOB = driver.findElement(By.cssSelector(locator));
		stepTwoSelectDOB.sendKeys(pss);
	}

	public void stepTwoSelectMarital(String locator, String Marital) {
		Select drop1 = new Select(driver.findElement(By.cssSelector(locator)));
		drop1.selectByVisibleText(Marital);
	}

	public void fn_stepTwoSelectNationality(String locator, String Nationality) throws InterruptedException 
	{
		Thread.sleep(1500);
		Select drop2 = new Select(driver.findElement(By.cssSelector(locator)));
		drop2.selectByVisibleText(Nationality);
	}
	
	public void fn_stepTwoHighest_Qualification(String locator, String quali) throws InterruptedException
	{
		Thread.sleep(1500);
		Select drop13 = new Select(driver.findElement(By.xpath(locator)));
		drop13.selectByVisibleText(quali);
	}

	public void fn_completeSecondStagePan(String locator, String pann) throws InterruptedException 
	{
		Thread.sleep(1500);
		WebElement completeSecondStagePan = driver.findElement(By.cssSelector(locator));
		completeSecondStagePan.sendKeys(pann);
	}

	public void fn_stepTwoSelectAddress_Line1(String locator, String pss) throws InterruptedException 
	{
		Thread.sleep(1500);
		WebElement stepTwoSelectAddress_Line1 = driver.findElement(By.cssSelector(locator));
		stepTwoSelectAddress_Line1.sendKeys(pss);
	}

	public void fn_StepTwoSelectAddress_Line2(String locator, String pss) throws InterruptedException 
	{
		Thread.sleep(1500);
		WebElement stepTwoSelectAddress_Line2 = driver.findElement(By.cssSelector(locator));
		stepTwoSelectAddress_Line2.sendKeys(pss);
	}
	
	public void fn_StepTwoSelectLandmark(String locator, String ps) throws InterruptedException
	{
		Thread.sleep(1500);
		WebElement StepTwoSelectLandmark = driver.findElement(By.xpath(locator));
		StepTwoSelectLandmark.sendKeys(ps);
	}

	public void stepTwoSelectCity(String locator, String City) throws InterruptedException
	{
		Thread.sleep(2000);
		Select drop3 = new Select(driver.findElement(By.xpath(locator)));
		drop3.selectByVisibleText(City);
	}

	public void stepTwoSelectPincode(String locator, String pss) throws InterruptedException 
	{
		Thread.sleep(1500);
		WebElement stepTwoSelectPincode = driver.findElement(By.xpath(locator));
		stepTwoSelectPincode.sendKeys(pss);
	}

	public void stepTwoSelectYears_at_Current_Residence(String locator, String pss) throws InterruptedException
	{
		Thread.sleep(1500);
		WebElement stepTwoSelectYears_at_Current_Residence = driver.findElement(By.xpath(locator));
		stepTwoSelectYears_at_Current_Residence.sendKeys(pss);
	}
	
	public void stepTwoTypeOfAccomodation(String locator, String owned) throws InterruptedException
	{
		//Thread.sleep(1500);
		Select accomodation = new Select(driver.findElement(By.xpath(locator)));
		accomodation.selectByVisibleText(owned);
	}

	public void stepTwoSelectContinue(String locator) throws InterruptedException
	{
		fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(1500);
		WebElement stepTwoSelectContinue = driver.findElement(By.xpath(locator));
		stepTwoSelectContinue.click();
	}
	
	public void fn_stepLoanAmountRequired(String locator, String Amount) throws InterruptedException
	{
		Thread.sleep(1500);
		WebElement stepLoanAmountRequired = driver.findElement(By.xpath(locator));
        stepLoanAmountRequired.clear();
		stepLoanAmountRequired.sendKeys(Amount);
		
	}
	
	public void fn_stepTenure(String locator, String tenure) throws InterruptedException
	{
		Thread.sleep(1500);
		WebElement stepTenure = driver.findElement(By.xpath(locator));
		stepTenure.clear();
		stepTenure.sendKeys(tenure);
	}
	
	public void fn_purposeOfLoan(String locator, String purpose) throws InterruptedException
	{
		Thread.sleep(1500);
	     Select loanp = new Select(driver.findElement(By.xpath(locator)));
	     loanp.selectByVisibleText(purpose);
	}
	
	public void fn_stepSelectContinue(String locator) throws InterruptedException
	{
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement stepSelectContinue = driver.findElement(By.xpath(locator));
		stepSelectContinue.click();
	}
	

	public void fn_completeThirdStageOccuoation(String locator, String occupation) throws InterruptedException 
	{
		Thread.sleep(1500);
		Select drop4 = new Select(driver.findElement(By.xpath(locator)));
		drop4.selectByVisibleText(occupation);

	}

	public void fn_completeFourthStageOccupation(String locator, String Occupation) throws InterruptedException {
		Thread.sleep(2000);
		Select pss = new Select(driver.findElement(By.xpath(locator)));
		pss.selectByVisibleText(Occupation);
	}

	public void fn_completeFourthStageCompany_Name(String locator, String ps) throws InterruptedException 
	{
		Thread.sleep(1500);
		WebElement completeFourthStageCompany_Name = driver.findElement(By.xpath(locator));
		completeFourthStageCompany_Name.sendKeys(ps);
	}

	public void fn_completeFourthStageGross_Monthly_Income(String locator, String ps) throws InterruptedException 
	{
		Thread.sleep(1500);
		WebElement completeFourthStageGross_Monthly_Income = driver.findElement(By.xpath(locator));
		completeFourthStageGross_Monthly_Income.sendKeys(ps);
	}
	
	public void fn_completeFourthStageNet_Monthly_Income(String locator, String ps) throws InterruptedException
	{
		Thread.sleep(1500);
		WebElement completeFourthStageNet_Monthly_Income = driver.findElement(By.xpath(locator));
		completeFourthStageNet_Monthly_Income.sendKeys(ps);
	}

	public void fn_completeForthStageModeOF_salary(String locator, String Salary) throws InterruptedException
	{
		Thread.sleep(1500);
		Select drop5 = new Select(driver.findElement(By.xpath(locator)));
		drop5.selectByVisibleText(Salary);
	}

	public void fn_completeTypeOFCompany(String locator, String company) throws InterruptedException 
	{
		Thread.sleep(1500);
		Select ps = new Select(driver.findElement(By.xpath(locator)));
		ps.selectByVisibleText(company);
	}
	
	
	public void fn_forthStageProfessionType(String locator, String Profession) throws InterruptedException
	{
		Thread.sleep(2000);
		Select drop6 = new Select(driver.findElement(By.xpath(locator)));
		drop6.selectByVisibleText(Profession);
	}
	
	public void fn_forthStageDesignation(String locator, String designation) throws InterruptedException 
	{
		Thread.sleep(1500);
		Select ps1 = new Select(driver.findElement(By.xpath(locator)));
		ps1.selectByVisibleText(designation);
	}
	
	public void fn_completeFourthStageNumber_of_Years_in_Current_Work(String locator, String pss) throws InterruptedException {
		Thread.sleep(1500);
		WebElement completeFourthStageNumber_of_Years_in_Current_Work = driver.findElement(By.xpath(locator));
		completeFourthStageNumber_of_Years_in_Current_Work.sendKeys(pss);
	}

	public void fn_completeFourthStageTotal_Number_of_Years_in_Work(String locator, String pss) throws InterruptedException {
		Thread.sleep(1500);
		WebElement completeFourthStageTotal_Number_of_Years_in_Work = driver.findElement(By.xpath(locator));
		completeFourthStageTotal_Number_of_Years_in_Work.sendKeys(pss);
	}

	public void fn_completeForthStageOffice_Address1(String locator, String pss) throws InterruptedException {
		Thread.sleep(1500);
		WebElement completeForthStageOffice_Address1 = driver.findElement(By.xpath(locator));
		completeForthStageOffice_Address1.sendKeys(pss);
	}

	public void fn_completeForthStageOffice_Address2(String locator, String pss) throws InterruptedException {
		Thread.sleep(1500);
		WebElement completeForthStageOffice_Address2 = driver.findElement(By.xpath(locator));
		completeForthStageOffice_Address2.sendKeys(pss);
	}

	public void fn_completeForthStageOffice_city(String locator, String city) throws InterruptedException
	{
		Thread.sleep(1000);
		Select drop7 = new Select(driver.findElement(By.xpath(locator)));
		drop7.selectByVisibleText(city);
	}

	public void fn_completeForthStageOffice_Pincode(String locator, String pss) throws InterruptedException {
		Thread.sleep(1500);
		WebElement completeForthStageOffice_Pincode = driver.findElement(By.xpath(locator));
		completeForthStageOffice_Pincode.sendKeys(pss);
	}

	public void fn_completeForthStageOffice_Phone(String locator, String pss) throws InterruptedException {
		Thread.sleep(1500);
		WebElement completeForthStageOffice_Phone = driver.findElement(By.xpath(locator));
		completeForthStageOffice_Phone.sendKeys(pss);
	}

	public void fn_completeFourthStageContinue(String locator) {
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement completeFourthStageContinue = driver.findElement(By.xpath(locator));
		completeFourthStageContinue.click();
	}

	public void fn_completeFifthStageAny_ExistingLoanWithBank(String locator, String Existingloan) throws InterruptedException 
	{
		Thread.sleep(1500);
		Select drop8 = new Select(driver.findElement(By.xpath(locator)));
		drop8.selectByVisibleText(Existingloan);
	}
	
	public void fn_completeFifthStageContinue(String locator) throws InterruptedException
	{
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement completeFifthStageContinue = driver.findElement(By.xpath(locator));
		completeFifthStageContinue.click();
	}

	public void fn_completeSixthStagePrimaryExistingBank_Name(String locator, String bank) throws InterruptedException 
	{
		Thread.sleep(1500);
		Select drop9 = new Select(driver.findElement(By.xpath(locator)));
		drop9.selectByVisibleText(bank);
	}
	
	public void fn_completeSixthStageAnyChequeBounceInLast_ThreeMonths(String locator, String cheque) throws InterruptedException
	{
		Thread.sleep(1500);
		Select drop12 = new Select(driver.findElement(By.xpath(locator)));
		drop12.selectByVisibleText(cheque);
	}

	public void fn_completeFinalSubmit(String locator) throws InterruptedException 
	{
		fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(1500);
		WebElement completeFinalSubmit = driver.findElement(By.cssSelector(locator));
		completeFinalSubmit.click();
	}

}
