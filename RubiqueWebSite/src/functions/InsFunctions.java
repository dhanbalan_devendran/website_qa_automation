package functions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import java.util.*;
import java.lang.Exception;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import drivers.InsDrivers;
import jxl.Workbook;

@SuppressWarnings("rawtypes")
public class InsFunctions extends InsDrivers {

	public static String URL = "https://www.rubique.com/two-wheeler-and-bike-loans";
	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileIns1 = workingDir + "/RubiqueWebSite/inputFiles/ins_productflow_20170620_tb_01.xls";
	public static String OutputSnapshoots = workingDir + "/outputFiles";
	public static WebDriver driver;
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	public static String screenSnapsPath = workingDir +"/outputFiles/"+"ins"+timeStamp+".png";
	public InsFunctions(WebDriver driver) {
		this.driver = driver;
	}

	public InsFunctions() {

	}

	public HtmlUnitDriver fn_setUpHtmlUnitDriver()
	{
		 driver = new HtmlUnitDriver ( BrowserVersion.CHROME );
			System.out.println("fn_setUpChrome");
			return (HtmlUnitDriver) driver;
	}
	
	public WebDriver fn_setUpChrome() {
		fn_setUpHtmlUnitDriver();
//		// Setting the property to enable the chrome driver
//	//	System.setProperty("webdriver.chrome.driver", cromeDrivePath);
//
//		// Initialize browser
//		driver = new ChromeDriver();
//
//		driver.manage().window().maximize();
//		driver.manage().deleteAllCookies();
		System.out.println("fn_setUpChrome");
		return driver;
	}

	public void fn_openUrl(String url) {
		System.out.println("fn_openUrl" + url);
		fn_setUpChrome();
		// fn_getdata(0);

		driver.get(url);
		// Maximize browser
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("fn_openUrl");

	}

	public void fn_selectBL() {
		if (driver
				.findElement(
						By.cssSelector("div[class='viewFullDetail purple-btn'] [productslug='tata-capital-home-loan']"))
				.isDisplayed()) {
			System.out.println("Element1 is Visible");

		} else {
			System.out.println("Element1 is InVisible");
		}
		System.out.println("fn_selectBL");
	}

	public void fn_linkTest() {
		//
		ArrayList<String> linkList = new ArrayList<String>();
		// loop over all the a elements in the page
		for (WebElement link : driver.findElements(
				By.cssSelector("div[class='viewFullDetail purple-btn'] [productslug='tata-capital-home-loan']"))) {
			System.out.println("webListLinks" + link.getText());

			// Check if link is displayed and not previously visited
			if (link.isDisplayed() && !linkList.contains(link.getText())) {
				// add link to list of links already visited
				linkList.add(link.getText());
				System.out.println(link.getText());
				// click on the link. This opens a new page
				// link.click();
				// call recursiveLinkTest on the new page
				new InsFunctions(driver).fn_linkTest();
			}
		}
		// driver.navigate().back();
	}

	public void clickLinkByHref(String href) {
		ArrayList<WebElement> anchors;
		System.out.println(href);
		anchors = (ArrayList<WebElement>) driver.findElements(By.tagName("a"));
		System.out.println("1" + anchors);
		Iterator<WebElement> i = anchors.iterator();

		while (i.hasNext()) {
			WebElement anchor = i.next();
			System.out.println(anchor.getAttribute("href"));

			if (anchor.getAttribute("href").contains(href)) {
				System.out.println(anchor.getAttribute("href"));
				System.out.println("3" + anchor);
				anchor.click();

			}
		}
	}

	public void fn_setDownChrome() {
		driver.quit();
		System.out.println("fn_setDownChrome");
	}

	public static void fn_highLightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;

		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {

			System.out.println(e.getMessage());
		}

		js.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", element);

	}

	public void fn_mainDropDownProduct(String mainProd, String subProd, String prod) throws InterruptedException {
		mainProd = mainProd.replaceAll("^\"|\"$", "");
		subProd = subProd.replaceAll("^\"|\"$", "");
		prod = prod.replaceAll("^\"|\"$", "");
		System.out.println(mainProd + subProd + prod);
		// Default set the locator
		if (mainProd.equals(null)) {
			System.out.println(" mainProd is null ");
			// mainProd= "#menu-item-7 > a > span" ;
			mainProd = "Products";
		}
		if (subProd.equals(null)) {
			System.out.println(" subProd is null ");
			subProd = "Consumer Loans";
		}
		if (prod.equals(null)) {
			System.out.println(" subProd is null ");
			prod = "HDB Financial Services";
		}
		// WebElement c2=
		// step 1
		Actions actionProduct = new Actions(driver);
		WebElement c1 = driver.findElement(By.partialLinkText("Login"));
		if (c1.isDisplayed()) {
			System.out.println("Element1 is Visible");
			actionProduct.clickAndHold(c1).perform();
			Thread.sleep(4000);
		} else {
			System.out.println("Element1 is InVisible");
		}

		// step 2
		WebElement c2 = driver.findElement(By.linkText(subProd));
		if (c2.isDisplayed()) {
			System.out.println("Element2 is Visible");
			actionProduct.clickAndHold(c2).perform();
			Thread.sleep(5000);
		} else {
			System.out.println("Element2 is InVisible");
		}
		// step 3
		WebElement c3 = driver.findElement(By.partialLinkText(prod));
		if (c3.isDisplayed()) {
			System.out.println("Element3 is Visible");
			actionProduct.click(c3).perform();
			Thread.sleep(4000);
		} else {
			System.out.println("Element3 is InVisible");
		}

		System.out.println("fn_mainDropDownProduct");
	}

	public void fn_clickCheckEligibility(String eligibility) throws InterruptedException {
		eligibility = eligibility.replaceAll("^\"|\"$", "");

		Actions actionProduct = new Actions(driver);
		WebElement s1 = driver.findElement(By.linkText(eligibility));
		actionProduct.click(s1).perform();
		Thread.sleep(1000);
		System.out.println("Check Button clicked");
		System.out.println("fn_clickCheckEligibility");
	}

	public void fn_stepOneMobileNumber(String mobileNumber, String locator) throws InterruptedException {
		if (locator.equals(null)) {
			locator = "//*[@id='5']";
		}

		if (mobileNumber.equals(null)) {
			mobileNumber = "9870929870";
		}

		WebElement b1 = driver.findElement(By.cssSelector(locator));
		b1.sendKeys(mobileNumber);
		Thread.sleep(1000);
		System.out.println("Step 1a: Passed");

		// checking verification the entered is visible or not
		if (b1.getText().equals(mobileNumber)) {
			Thread.sleep(1000);
			System.out.println("Element1a has mobile no:" + mobileNumber);
		} else {
			Thread.sleep(1000);
			System.out.println("Element1a has mobile no incorrect :" + b1.getText());
		}
		System.out.println("fn_stepOneMobileNumber");
	}

	public void fn_stepOneEmail(String email, String locator) throws InterruptedException {
		if (locator.equals(null)) {
			locator = "//*[@id='6']";
		}

		if (email.equals(null)) {
			email = "email@test.com";
		}

		WebElement b1 = driver.findElement(By.cssSelector(locator));
		b1.sendKeys(email);
		Thread.sleep(1000);
		System.out.println("Step 1a: Passed");

		// checking verification the entered is visible or not
		if (b1.getText().equals(email)) {
			Thread.sleep(1000);
			System.out.println("Element1a has email is:" + email);
		} else {
			Thread.sleep(1000);
			System.out.println("Element1a has email incorrect :" + b1.getText());
		}

		System.out.println("fn_stepOneEmail");

	}

	public void fn_stepOneContinueButton() {
		WebElement v1 = driver.findElement(By.xpath("//*[@id='form_group_3']/div[3]/button[2]/span"));
		v1.click();
		System.out.println("fn_stepOneContinueButton");
	}

	public void fn_clickLoginButton() {
		WebElement v1 = driver.findElement(By.cssSelector("button#login-button.btn-primary btn-lg text-center"));
		v1.click();
		System.out.println("fn_clickLoginButton");
	}

	public void fn_loginMobileNumber(String mobileNumber, String locator) throws InterruptedException {
	
		if (locator.equals(null)) {
			locator = "#login-email";
		}

		if (mobileNumber.equals(null)) {
			mobileNumber = "9870929870";
		}

		WebElement b1 = driver.findElement(By.xpath(locator));
		b1.sendKeys(mobileNumber);
		Thread.sleep(1000);
		System.out.println("Step 1a: Passed");

		// checking verification the entered is visible or not
		if (b1.getText().equals(mobileNumber)) {
			Thread.sleep(1000);
			System.out.println("Element1a has mobile no:" + mobileNumber);
		} else {
			Thread.sleep(1000);
			System.out.println("Element1a has mobile no incorrect :" + b1.getText());
		}
		System.out.println("fn_loginMobileNumber");
	}

	public void fn_loginEmail(String email, String locator) throws InterruptedException {
		if (locator.equals(null)) {
			locator = "input#login-pwd.form-control";
		}

		if (email.equals(null)) {
			email = "email@test.com";
		}

		WebElement f1 = driver.findElement(By.cssSelector(locator));

		driver.switchTo().activeElement();

		if (driver.findElement(By.cssSelector(locator)).isDisplayed())
			{
			System.out.println("pass1 is Visible");
		}
		if (driver.findElement(By.id("login-email")).isDisplayed()) {
			System.out.println("pass2 is Visible");
		}
		if (driver.findElement(By.xpath("//*[@id='login-email']")).isDisplayed())
			;
		{
			System.out.println("pass3 is Visible");
		}

		WebElement b1 = driver.findElement(By.cssSelector(locator));
		b1.sendKeys(email);
		Thread.sleep(1000);
		System.out.println("Step 1a: Passed");

		// checking verification the entered is visible or not
		if (b1.getText().equals(email)) {
			Thread.sleep(1000);
			System.out.println("Element1a has email is:" + email);
		} else {
			Thread.sleep(1000);
			System.out.println("Element1a has email incorrect :" + b1.getText());
		}

		System.out.println("fn_loginEmail");

	}

	public void fn_stepTwoFullName(String locator, String fullname) throws InterruptedException {
		// Full name //
		WebElement stp1 = driver.findElement(By.cssSelector(locator));
		stp1.sendKeys(fullname);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoFullName: Passed");
		// fn_takeScreenshot(driver, OutputSnapshoots + "/fn_stepTwoFullName"+timeStamp+".png");
	}

	public void fn_stepTwoDOB(String locator, String dob) throws InterruptedException {
		WebElement stp2 = driver.findElement(By.cssSelector(locator));
		stp2.sendKeys(dob);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoDOB: Passed");
	}

	public void fn_stepTwoPAN(String locator, String pan) throws InterruptedException {// "input[id='15'][name='15'][column-name='pan_id'][class='form-control
																						// value-field
																						// custom-class-pan']"
		WebElement stp3 = driver.findElement(By.cssSelector(locator));
		stp3.sendKeys(pan);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoPAN: Passed");
	}

	public void fn_stepTwoPassportNumber(String locator, String passport) throws InterruptedException {// "input[id='16'][name='16'][column-name='passport'][class='form-control
																										// value-field
																										// custom-class-alphanum']"
		WebElement stp3 = driver.findElement(By.cssSelector(locator));
		stp3.sendKeys(passport);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoPassportNumber: Passed");
	}

	public void fn_stepTwoAadhaarNumber(String locator, String Aadhaar) throws InterruptedException {// "input[id='17'][name='17'][column-name='aadhaar_id'][class='form-control
		WebElement stp3 = driver.findElement(By.cssSelector(locator));
		stp3.sendKeys(Aadhaar);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoAadhaarNumber: Passed");
	}

	public void fn_stepTwoAddress1(String locator, String address) throws InterruptedException {
		WebElement stp4 = driver.findElement(By.cssSelector(locator));
		stp4.sendKeys(address);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoAddress1: Passed");
	}

	public void fn_stepTwoAddress2(String locator, String address) throws InterruptedException {
		WebElement stp5 = driver.findElement(By.cssSelector(locator));
		stp5.sendKeys(address);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoAddress2: Passed");
	}

	public void fn_stepTwoPincode(String locator, String pincode) throws InterruptedException {
		WebElement stp6 = driver.findElement(By.cssSelector(locator));
		stp6.sendKeys(pincode);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoPincode: Passed");
	}

	public void fn_stepTwoLandline(String locator, String landline) throws InterruptedException {
		WebElement stp6 = driver.findElement(By.cssSelector(locator));
		stp6.sendKeys(landline);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoLandline: Passed");
		}

	public void fn_stepTwoYearOfResidence(String locator, String yor) throws InterruptedException {
		WebElement stp7 = driver.findElement(By.xpath("//*[@id='29']"));
		stp7.sendKeys(yor);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoYearOfResidence: Passed");
		

	}

	public void fn_stepTwoSelectGender(String locator, int gender) throws InterruptedException {
		Select stp8 = new Select(driver.findElement(By.cssSelector(locator)));
		stp8.selectByIndex(gender);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoSelectGender: Passed");
	}

	public void fn_stepTwoSelectMaritalStatus(String locator, int marital) throws InterruptedException {
		Select stp9 = new Select(driver.findElement(By.cssSelector(locator)));
		stp9.selectByIndex(marital);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoSelectMaritalStatusId: Passed");
	}

	public void fn_stepTwoSelectNationality(String locator, int nationality) throws InterruptedException {
		// Nationality -->
		// "input[name='12'][ui-type='dropdown'][column-name='nationality']"
		Select stp10 = new Select(driver.findElement(By.cssSelector(locator)));
		stp10.selectByIndex(nationality);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoSelectNationality: Passed");
	}

	public void fn_stepTwoSelectQualification(String locator, int Qualification) throws InterruptedException {
		Select stp11 = new Select(driver.findElement(By.cssSelector(locator)));
		stp11.selectByIndex(Qualification);
		Thread.sleep(1000);
		System.out.println("Step fn_stepTwoSelectQualification: Passed");
	}

	public void fn_stepTwoSelectCity(String locator, int city) throws InterruptedException {
		Select stp12 = new Select(driver.findElement(By.cssSelector(locator)));
		stp12.selectByIndex(city);
		Thread.sleep(2000);
		System.out.println("Step fn_stepTwoSelectCity: Passed");
		
	}

	public void fn_stepTwoSelectAccomodationType(String locator, int accomodation) throws InterruptedException {
		Select stp13 = new Select(driver.findElement(By.cssSelector(locator)));
		stp13.selectByIndex(accomodation);
		Thread.sleep(2000);
		System.out.println("Step fn_stepTwoSelectAccomodationType: Passed");
		
	}

	public void fn_stepTwoSelectAccomodationYears(String locator, String accomodation) throws InterruptedException {
		WebElement stp14 = driver.findElement(By.cssSelector(locator));
		stp14.sendKeys(accomodation);
		Thread.sleep(2000);
		System.out.println("Step fn_stepTwoSelectAccomodationYears: Passed");
	}

	public void fn_stepTwoPrevious(String locator) {
		// r
		Actions actionProduct = new Actions(driver);//
		WebElement p1 = driver.findElement(
				By.xpath("(//*[@type='button' and @class='btn hex-hor-button btn-previous pull-left'])[2]"));// .isDisplayed();
				if (p1.isDisplayed()) {
			System.out.println("Element1 is Visible");
			actionProduct.click(p1).perform();
		} else {
			System.out.println("Element1 is InVisible");
		}
	}

	public static void waitForNewWindowAndSwitchToIt(WebDriver driver) throws InterruptedException {
		String cHandle = driver.getWindowHandle();
		String newWindowHandle = null;
		Set<String> allWindowHandles = driver.getWindowHandles();

		// Wait for 20 seconds for the new window and throw exception if not
		// found
		for (int i = 0; i < 20; i++) {
			if (allWindowHandles.size() > 1) {
				for (String allHandlers : allWindowHandles) {
					if (!allHandlers.equals(cHandle))
						newWindowHandle = allHandlers;
				}
				driver.switchTo().window(newWindowHandle);
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		if (cHandle == newWindowHandle) {
			throw new RuntimeException("Time out - No window found");
		}
	}

	public void switchtoDefaultFrame() {
		try {
			driver.switchTo().defaultContent();
			System.out.println("Navigated back to webpage from frame");
		} catch (Exception e) {
			System.out.println("unable to navigate back to main webpage from frame" + e.getStackTrace());
		}
	}

	public void switchToFrame(String ParentFrame, String ChildFrame) {
		try {
			driver.switchTo().frame(ParentFrame).switchTo().frame(ChildFrame);
			System.out.println("Navigated to innerframe with id " + ChildFrame + "which is present on frame with id"
					+ ParentFrame);
		} catch (NoSuchFrameException e) {
			System.out
					.println("Unable to locate frame with id " + ParentFrame + " or " + ChildFrame + e.getStackTrace());
		} catch (Exception e) {
			System.out.println("Unable to navigate to innerframe with id " + ChildFrame
					+ "which is present on frame with id" + ParentFrame + e.getStackTrace());
		}
	}

	public void switchToFrame(WebElement frameElement) {
		try {

			if (frameElement.isDisplayed()) {
				driver.switchTo().frame(frameElement);
				System.out.println("Navigated to frame with element " + frameElement);
			} else {
				System.out.println("Unable to navigate to frame with element " + frameElement);
			}
		} catch (NoSuchFrameException e) {
			System.out.println("Unable to locate frame with element " + frameElement + e.getStackTrace());
		} catch (StaleElementReferenceException e) {
			System.out.println(
					"Element with " + frameElement + "is not attached to the page document" + e.getStackTrace());
		} catch (Exception e) {
			System.out.println("Unable to navigate to frame with element " + frameElement + e.getStackTrace());
		}
	}

	public void fn_homeLoginClick(String locator) throws InterruptedException {
		driver.findElement(By.xpath(locator)).click();
		Thread.sleep(1000);
	}

	public void fn_homeLoginEnterEmail(String locator, String email) throws InterruptedException {
		driver.findElement(By.xpath(locator)).sendKeys(email);
		System.out.println("fn_homeLoginEnterEmail");
		Thread.sleep(1000);
	}

	public void fn_homeLoginEnterPassword(String locator, String password) throws InterruptedException {
		driver.findElement(By.xpath(locator)).sendKeys(password);
		System.out.println("fn_homeLoginEnterPassword");
		Thread.sleep(1000);
	}

	public void fn_homeLoginSubmit(String locator) throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(By.cssSelector(locator)).click();
		System.out.println("fn_homeLoginSubmit");
		Thread.sleep(1000);
	}

	public void fn_takeScreenshot(WebDriver driver, String path) {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(path));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("fn_takeScreenshot taken check at: "+path);
	}

	public void fn_loop2() throws InterruptedException {
		String notWorkingUrlTitle = "Under Construction: QAAutomated";
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> linkElements = driver
				.findElements(By.cssSelector("a[href^='/housing-home-loan/'][class='viewFullDetail']"));
		String[] linkTexts = new String[linkElements.size()];
		System.out.println("size" + linkElements);
		int i = 0;
		// extract the link texts of each link element
		for (WebElement elements : linkElements) {

			linkTexts[i] = elements.getText();
			System.out.println("weblink" + linkTexts[i]);
			i++;
		}
		// test each link
		for (String t : linkTexts) {
			System.out.println("link text" + t);

			driver.findElement(By.linkText(t)).click();
			if (driver.getTitle().equals(notWorkingUrlTitle)) {
				System.out.println("\"" + t + "\"" + " is not working.");
			} else {
				System.out.println("\"" + t + "\"" + " is working.");
			}
			driver.navigate().back();
		}
		driver.quit();
	}

	public void fn_loop3() throws InterruptedException {
		String[] links = null;
		int linksCount = 0;
		List<WebElement> linksize = driver.findElements(By.cssSelector("a[class='viewFullDetail']"));
		linksCount = linksize.size();
		System.out.println("Total no of links Available: " + linksCount);
		System.out.println("List of links Available: ");
		// print all the links from webpage
		for (int i = 0; i < linksCount; i++) {
			links[i] = linksize.get(i).getAttribute("href");
			System.out.println("all_links_webpage:" + linksize.get(i).getAttribute("href"));
		}
		// navigate to each Link on the webpage
		for (int i = 0; i < linksCount; i++) {
			System.out.println("URLy" + links[i]);
			driver.navigate().to(links[i]);
			Thread.sleep(5000);
			driver.navigate().back();
		}
	}

	public void fn_forLoopAllProducts() throws InterruptedException {
		String[] linksImgs = null;
		String[] linksViews = null;
		int imgCount = 0;
		int viewCount = 0;
		List<WebElement> linkImg;
		driver.navigate().to("https://beta.rubique.com/credit-cards");
		linkImg = driver.findElements(By.cssSelector(".row.borderBottom.listBankId.col-sm-12.col-xs-6>img"));
		System.out.println("Total no of links Available: " + linkImg.size());

		imgCount = linkImg.size();
		linksImgs = new String[imgCount];
		int i = 0;

		for (WebElement e : linkImg) {
			// linkImg.listIterator().next();
			System.out.println(e.isDisplayed());
			e.click();

			for (int z = 0; z < imgCount; z++) {
				linksImgs[z] = linkImg.get(z).getAttribute("src");
				System.out.println("all_links_webpage:" + linksImgs[z]);
				// driver.navigate().to(linksImgs[z]);
				Thread.sleep(5000);
			}
			// lop2
			List<WebElement> linkView = driver.findElements(By.cssSelector("a[class='viewFullDetail']"));
			System.out.println("Total no of links Available: " + linkView.size());
			viewCount = linkView.size();
			linksViews = new String[viewCount];

			for (int j = 0; j < viewCount; j++) {
				linksViews[j] = linkView.get(j).getAttribute("href");
				System.out.println("all_links_webpage:" + linksViews[j]);
			}
			// navigate to each CC Link on the webpage
			for (int k = 0; k < viewCount; k++) {
				System.out.println("URLy" + linksViews[k]);
				driver.navigate().to(linksViews[k]);
				Thread.sleep(5000);
				driver.navigate().back();
			}
			Thread.sleep(5000);
		}
	}

	public void fn_stepThreeLoanAmountRequired(String locator, String amount) throws InterruptedException {
		Thread.sleep(2000);
		WebElement a1 = driver.findElement(By.cssSelector(locator));
		a1.sendKeys(amount);
		System.out.println("fn_stepThreeLoanAmountRequired");
	}

	public void fn_stepThreeYear(String locator, String year) {
		WebElement a1 = driver.findElement(By.cssSelector(locator));
		a1.sendKeys(year);
		System.out.println("fn_stepThreeYear");
	}

	public void fn_stepThreeMonths(String locator, String months) {
		WebElement a1 = driver.findElement(By.cssSelector(locator));
		a1.sendKeys(months);
		System.out.println("fn_stepThreeMonths");
	}

	public void fn_stepThreeContinue(String locator) throws InterruptedException {
		Thread.sleep(1000);
		// fn_takeScreenshot(driver, OutputSnapshoots + "/fn_stepThreeContinue"+timeStamp+".png");
		driver.findElement(By.cssSelector(locator)).click();
		System.out.println("fn_stepThreeContinue");
		Thread.sleep(1000);
	}

	// Steps 4
	public void fn_stepFourSelectOccupation(String locator, String occupation) throws InterruptedException {

		if (driver.findElement(By.cssSelector(locator)).isDisplayed()) {
			System.out.println("Element found fn_stepFourSelectOccupation");
		}

		Select stp8 = new Select(driver.findElement(By.cssSelector(locator)));
		stp8.selectByValue(occupation);

		Thread.sleep(1000);
		System.out.println("fn_stepFourSelectOccupation");
	}

	public void fn_stepFourSelectOfficeCity(String locator, String Officecity) throws InterruptedException {
		if (driver.findElement(By.cssSelector(locator)).isDisplayed()) {
			System.out.println("Element found fn_stepFourOfficeCity");
		}

		Select stp8 = new Select(driver.findElement(By.cssSelector(locator)));
		stp8.selectByValue(Officecity);

		Thread.sleep(1000);
		System.out.println("fn_stepFourOfficeCity");
	}

	public void fn_stepFourSelectCompanyName(String locator, String company) throws InterruptedException {
		Thread.sleep(5000);
		Actions a = new Actions(driver);
		driver.findElement(By.cssSelector(locator)).sendKeys("TEST AND VERIFICATION SOLUTIONS INDIA PRIVATE LIMITED");
		a.clickAndHold(driver.findElement(By.cssSelector(locator))).perform();

		List<WebElement> c1 = driver.findElements(By.cssSelector(locator));
		System.out.println(c1.get(1));
		// System.out.println(c1.get(0).click(););
		c1.get(0).clear();
		c1.get(0).sendKeys(company);
		Thread.sleep(2000);
		c1.get(0).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		c1.get(0).sendKeys(Keys.ENTER);
		/**
		 * for ( WebElement b : c1) { System.out.println("each elements");
		 * System.out.println(b.getAttribute("id")); //System.out.println(b.);
		 * System.out.println(b.isDisplayed()); b.clear(); Thread.sleep(1000);
		 * b.sendKeys(company); Thread.sleep(3000); b.sendKeys(Keys.ARROW_DOWN);
		 * Thread.sleep(1000); b.sendKeys(Keys.ENTER); //Select m1 = new
		 * Select(b); //m1.selectByVisibleText(
		 * "TEST AND VERIFICATION SOLUTIONS INDIA PRIVATE LIMITED") ; }
		 **/

		System.out.println("fn_stepFourSelectCompanyName");

	}

	public void fn_stepFourGrossMonthlyIncome(String locator, String income) throws InterruptedException {
		WebElement i1 = driver.findElement(By.cssSelector(locator));
		i1.sendKeys(income);
		Thread.sleep(1000);
		System.out.println("fn_stepFourGrossMonthlyIncome");
	}

	public void fn_stepFourSelectSalaryMode(String locator, String mode) throws InterruptedException {
		Select m1 = new Select(driver.findElement(By.cssSelector(locator)));
		m1.selectByValue(mode);
		Thread.sleep(2000);
		System.out.println("fn_stepFourSelectSalaryMode");
	}

	public void fn_stepFourSelectCompanyType(String locator, String type) throws InterruptedException {
		Select t1 = new Select(driver.findElement(By.cssSelector(locator)));
		t1.selectByValue(type);
		// stp12.deselectByValue(13);
		Thread.sleep(2000);
		System.out.println("fn_stepFourSelectCompanyType");
	}

	public void fn_stepFourSelectProfessionType(String locator, String profession) throws InterruptedException {
		Select p1 = new Select(driver.findElement(By.cssSelector(locator)));
		p1.selectByValue(profession);
		// stp12.deselectByValue(13);
		Thread.sleep(2000);
		System.out.println("fn_stepFourSelectProfessionType");
	}

	public void fn_stepFourSelectDesignation(String locator, String designation) throws InterruptedException {
		Select d1 = new Select(driver.findElement(By.cssSelector(locator)));
		d1.selectByValue(designation);
		// stp12.deselectByValue(13);
		Thread.sleep(2000);
		System.out.println("fn_stepFourSelectDesignation");
	}

	public void fn_stepFourNOYCurrentWork(String locator, String currentwork) throws InterruptedException {
		WebElement w1 = driver.findElement(By.cssSelector(locator));
		w1.sendKeys(currentwork);
		Thread.sleep(1000);
		System.out.println("fn_stepFourNOYCurrentWork");
	}

	public void fn_stepFourNOYearWork(String locator, String totalwork) throws InterruptedException {
		WebElement w1 = driver.findElement(By.cssSelector(locator));
		w1.sendKeys(totalwork);
		Thread.sleep(1000);
		System.out.println("fn_stepFourNOYearWork");
	}

	public void fn_stepFourAddress1(String locator, String address) throws InterruptedException {
		WebElement w1 = driver.findElement(By.cssSelector(locator));
		w1.sendKeys(address);
		Thread.sleep(1000);
		System.out.println("fn_stepFourAddress1");
	}

	public void fn_stepFourAddress2(String locator, String address) throws InterruptedException {
		WebElement w1 = driver.findElement(By.cssSelector(locator));
		w1.sendKeys(address);
		Thread.sleep(1000);
		System.out.println("fn_stepFourAddress2");
	}

	public void fn_stepFourLandmark(String locator, String landmark) throws InterruptedException {
		WebElement w1 = driver.findElement(By.cssSelector(locator));
		w1.sendKeys(landmark);
		Thread.sleep(1000);
		System.out.println("fn_stepFourLandmark");
	}

	public void fn_stepFourOfficePincode(String locator, String pincode) throws InterruptedException {
		WebElement w1 = driver.findElement(By.cssSelector(locator));
		w1.sendKeys(pincode);
		Thread.sleep(1000);
		System.out.println("fn_stepFourOfficePincode");
	}

	public void fn_stepFourOfficePhoneNo(String locator, String phonenumber) throws InterruptedException {
		WebElement w1 = driver.findElement(By.cssSelector(locator));
		w1.sendKeys(phonenumber);
		Thread.sleep(1000);
		System.out.println("fn_stepFourOfficePhoneNo");
	}

	public void fn_stepFiveAnyLoanExisting(String locator, String anyloan) throws InterruptedException {
		Select a1 = new Select(driver.findElement(By.cssSelector(locator)));
		a1.selectByValue(anyloan);
		// stp12.deselectByValue(13);
		Thread.sleep(2000);
		System.out.println("fn_stepFiveAnyLoanExisting");
	}

	public void fn_stepFiveExistingLoanEMI(String locator, String loanemi) throws InterruptedException {
		WebElement e1 = driver.findElement(By.cssSelector(locator));
		e1.sendKeys(loanemi);
		// stp12.deselectByValue(13);
		Thread.sleep(2000);
		System.out.println("fn_stepFiveExistingLoanEMI");
	}

	public void fn_stepFiveEMIPaid(String locator, String emi) throws InterruptedException {
		WebElement e1 = driver.findElement(By.cssSelector(locator));
		e1.sendKeys(emi);
		// stp12.deselectByValue(13);
		Thread.sleep(2000);
		System.out.println("fn_stepFiveEMIPaid");
	}

	public void fn_stepFiveExisitingLoanType(String locator, String type) throws InterruptedException {
		Select t1 = new Select(driver.findElement(By.cssSelector(locator)));
		t1.selectByValue(type);
		Thread.sleep(2000);
		System.out.println("fn_stepFiveExisitingLoanType");
	}

	public void fn_stepFiveContinue(String locator) throws InterruptedException {

		Actions actionProduct = new Actions(driver);
		WebElement c1 = null;
		if (driver.findElement(By.xpath(locator)).isDisplayed()) {
			System.out.println("found the emelenet continue");
			driver.findElement(By.xpath(locator)).click();
		}
		// actionProduct.click(c1).perform();
		System.out.println("fn_stepFiveContinue");
	}

	public void fn_stepSixSelectBankName(String locator, String bank) throws InterruptedException {
		Select t1 = new Select(driver.findElement(By.cssSelector(locator)));
		t1.selectByValue(bank);
		Thread.sleep(2000);
		System.out.println("fn_sixStepSelectBankName");
	}

	public void fn_stepSixSelectChequeBounce(String locator, String bounce) throws InterruptedException {
		Select t1 = new Select(driver.findElement(By.cssSelector(locator)));
		t1.selectByValue(bounce);
		Thread.sleep(2000);
		System.out.println("fn_sixStepSelectChequeBounce");
	}

	public void fn_stepSixClickHere(String locator) throws InterruptedException {
		WebElement e1 = driver.findElement(By.cssSelector(locator));
		e1.click();
		Thread.sleep(2000);
		System.out.println("fn_stepSixClickHere");
	}

	public void fn_stepSixOTP(String locator, String otp) throws InterruptedException {
		WebElement e1 = driver.findElement(By.cssSelector(locator));
		e1.sendKeys(otp);
		Thread.sleep(2000);
		System.out.println("fn_stepSixOTP");
	}

	public void fn_stepSixSubmit(String locator) throws InterruptedException

	{
		Actions actionProduct = new Actions(driver);
		WebElement c1 = null;
		if (driver.findElement(By.cssSelector(locator)).isDisplayed()) {
			System.out.println("found the emelenet continue");
			c1 = driver.findElement(By.cssSelector(locator));
		}
		actionProduct.click(c1).perform();
		System.out.println("fn_stepSixSubmit");
	}

	public Connection fn_databaseJdbcConnection(String url, String dbName, String driver, String userName,
			String password)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Class.forName(driver).newInstance();
		Connection connection1 = DriverManager.getConnection(url + dbName, userName, password);
		return connection1;
	}

	public String fn_getOtpFromLogin(Connection conection, String query) throws SQLException {
		String otp = null;
		Statement statement = conection.createStatement();
		ResultSet rs = statement.executeQuery(query);
		while (rs.next()) {

			otp = rs.getString("otp");
			System.out.println(otp);
		}
		return otp;
	}

	public void fn_stepTwoContinue(String locator) throws InterruptedException

	{
		Actions actionProduct = new Actions(driver);
		WebElement c1 = null;
		if (driver.findElement(By.xpath(locator)).isDisplayed()) {
			System.out.println("found the emelenet continue");
			c1 = driver.findElement(By.xpath(locator));
		}
		actionProduct.click(c1).perform();
		System.out.println("Step 14");
	}

	public void fn_Insurance(String locator, String name, String mobile, String email, String Insurancetype)
			throws InterruptedException {
		mobile = mobile.replaceAll("^\"|\"$", "");
		email = email.replaceAll("^\"|\"$", "");
		name = name.replaceAll("^\"|\"$", "");
		Insurancetype = Insurancetype.replaceAll("^\"|\"$", "");

		// mobile = mobile.replaceAll("^\"|\"$", "");

		System.out.println("name/;->" + locator);
		System.out.println("mobile/;->" + mobile);
		System.out.println("email/;->" + email);
		System.out.println("Insurancetype/;->" + Insurancetype);
		if (driver.findElement(By.xpath(locator)).isDisplayed()) {
			System.out.println("found it on home page");
			driver.findElement(By.xpath(locator)).click();
		} else {
			System.out.println("not found");
		}
		/**
		 * String name="NAme"; String mobile ="9865327845" ; String
		 * email="teu@123.com"; String Insurancetype= "General Insurance";
		 **/
		driver.navigate().to("https://beta.rubique.com/insurance");
		;

		WebElement c1, c2, c3, c4;
		if (driver.findElement(By.cssSelector("input[column-name='name'][id='name'][placeholder='Full Name']"))
				.isDisplayed()) {
			System.out.println("fname");
			c1 = driver.findElement(By.cssSelector("input[column-name='name'][id='name'][placeholder='Full Name']"));
			// fn_highLightElement(driver, c1);
			c1.sendKeys(name);
		}

		if (driver
				.findElement(By.cssSelector(
						"input[column-name='phone'][id='mobile'][title='The correct format is valid 10 digit mobile number']"))
				.isDisplayed()) {
			System.out.println("mobile");
			c2 = driver.findElement(By.cssSelector(
					"input[column-name='phone'][id='mobile'][title='The correct format is valid 10 digit mobile number']"));
			// fn_highLightElement(driver, c2);
			c2.sendKeys(mobile);
		}
		if (driver.findElement(By.cssSelector("input[placeholder='Email'][column-name='email'][id='email']"))
				.isDisplayed()) {
			System.out.println("email");
			c3 = driver.findElement(By.cssSelector("input[placeholder='Email'][column-name='email'][id='email']"));
			// fn_highLightElement(driver, c3);
			c3.sendKeys(email);
		}
		if (driver
				.findElement(
						By.cssSelector("button[type='submit'][id='insurance-apply-button'][class='hex-hor-button']"))
				.isDisplayed()) {
			c4 = driver.findElement(
					By.cssSelector("button[type='submit'][id='insurance-apply-button'][class='hex-hor-button']"));
			// fn_highLightElement(driver, c4);
			c4.click();
		}
		if (driver.findElement(By.cssSelector("select[column-name='prod'][id='product_type_sought']")).isDisplayed()) {

			Select t1 = new Select(
					driver.findElement(By.cssSelector("select[column-name='prod'][id='product_type_sought']")));
			// fn_highLightElement(driver, t1);
			t1.selectByValue(Insurancetype);
			Thread.sleep(2000);
		}

		if (driver
				.findElement(
						By.cssSelector("button[type='submit'][id='insurance-apply-button'][class='hex-hor-button']"))
				.isDisplayed()) {
			c4 = driver.findElement(
					By.cssSelector("button[type='submit'][id='insurance-apply-button'][class='hex-hor-button']"));
			// fn_highLightElement(driver, c4);
			c4.click();
		}

		// fn_takeScreenshot(driver, OutputSnapshoots + "/Insurance1"+timeStamp+".png");
	}

	public void fn_insuranceName(String locator, String name) {
		locator = locator.replaceAll("^\"|\"$", "");
		name = name.replaceAll("^\"|\"$", "");

		System.out.println(name);
		System.out.println(locator);
		WebElement c1;
		if (driver.findElement(By.cssSelector(locator)).isDisplayed()) {
			System.out.println("fname");
			c1 = driver.findElement(By.cssSelector(locator));
			// fn_highLightElement(driver, c1);
			c1.sendKeys(name);
		} else {
			System.out.println("not found name feild");

		}
		System.out.println("fn_insuranceName");
	}

	public void fn_insuranceMiddleIcon(String locator) {
		WebElement a;
		a = driver.findElement(By.cssSelector(locator));
		a.click();

	}

	public void fn_insuranceMobile(String locator, String mobile) {
		locator = locator.replaceAll("^\"|\"$", "");
		mobile = mobile.replaceAll("^\"|\"$", "");

		WebElement c1;
		if (driver.findElement(By.cssSelector(locator)).isDisplayed()) {
			System.out.println("mobile");
			c1 = driver.findElement(By.cssSelector(locator));
			// fn_highLightElement(driver, c1);
			c1.sendKeys(mobile);
		} else {
			System.out.println("not found name feild");

		}
		System.out.println("fn_insuranceMobile");
	}

	public void fn_insuranceEmail(String locator, String email) {
		locator = locator.replaceAll("^\"|\"$", "");
		email = email.replaceAll("^\"|\"$", "");
		WebElement c1;
		if (driver.findElement(By.cssSelector(locator)).isDisplayed()) {
			System.out.println("email");
			c1 = driver.findElement(By.cssSelector(locator));
			// fn_highLightElement(driver, c1);
			c1.sendKeys(email);
		} else {
			System.out.println("not found name feild");

		}

		System.out.println("fn_insuranceEmail");
	}

	public void fn_insuranceSelectInsuranceType(String locator, String Insurancetype) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		Insurancetype = Insurancetype.replaceAll("^\"|\"$", "");

		if (driver.findElement(By.cssSelector(locator)).isDisplayed()) {

			Select t1 = new Select(driver.findElement(By.cssSelector(locator)));
			// fn_highLightElement(driver, t1);
			t1.selectByValue(Insurancetype);
			Thread.sleep(2000);
		}
		System.out.println("fn_insuranceSelectInsuranceType");
	}

	public void fn_insuranceSubmit(String locator) {
		locator = locator.replaceAll("^\"|\"$", "");

		WebElement c4;
		if (driver.findElement(By.cssSelector(locator)).isDisplayed()) {
			c4 = driver.findElement(By.cssSelector(locator));
			// fn_highLightElement(driver, c4);
			c4.click();
		}
		// fn_takeScreenshot(driver, screenSnapsPath );
		System.out.println("fn_insuranceSubmit");
	}

	public void fn_insuranceFooterLinkClick(String locator) {
		WebElement c4;
		if (driver.findElement(By.xpath(locator)).isDisplayed()) {
			System.out.println("footer found");
			c4 = driver.findElement(By.xpath(locator));
			// fn_highLightElement(driver, c4);
			c4.click();
		}
		// fn_takeScreenshot(driver, screenSnapsPath);
		System.out.println("fn_insuranceFooterLinkClick");
	}
	
	public void fn_closeBrowser()
	{
		System.out.println("closing browser fn_closeBrowser");
		driver.close();
	}
}