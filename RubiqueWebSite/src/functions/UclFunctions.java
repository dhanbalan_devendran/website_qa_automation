

package functions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import drivers.UclDrivers;

public class UclFunctions extends UclDrivers {
	
	public WebDriver fn_setUpChrome() {
		workingDir = cromeDrivePath;
		System.out.println(workingDir);
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		System.out.println("fn_setUpChrome");
		return driver;
	    }

	public void fn_openUrl(String URL) {
		driver.get(URL);
		System.out.println("fn_openUrl");
	}
	public void fn_homeLoginClick(String locator) throws InterruptedException 
	//click on homepage login
	{
		Thread.sleep(1000);
		WebElement homeLoginClick = driver.findElement(By.xpath(locator));
		homeLoginClick.click(); 
		System.out.println("home click");		
	}	     
public void fn_emailId(String locator,String email) throws InterruptedException {
	//filling email/username 
	Thread.sleep(1000);

		WebElement emailid = driver.findElement(By.xpath(locator));
		emailid.sendKeys(email);
		System.out.println("Pass email");		
	}
public void fn_password(String locator,String pss) throws InterruptedException {//filling pwd
	Thread.sleep(1000);

	WebElement passwrd = driver.findElement(By.xpath(locator));
	passwrd.sendKeys(pss);
	System.out.println("Pass pwd");	
}
public void fn_login(String locator) throws InterruptedException{
	Thread.sleep(1000);

	WebElement passwrd = driver.findElement(By.xpath(locator));
	passwrd.click();
	System.out.println("login click");
}
public void fn_Product(String locator) throws InterruptedException{
	Thread.sleep(1000);

	WebElement passwrd = driver.findElement(By.linkText(locator));
	passwrd.click();
	System.out.println("Product clicked");
	}
public void fn_ConsumerLoan(String carloan) throws InterruptedException{
	Thread.sleep(1000);

	WebElement subMenu = driver.findElement(By.linkText(carloan));
	Actions act =new Actions(driver);
    act.moveToElement(subMenu);
    act.moveToElement(subMenu).build().perform();
	System.out.println("move to ConsumerLoan");
	}
	
public void fn_HeaderUsedCarLoan(String locator) throws InterruptedException{
	Thread.sleep(1000);

	WebElement passwrd = driver.findElement(By.linkText(locator));
	passwrd.click();
	System.out.println("click on product");
}
public void fn_HeaderUsedCarLoanClick(String locator) throws InterruptedException{
	Thread.sleep(1000);

	WebElement passwrd = driver.findElement(By.cssSelector(locator));
	passwrd.click();
	System.out.println("CapitalFirst clicked");
}
	

//---------------------------------------------------------------------------------------------------------


public void fn_clickEligibility(String locator) throws InterruptedException {
	WebElement passwrd = driver.findElement(By.cssSelector(locator));
	passwrd.click();
	System.out.println("click on Eligibility");
}
public void fn_selectSecondStagemobile(String locator,String mobile){
	WebDriverWait wait = new WebDriverWait(driver,30);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));  
	driver.findElement(By.cssSelector(locator)); 
	WebElement Tenure = driver.findElement(By.cssSelector(locator));
	Tenure.clear();
	Tenure.sendKeys(mobile);
	System.out.println("Pass mobile");

}
public void fn_selectSecondStageEmailId(String locator,String email){
	
	WebElement emailid = driver.findElement(By.cssSelector(locator));
	emailid.clear();
	emailid.sendKeys(email);
	System.out.println("Pass email");
}

public void fn_selectSecondStageNext(String locator){
	WebElement next = driver.findElement(By.xpath(locator));
	next.click();	
	System.out.println("clicked continue");	
}
//----------------------------------------------------------------------------------------------------


public void fn_completeSecondStepFullName(String locator,String pass) throws InterruptedException{
	Thread.sleep(1000);
	WebElement completeSecondStepFullName=driver.findElement(By.cssSelector(locator));
	completeSecondStepFullName.sendKeys(pass);
	System.out.println("clicked fn_completeSecondStepFullName");	

}
public void fn_stepTwoSelectGenderId(String locator,String gender) throws InterruptedException {
	Select drp0 = new Select(driver.findElement(By.cssSelector(locator)));
	drp0.selectByVisibleText(gender);
	System.out.println("clicked fn_stepTwoSelectGenderId");	

}
 public void fn_stepTwoSelectDOB(String locator,String pss){
	 WebElement stepTwoSelectDOB = driver.findElement(By.cssSelector(locator));
	 stepTwoSelectDOB.sendKeys(pss);	 
		System.out.println("clicked fn_stepTwoSelectDOB");	

 }
 public void fn_stepTwoSelectMarital(String locator,String Marital){
	 Select drp1 = new Select(driver.findElement(By.cssSelector(locator)));
		drp1.selectByVisibleText(Marital);
		System.out.println("clicked fn_stepTwoSelectMarital");	

 }
 public void fn_completeSecondStagePan(String locator,String pann){ 		
		WebElement completeSecondStagePan = driver.findElement(By.cssSelector(locator));
		completeSecondStagePan.sendKeys(pann);
		System.out.println("clicked fn_completeSecondStagePan");	

	} 
 public void fn_stepTwoSelectAddressLine1(String locator,String pss)
 {
	 WebElement stepTwoSelectAddressLine1= driver.findElement(By.cssSelector(locator));
	 stepTwoSelectAddressLine1.sendKeys(pss);
		System.out.println("clicked fn_stepTwoSelectAddressLine1");	

 }
 public void fn_stepTwoSelectAddressLine2(String locator,String pss)
 {
	 WebElement stepTwoSelectAddressLine2= driver.findElement(By.cssSelector(locator));
	 stepTwoSelectAddressLine2.sendKeys(pss);
		System.out.println("clicked fn_stepTwoSelectAddressLine2");	

 }
 public void fn_stepTwoSelectCity(String locator,String Owned){
	 Select drp3 = new Select(driver.findElement(By.cssSelector(locator)));
		drp3.selectByVisibleText(Owned);
		System.out.println("clicked fn_stepTwoSelectCity");	

 }

 public void fn_stepTwoSelectYearsatcurrentresidence(String locator,String ps){
	 WebElement stepTwoSelectPincode = driver.findElement(By.cssSelector(locator));
	 stepTwoSelectPincode.sendKeys(ps);
		System.out.println("clicked fn_stepTwoSelectYearsatcurrentresidence");	

 }
 public void fn_stepTwoSelectTypeofAccommodation(String locator,String Owned){
	 Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Owned);
		System.out.println("clicked fn_stepTwoSelectTypeofAccommodation");	

 }  
 public void fn_stepTwoSelectContinue(String locator){
	 WebElement stepTwoSelectPincode = driver.findElement(By.xpath(locator));
	 stepTwoSelectPincode.click();
		System.out.println("clicked fn_stepTwoSelectContinue");	

 }	
 //---------------------------------------------------------------------------
 public void fn_completeThirdStageLoanAmountRequired (String locator,String ps) throws InterruptedException{
		Thread.sleep(1000);
	 WebElement completeThirdStageLoan_Amount_Required = driver.findElement(By.cssSelector(locator));
	 completeThirdStageLoan_Amount_Required.sendKeys(ps);
		System.out.println("clicked fn_completeThirdStageLoanAmountRequired");	

 }
 public void fn_completeThirdStageTenure(String locator,String ps){
	 WebElement completeThirdStageTenure= driver.findElement(By.xpath(locator));
	 completeThirdStageTenure.sendKeys(ps);
		System.out.println("clicked fn_completeThirdStageTenure");	

 } 
 public void fn_completeThirdStageContinue(String locator){
	 WebElement completeThirdStageContinue= driver.findElement(By.xpath(locator));
	 completeThirdStageContinue.click();
		System.out.println("clicked fn_completeThirdStageContinue");	

 } 
//----------------------------------------------------------------------------------
 
 public void fn_completeFourthStageOccupation(String locator,String Occupation) throws InterruptedException{
		Thread.sleep(1000);
	 Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Occupation);
		System.out.println("clicked fn_completeFourthStageOccupation");	

 }
 public void fn_completeFourthStageCompanyName(String locator,String ps) throws InterruptedException{
		Thread.sleep(1000);
	 WebElement completeFourthStageCompany_Name= driver.findElement(By.xpath(locator));
	 completeFourthStageCompany_Name.sendKeys(ps);
		System.out.println("clicked fn_completeFourthStageCompanyName");	

 }
 public void fn_completeFourthStageGrossMonthlyIncome(String locator,String ps) throws InterruptedException{
		Thread.sleep(1000);

	 WebElement completeFourthStageGross_Monthly_Income= driver.findElement(By.xpath(locator));
	 completeFourthStageGross_Monthly_Income.sendKeys(ps);
		System.out.println("clicked fn_completeFourthStageGrossMonthlyIncome");	

 }
  public void fn_completeFourthStageDoyouhaveincomeproofdocument (String locator,String Owned) throws InterruptedException{
		Thread.sleep(1000);

	 Select drp10 = new Select(driver.findElement(By.xpath(locator)));
		drp10.selectByVisibleText(Owned);
		System.out.println("clicked fn_completeFourthStageDoyouhaveincomeproofdocument");	

 }
 public void fn_completeFourthStageNumberofYearsinCurrentWork(String locator,String work) throws InterruptedException{
		Thread.sleep(1000);

	 WebElement completeFourthStageNumber_of_Years_in_Current_Work= driver.findElement(By.xpath(locator));
	 completeFourthStageNumber_of_Years_in_Current_Work.sendKeys(work);
		System.out.println("clicked fn_completeFourthStageNumberofYearsinCurrentWork");	

 }
 public void fn_completeFourthStageTotalNumberofYearsinWork(String locator,String TotalWork){
	 WebElement completeFourthStageTotal_Number_of_Years_in_Work= driver.findElement(By.xpath(locator));
	 completeFourthStageTotal_Number_of_Years_in_Work.sendKeys(TotalWork);
		System.out.println("clicked fn_completeFourthStageTotalNumberofYearsinWork");	

 }
 public void fn_completeFourthStageContinue(String locator){
	 WebElement completeFourthStageContinue= driver.findElement(By.xpath(locator));
	 completeFourthStageContinue.click();
		System.out.println("clicked fn_completeFourthStageContinue");	

}
 //------------------------------------------------------------------------------------------------
 public void fn_completeFifthStageAnyExistingLoanwithBank(String locator,String No) throws InterruptedException{
		Thread.sleep(1000);

	 Select drp6 = new Select(driver.findElement(By.cssSelector(locator)));
		drp6.selectByVisibleText(No);
		System.out.println("clicked fn_completeFifthStageAnyExistingLoanwithBank");	

 }
 public void fn_completeFifthStageContinue(String locator){
	 WebElement completeFourthStageContinue= driver.findElement(By.xpath(locator));
	 completeFourthStageContinue.click();
		System.out.println("clicked fn_completeFifthStageContinue");	

}

 public void fn_completeSixthStageBankingSince(String locator,String year ) throws InterruptedException{
		Thread.sleep(1000);

	 WebElement completeSixthStagePrimarExistingBankName = driver.findElement(By.xpath(locator));
	 completeSixthStagePrimarExistingBankName.sendKeys(year);
		System.out.println("clicked completeSixthStagePrimarExistingBankName");	

 }
 public void fn_completeSixthStageContinue(String locator){
	 WebElement completeFourthStageContinue= driver.findElement(By.xpath(locator));
	 completeFourthStageContinue.click();
		System.out.println("clicked fn_completeSixthStageContinue");	

}
 public void fn_completeSeventhStageManufacturer (String locator,String pss) throws InterruptedException{
		Thread.sleep(1000);

	 Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(pss);
		System.out.println("clicked fn_completeSeventhStageManufacturer");	

}

public void fn_completeSeventhStageModel(String locator,String pss) throws InterruptedException{
	Thread.sleep(1000);

	 Select drp20 = new Select(driver.findElement(By.cssSelector(locator)));
		drp20.selectByVisibleText(pss);
		System.out.println("clicked fn_completeSeventhStageModel");	

}
public void fn_completeSeventhStageExshowroomPrice(String locator,String pss ){
	 WebElement Ex_showroom_Price  = driver.findElement(By.cssSelector(locator));
	 Ex_showroom_Price .sendKeys(pss);
		System.out.println("clicked fn_completeSeventhStageExshowroomPrice");	

}
public void fn_completeSeventhStageAgeofVehicle (String locator, String pss){
	 WebElement completeFinalStage  = driver.findElement(By.cssSelector(locator));
	 completeFinalStage.sendKeys(pss);
		System.out.println("clicked fn_completeFinalStage");	

}

public void fn_completeFinalStage (String locator) throws InterruptedException{
	Thread.sleep(1000);

	 WebElement completeFinalStage  = driver.findElement(By.cssSelector(locator));
	 completeFinalStage.click();
		System.out.println("clicked fn_completeFinalStage");	

}
//------------------------------------------------------------------
//Footer flow
public void fn_FooterUsedCarLoanClick(String locator) throws InterruptedException{
	Thread.sleep(1000);
	WebElement passwrd = driver.findElement(By.xpath(locator));
	passwrd.click();
	System.out.println("CapitalFirst clicked");
}
public void fn_ProductlistCarLoanClick(String locator) throws InterruptedException{
	Thread.sleep(2000);
	WebElement passwrd = driver.findElement(By.xpath(locator));
	passwrd.click();
	System.out.println("CapitalFirst clicked");

}
}
