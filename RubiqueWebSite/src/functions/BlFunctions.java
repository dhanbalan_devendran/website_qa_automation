package functions;

/** 
 * Author: Trupti Bhosale created on 2017-06-17
 * This file contains all functions for all fields needed for executing Business loan product flow
 * */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.io.FileInputStream;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Hashtable;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class BlFunctions {
	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileIBl1 = workingDir + "/RubiqueWebSite/inputFiles/BLtestdataFooterFlow.xls";
	// public static String inputFileIBl1 = workingDir +
	// "/RubiqueWebSite/inputFiles/BLtestdataHeaderFlow.xls";
	public static String OutputSnapshoots = workingDir + "/outputFiles";
	public static WebDriver driver;
	boolean present;
	String Actualtext, Actualtext1;
	String LaonAmount, years, months, companName, occupation;
	// int Income;
	public String locatorValue;
	String dataValue;
	boolean productFlag = true;
	static String emailid;
	String mobileNumber1;
	public static Workbook wb;
	public static Sheet ws;
	public static Sheet ws1;
	public static Sheet ws2;
	public static Sheet ws3;
	public static FileInputStream fs;
	// public locatorValue;

	public BlFunctions(WebDriver BLDriver) {

		// this.driver=BLDriver;
		BlFunctions.driver = driver;
	}

	public BlFunctions() {

	}

	/*
	 * public void fn_readExcel() throws Exception {
	 * 
	 * fs=new FileInputStream(inputFileIBl1); wb=Workbook.getWorkbook(fs);
	 * 
	 * }
	 */

	public WebDriver fn_setUpChrome() {

		workingDir = cromeDrivePath;
		System.out.println(workingDir);
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);

		// Initialize browser
		driver = new ChromeDriver();

		// wait = new WebDriverWait(driver, 5);
		driver.manage().window().maximize();
		System.out.println("fn_setUpChrome");
		return driver;
	}

	public void fn_openUrl(String URL) {

		fn_setUpChrome();
		driver.get(URL);
		// Maximize browser
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("fn_openUrl");

	}

	public void fn_homeLoginClick(String locator) throws InterruptedException {
		Thread.sleep(2000);
		WebElement homeLoginClick = driver.findElement(By.xpath(locator));
		homeLoginClick.click();
		System.out.println("fn_homeLoginClick");
	}

	public void fn_loginButton(String locator) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		Thread.sleep(5000);
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.click();
		System.out.println("fn_loginButton");
	}

	public void fn_LoginEmail(String locator) throws InterruptedException {
		Thread.sleep(5000);
		String Login = "Praveen.kumar@rubique.com";
		locator = locator.replaceAll("^\"|\"$", "");
		WebElement emailIds = driver.findElement(By.cssSelector(locator));
		emailIds.sendKeys(Login);
		System.out.println("fn_LoginEmail");
	}

	public void fn_Password(String locator, String Pswd) throws InterruptedException {
		Thread.sleep(3000);
		Pswd = Pswd.replaceAll("^\"|\"$", "");
		WebElement Password = driver.findElement(By.cssSelector(locator));
		Password.sendKeys(Pswd);
		System.out.println("fn_Password");
	}

	public void fn_emailId(String locator, String email) throws InterruptedException {
		Thread.sleep(2000);
		String Login = email;
		WebElement emailids = driver.findElement(By.xpath(locator));
		emailids.sendKeys(Login);
		System.out.println("fn_emailId");
	}

	public void fn_password(String locator, String pss) throws InterruptedException {
		WebElement passwrd = driver.findElement(By.xpath(locator));
		passwrd.sendKeys(pss);
		System.out.println("Pass fn_password");
	}

	public void fn_login(String string) throws InterruptedException {
		WebElement login = driver.findElement(By.xpath(string));
		login.click();
		Thread.sleep(1000);
		System.out.println("Logged In");
	}

	public void fn_productLink(String locator) throws InterruptedException {
		// locator=locator.replaceAll("^\"|\"$", "");
		Thread.sleep(5000);
		// WebDriverWait wait = new WebDriverWait(driver, 30);
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.className(locator)));
		if (driver.findElement(By.xpath(locator)).isDisplayed()) {
			System.out.println("footer found");
			driver.findElement(By.xpath(locator)).click();
		}
		// WebElement bl_footerLink= driver.findElement(By.xpath(locator));
		// bl_footerLink.click();
		System.out.println("fn_productLink");
		// Actualtext = driver.findElement(By.xpath(locator)).getText();
		// return Actualtext;
	}

	public void fn_checkEligibility(String locator) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		Thread.sleep(1000);
		WebElement checkEligibilityButton = driver.findElement(By.cssSelector(locator));
		checkEligibilityButton.click();
		System.out.println("fn_checkEligibility");
	}

	public void fn_stepOneMobileNumber(String locator, String mobileNumber) throws InterruptedException {
		Thread.sleep(3000);
		mobileNumber = mobileNumber.replaceAll("^\"|\"$", "");
		WebElement mbNumber = driver.findElement(By.cssSelector(locator));
		mbNumber.sendKeys(mobileNumber);
		System.out.println("fn_stepOneMobileNumber");
	}

	public void fn_stepOneEmail(String locator) throws InterruptedException {

		emailid = randomEmail();
		// emailid ="testuser@gmail.com";
		locator = locator.replaceAll("^\"|\"$", "");
		// emailid=emailid.replaceAll("^\"|\"$", "");

		WebElement emailIds = driver.findElement(By.cssSelector(locator));
		emailIds.clear();
		emailIds.sendKeys(emailid);
		System.out.println("fn_stepOneEmail");
	}

	public static String randomEmail() {
		// return "Random-" + UUID.randomUUID().toString() + "@yopmail.com";
		// return "Random" + Random().toString() + "@yopmail.com";
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(1000);
		return "username" + randomInt + "@gmail.com";
	}

	public void fn_stepOneContinueButton(String locator) {
		locator = locator.replaceAll("^\"|\"$", "");

		WebElement submitButton = driver.findElement(By.xpath(locator));
		submitButton.click();
		System.out.println("fn_stepOneContinueButton");
	}

	public void fn_stepTwoFullName(String locator, String fullname) throws InterruptedException {

		// locator=locator.replaceAll("^\"|\"$", "");
		fullname = fullname.replaceAll("^\"|\"$", "");
		Thread.sleep(1000);
		WebElement fullName = driver.findElement(By.cssSelector(locator));
		fullName.sendKeys(fullname);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoFullName");
	}

	public void fn_stepTwoDOB(String locator, String dob) throws InterruptedException {

		locator = locator.replaceAll("^\"|\"$", "");
		dob = dob.replaceAll("^\"|\"$", "");
		WebElement DOB = driver.findElement(By.cssSelector(locator));
		DOB.sendKeys(dob);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoDOB");
	}

	public void fn_stepTwoPAN(String locator, String pan) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		pan = pan.replaceAll("^\"|\"$", "");
		WebElement PANindia = driver.findElement(By.cssSelector(locator));
		PANindia.sendKeys(pan);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoPAN");
	}

	public void fn_stepTwoAddress1(String locator, String address) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		address = address.replaceAll("^\"|\"$", "");

		WebElement Address1 = driver.findElement(By.cssSelector(locator));
		Address1.sendKeys(address);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoAddress");
	}

	public void fn_stepTwoPermanentAddress(String locator, String address) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		address = address.replaceAll("^\"|\"$", "");
		WebElement Address1 = driver.findElement(By.xpath(locator));
		Address1.clear();
		Address1.sendKeys(address);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoAddress");
	}

	public void fn_stepTwoPincode(String locator, String pincode) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		pincode = pincode.replaceAll("^\"|\"$", "");
		WebElement Pincode = driver.findElement(By.cssSelector(locator));
		Pincode.sendKeys(pincode);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoPincode");
	}

	public void fn_stepTwoYearOfResidence(String locator, String yor) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		yor = yor.replaceAll("^\"|\"$", "");
		WebElement yearOfResidence = driver.findElement(By.cssSelector(locator));
		yearOfResidence.sendKeys(yor);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoYearOfResidence");
	}

	public void fn_stepTwoSelectGenderId(String locator, String gender) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		gender = gender.replaceAll("^\"|\"$", "");
		Select Gender = new Select(driver.findElement(By.cssSelector(locator)));
		Gender.selectByVisibleText(gender);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoSelectGenderId");
	}

	public void fn_stepTwoSelectMaritalStatusId(String locator, String marital) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		marital = marital.replaceAll("^\"|\"$", "");
		Select MartialStatus = new Select(driver.findElement(By.cssSelector(locator)));
		MartialStatus.selectByVisibleText(marital);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoSelectMaritalStatusId");
	}

	public void fn_stepTwoSelectNationalityId(String locator, String nationality) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		nationality = nationality.replaceAll("^\"|\"$", "");
		Select Nationality = new Select(driver.findElement(By.cssSelector(locator)));
		Nationality.selectByVisibleText(nationality);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoSelectNationalityId");
	}

	public void fn_stepTwoSelectQualificationId(String locator, String Qualification) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		Qualification = Qualification.replaceAll("^\"|\"$", "");
		Select QualificationId = new Select(driver.findElement(By.cssSelector(locator)));
		QualificationId.selectByVisibleText(Qualification);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoSelectQualificationId");
	}

	public void fn_stepTwoSelectCityId(String locator, String city) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		city = city.replaceAll("^\"|\"$", "");
		Select City = new Select(driver.findElement(By.cssSelector(locator)));
		City.selectByVisibleText(city);
		Thread.sleep(2000);
		System.out.println("fn_stepTwoSelectCityId");
	}

	public void fn_stepTwoSelectAccomodationTypeId(String locator, String accomodation) throws InterruptedException {

		locator = locator.replaceAll("^\"|\"$", "");
		accomodation = accomodation.replaceAll("^\"|\"$", "");
		Select stp13 = new Select(driver.findElement(By.cssSelector(locator)));
		stp13.selectByVisibleText(accomodation);
		Thread.sleep(2000);
		System.out.println("fn_stepTwoSelectAccomodationTypeId");
	}

	public void fn_stepTwoSelectAccomodationYearsId(String locator, String accomodation) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		accomodation = accomodation.replaceAll("^\"|\"$", "");
		WebElement Accomodation = driver.findElement(By.xpath(locator));
		Accomodation.sendKeys(accomodation);
		Thread.sleep(2000);
		System.out.println("fn_stepTwoSelectAccomodationYearsId");
	}

	public void fn_stepTwoLandmark(String locator, String Landmark) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		Landmark = Landmark.replaceAll("^\"|\"$", "");
		WebElement Landmarks = driver.findElement(By.cssSelector(locator));
		Landmarks.sendKeys(Landmark);
		Thread.sleep(2000);
		System.out.println("fn_stepTwoLandmark");
	}

	public void fn_stepThreeloanBasis(String locator, String LoanBas) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		LoanBas = LoanBas.replaceAll("^\"|\"$", "");
		Select LoanBasis = new Select(driver.findElement(By.cssSelector(locator)));
		LoanBasis.selectByVisibleText(LoanBas);
		System.out.println("fn_stepThreeloanBasis");
	}

	public void fn_stepThreeswipeMachinesince(String locator, String Noyears) {
		locator = locator.replaceAll("^\"|\"$", "");
		Noyears = Noyears.replaceAll("^\"|\"$", "");
		Select SwipeMachine = new Select(driver.findElement(By.cssSelector(locator)));
		SwipeMachine.selectByVisibleText(Noyears);
		System.out.println("fn_stepThreeswipeMachinesince");
	}

	public void fn_stepThreeSwipeMonth(String locator, String Months) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		Months = Months.replaceAll("^\"|\"$", "");
		WebElement swipeMonth = driver.findElement(By.cssSelector(locator));
		swipeMonth.sendKeys(Months);
		Thread.sleep(1000);
		System.out.println("fn_stepThreeSwipeMonth");
	}

	public void fn_stepThreeLoanAmount(String locator, String loanAmounts) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		loanAmounts = loanAmounts.replaceAll("^\"|\"$", "");

		// WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement loanAmount = driver.findElement(By.cssSelector(locator));
		// loanAmount =
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		// loanAmount.clear();
		loanAmount.sendKeys(loanAmounts);

		// LaonAmount=
		// driver.findElement(By.cssSelector(locator)).getAttribute(loanAmounts);
		Thread.sleep(1000);
		System.out.println("fn_stepThreeLoanAmount");
	}

	public void fn_stepThreeTenureYears(String locator, String yearsTenure) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		yearsTenure = yearsTenure.replaceAll("^\"|\"$", "");
		WebElement tenureYears = driver.findElement(By.cssSelector(locator));
		fn_loaderwait(locator);

		tenureYears.clear();
		tenureYears.sendKeys(yearsTenure);
		years = driver.findElement(By.cssSelector(locator)).getAttribute(yearsTenure);
		Thread.sleep(1000);
		System.out.println("fn_stepThreeTenureYears");
	}

	public void fn_stepThreeTenureMonths(String locator, String yearsMonth) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		yearsMonth = yearsMonth.replaceAll("^\"|\"$", "");
		WebElement tenureMonths = driver.findElement(By.cssSelector(locator));
		fn_loaderwait(locator);
		tenureMonths.clear();
		tenureMonths.sendKeys(yearsMonth);
		months = driver.findElement(By.cssSelector(locator)).getAttribute(yearsMonth);
		Thread.sleep(1000);
		System.out.println("fn_stepThreeTenureMonths");
	}

	public void fn_stepFourOccupation(String locator, String Occupations) throws InterruptedException {

		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		Occupations = Occupations.replaceAll("^\"|\"$", "");
		Select Occupation = new Select(driver.findElement(By.cssSelector(locator)));

		Occupation.selectByVisibleText(Occupations);
		System.out.println("fn_stepFourOccupation");
	}

	public void fn_stepFourApplicantType(String locator, String Type) throws InterruptedException {

		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		Type = Type.replaceAll("^\"|\"$", "");
		Select Type1 = new Select(driver.findElement(By.cssSelector(locator)));
		Type1.selectByVisibleText(Type);
		System.out.println("fn_stepFourApplicantType");
	}

	public void fn_stepFourConstitution(String locator, String Constitution) throws InterruptedException {

		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		Constitution = Constitution.replaceAll("^\"|\"$", "");
		Select ConstitutionType = new Select(driver.findElement(By.cssSelector(locator)));
		ConstitutionType.selectByVisibleText(Constitution);
		System.out.println("fn_stepFourConstitution");
	}

	public void fn_stepFourBuisnessYears(String locator, String Years) throws InterruptedException {

		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		Years = Years.replaceAll("^\"|\"$", "");
		WebElement BuisnessYears = driver.findElement(By.cssSelector(locator));
		BuisnessYears.sendKeys(Years);
		System.out.println("fn_stepFourBuisnessYears");
	}

	public void fn_stepFourOfficeOwnership(String locator, String ownership) throws InterruptedException {

		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		ownership = ownership.replaceAll("^\"|\"$", "");
		Select OfficeOwnership = new Select(driver.findElement(By.cssSelector(locator)));
		OfficeOwnership.selectByVisibleText(ownership);
		System.out.println("fn_stepFourOfficeOwnership");
	}

	public void fn_stepFourCompanyName(String locator, String companyName) throws InterruptedException {
		// Thread.sleep(50000);
		locator = locator.replaceAll("^\"|\"$", "");
		companyName = companyName.replaceAll("^\"|\"$", "");

		// WebDriverWait wait = new WebDriverWait(driver,30);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));
		// System.out.println("Element visible");
		// WebElement companyNames= driver.findElement(By.cssSelector(locator));
		WebElement companyNames = driver.findElement(By.xpath(locator));
		companyNames.sendKeys(companyName);
		System.out.println("fn_stepFourCompanyName");
	}

	public void fn_stepFourgrossMonthlyIncome(String locator, String Income) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		Income = Income.replaceAll("^\"|\"$", "");
		WebElement grossMonthlyIncome = driver.findElement(By.cssSelector(locator));
		grossMonthlyIncome.sendKeys(Income);
		System.out.println("fn_stepFourgrossMonthlyIncome");
	}

	public void fn_stepFourofficePhone(String locator, String officePhone) throws InterruptedException {

		locator = locator.replaceAll("^\"|\"$", "");
		officePhone = officePhone.replaceAll("^\"|\"$", "");
		WebElement officePhoneNo = driver.findElement(By.cssSelector(locator));
		officePhoneNo.sendKeys(officePhone);
		System.out.println("fn_stepFourofficePhone");
	}

	public void fn_stepFourIndustryType(String locator, String IndustryTypes) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		IndustryTypes = IndustryTypes.replaceAll("^\"|\"$", "");
		// Select IndustryType =new
		// Select(driver.findElement(By.cssSelector(locator)));
		Select IndustryType = new Select(driver.findElement(By.xpath(locator)));
		IndustryType.selectByVisibleText(IndustryTypes);
		System.out.println("fn_stepFourIndustryType");
	}

	public void fn_stepFourprofessionType(String locator, String professionType) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		professionType = professionType.replaceAll("^\"|\"$", "");
		// Select professionTypes =new
		// Select(driver.findElement(By.cssSelector(locator)));
		Select professionTypes = new Select(driver.findElement(By.xpath(locator)));
		professionTypes.selectByVisibleText(professionType);
		System.out.println("fn_stepFourprofessionType");
	}

	public void fn_stepFifthExistingEMI(String locator, String EMI) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		EMI = EMI.replaceAll("^\"|\"$", "");
		WebElement ExistingEMI = driver.findElement(By.cssSelector(locator));
		ExistingEMI.sendKeys(EMI);
		System.out.println("fn_stepFifthExistingEMI");
	}

	public void fn_stepFifthExistingLoanTenure(String locator, String tenure) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		tenure = tenure.replaceAll("^\"|\"$", "");
		WebElement LoanTenure = driver.findElement(By.cssSelector(locator));
		LoanTenure.sendKeys(tenure);
		System.out.println("fn_stepFifthExistingLoanTenure");
	}

	public void fn_stepFifthNoOfEMIPaid(String locator, String NoEMI) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		NoEMI = NoEMI.replaceAll("^\"|\"$", "");
		WebElement NoOfEMI = driver.findElement(By.cssSelector(locator));
		NoOfEMI.sendKeys(NoEMI);
		System.out.println("fn_stepFifthNoOfEMIPaid");
	}

	public void fn_stepFifthLoanType(String locator, String LoanType) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		LoanType = LoanType.replaceAll("^\"|\"$", "");
		Select Type = new Select(driver.findElement(By.cssSelector(locator)));
		Type.selectByVisibleText(LoanType);
		System.out.println("fn_stepFifthLoanType");
	}

	public void fn_stepFifthexistingLoan(String locator, String ExistingLoan) {
		locator = locator.replaceAll("^\"|\"$", "");
		ExistingLoan = ExistingLoan.replaceAll("^\"|\"$", "");
		Select ExistingLoans = new Select(driver.findElement(By.cssSelector(locator))); // ExistingLoan
		ExistingLoans.selectByVisibleText(ExistingLoan);
		System.out.println("fn_stepFifthexistingLoan");
	}

	public void fn_stepFifthchequeBounce(String locator, String ChequeBounce) {
		locator = locator.replaceAll("^\"|\"$", "");
		ChequeBounce = ChequeBounce.replaceAll("^\"|\"$", "");
		Select ChequeBounces = new Select(driver.findElement(By.cssSelector(locator))); // ChequeBounce
		ChequeBounces.selectByVisibleText(ChequeBounce);
		System.out.println("fn_stepFifthchequeBounce");
	}

	public void fn_stepSixcoApplicant(String locator, String CoApplicant) {
		locator = locator.replaceAll("^\"|\"$", "");
		CoApplicant = CoApplicant.replaceAll("^\"|\"$", "");
		Select CoApplicants = new Select(driver.findElement(By.cssSelector(locator))); // CoApplicant
		CoApplicants.selectByVisibleText(CoApplicant);
		System.out.println("fn_stepSixcoApplicant");
	}

	public void fn_stepSixcoApplicantRelationship(String locator, String Relation) {
		locator = locator.replaceAll("^\"|\"$", "");
		Relation = Relation.replaceAll("^\"|\"$", "");
		Select Relationship = new Select(driver.findElement(By.cssSelector(locator))); // CoApplicant
		Relationship.selectByVisibleText(Relation);
		System.out.println("fn_stepSixcoApplicantRelationship");
	}

	public void fn_stepSixPrimaryApplicant(String locator, String Applicant) {
		locator = locator.replaceAll("^\"|\"$", "");
		Applicant = Applicant.replaceAll("^\"|\"$", "");
		Select PrimaryApplicants = new Select(driver.findElement(By.cssSelector(locator))); // CoApplicant
		PrimaryApplicants.selectByVisibleText(Applicant);
		System.out.println("fn_stepSixpPrimaryApplicant");
	}

	public void fn_stepFifthShareholding(String locator, String share) throws InterruptedException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		share = share.replaceAll("^\"|\"$", "");
		WebElement shares = driver.findElement(By.cssSelector(locator));
		shares.sendKeys(share);
		System.out.println("fn_stepFifthShareholding");
	}

	public void fn_stepSeventhprimaryExistingBank(String locator, String PExistingBank)
			throws InterruptedException, ClassNotFoundException, SQLException {
		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		// PExistingBank=PExistingBank.replaceAll("^\"|\"$", "");
		Select PrimaryExistingBank = new Select(driver.findElement(By.cssSelector(locator))); // PrimaryExistingBank
		PrimaryExistingBank.selectByVisibleText(PExistingBank);
		System.out.println("fn_stepSeventhprimaryExistingBank");
		// fn_completeSeventhStage();
	}

	public void fn_YesNo(String locator, String YesNo) throws InterruptedException {

		Thread.sleep(1000);
		locator = locator.replaceAll("^\"|\"$", "");
		YesNo = YesNo.replaceAll("^\"|\"$", "");
		Select YesNoans = new Select(driver.findElement(By.cssSelector(locator))); // ChequeBounce
		YesNoans.selectByVisibleText(YesNo);
		System.out.println("fn_YesNo");
	}

	public void fn_YesNoLap(String locator, String YesNo) {
		locator = locator.replaceAll("^\"|\"$", "");
		// YesNo=YesNo.replaceAll("^\"|\"$", "");
		Select YesNoans = new Select(driver.findElement(By.cssSelector(locator))); // ChequeBounce
		YesNoans.selectByVisibleText(YesNo);
	}

	public void fn_stepTwoState(String locator, String State) {
		locator = locator.replaceAll("^\"|\"$", "");
		State = State.replaceAll("^\"|\"$", "");
		Select States = new Select(driver.findElement(By.cssSelector(locator))); // ChequeBounce
		States.deselectByVisibleText(State);
		System.out.println("fn_stepTwoState");
	}

	public void fn_stepTwoResidentalStatus(String locator, String Residental) {
		locator = locator.replaceAll("^\"|\"$", "");
		Residental = Residental.replaceAll("^\"|\"$", "");
		Select ResidentalStatus = new Select(driver.findElement(By.cssSelector(locator))); //
		ResidentalStatus.selectByVisibleText(Residental);
		System.out.println("fn_stepTwoResidentalStatus");
	}

	public void fn_stepThreeSalaryMode(String locator, String SalaryMode) {

		locator = locator.replaceAll("^\"|\"$", "");
		SalaryMode = SalaryMode.replaceAll("^\"|\"$", "");
		Select SalaryOfMode = new Select(driver.findElement(By.cssSelector(locator)));

		SalaryOfMode.selectByVisibleText(SalaryMode);
		System.out.println("fn_stepThreeSalaryMode");
	}

	public void fn_stepThreeCompanyType(String locator, String CompanyType) {
		locator = locator.replaceAll("^\"|\"$", "");
		CompanyType = CompanyType.replaceAll("^\"|\"$", "");
		Select CompanyOfType = new Select(driver.findElement(By.cssSelector(locator)));
		CompanyOfType.selectByVisibleText(CompanyType);
		System.out.println("fn_stepThreeCompanyType");
	}

	public void fn_stepFourCurrentWorkYears(String locator, String years) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		years = years.replaceAll("^\"|\"$", "");
		WebElement CurrentWorkYears = driver.findElement(By.cssSelector(locator));
		CurrentWorkYears.sendKeys(years);
		Thread.sleep(1000);
		System.out.println("fn_stepFourCurrentWorkYears");
	}

	public void fn_stepFourTotalWorkYears(String locator, String TotalWorkYear) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		TotalWorkYear = TotalWorkYear.replaceAll("^\"|\"$", "");
		WebElement TotalWorkYears = driver.findElement(By.cssSelector(locator));
		TotalWorkYears.sendKeys(TotalWorkYear);
		System.out.println("fn_stepFourTotalWorkYears");
	}

	public void fn_completeFirstStepEmploymentType(String locator) {
		WebElement EmploymentType = driver.findElement(By.cssSelector(locator));
		EmploymentType.click();
		System.out.println("fn_stepEightPropertyArea");
	}

	public void fn_thankYouPage() {

		String expectedUrl = "http://website.rubique.com/business-loan/thank-you";
		driver.get(expectedUrl);
		try {
			Assert.assertEquals(expectedUrl, driver.getCurrentUrl());
			System.out.println("Navigated to correct webpage");
		} catch (Throwable pageNavigationError) {
			System.out.println("Didn't navigate to correct webpage");
		}
	}

	private static int getRndNumber() {
		Random random = new Random();
		int randomNumber = 1;
		boolean loop = true;
		while (loop) {
			randomNumber = random.nextInt();
			if (Integer.toString(randomNumber).length() == 10 && !Integer.toString(randomNumber).startsWith("-")) {
				loop = false;
			}
		}
		return randomNumber;
	}

	public void fn_completeFirstStep() throws InterruptedException {
		Thread.sleep(5000);
		fn_stepOneMobileNumber("input[name='5']", "9878777777");
		Thread.sleep(1000);
		emailid = randomEmail();
		fn_stepOneEmail("input[id='6']");
		Thread.sleep(1000);
		fn_stepTwoSelectCityId("[column-name='current_city_id'][name='25']", "Mumbai");
		Thread.sleep(1000);
		fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[1]");
		System.out.println("fn_completeFirstStep");
	}

	public void fn_completeSecondStage() throws InterruptedException {
		Thread.sleep(1000);
		fn_stepTwoFullName("input[id='2'][name='2']", "BL automation user");
		Thread.sleep(1000);
		fn_stepTwoSelectGenderId("[class='form-control value-field'][column-name='gender_id'][name='7']", "Male");
		Thread.sleep(1000);
		fn_stepTwoDOB("input[id='fieldNumber8'][name='8']", "01-05-1999");
		Thread.sleep(1000);
		fn_stepTwoSelectMaritalStatusId("[class='form-control value-field'][column-name='marital_status_id'][name='9']",
				"Married");
		Thread.sleep(1000);
		fn_stepTwoSelectNationalityId("[class='form-control value-field'][column-name='nationality'][name='12']",
				"INDIAN");
		Thread.sleep(1000);
		fn_stepTwoSelectQualificationId(
				"[class='form-control value-field'][column-name='highest_qualification'][name='10']",
				"Bachelor's Degree");
		Thread.sleep(1000);
		fn_stepTwoPAN("[column-name='pan_id'][name='15']", "AKUPD2578C");
		Thread.sleep(1000);
		fn_stepTwoAddress1("[column-name='current_address.address1'][name='20']", "Sion Mumbai");
		Thread.sleep(1000);
		fn_stepTwoAddress1("[column-name='current_address.address2'][name='21']", "andheri mumbai");
		Thread.sleep(1000);

		fn_stepTwoPincode("[column-name='current_address.pincode'][name='23']", "400001");
		Thread.sleep(1000);
		fn_stepTwoYearOfResidence(
				"[class='form-control value-field custom-class-durationy'][column-name='residence_year'][name='29']",
				"14");
		Thread.sleep(1000);
		fn_stepTwoSelectAccomodationTypeId(
				"[class='form-control value-field'][column-name='accomodation_type'][name='30']", "Owned");
		Thread.sleep(1000);
		fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[2]");
		System.out.println("fn_completeSecondStage");
	}

	public void fn_completeThirdStage() throws InterruptedException {
		fn_stepThreeloanBasis("[column-name='other_details.loan_basis'][name='595']", "On Existing Swipe Machine");
		fn_stepThreeswipeMachinesince("[column-name='other_details.swipe_machine_since'][name='597'] ", "6 Months");
		fn_stepThreeSwipeMonth("[id='596'][class='form-control value-field custom-class-text'][name='596'] ", "6");
		fn_loaderwait("[id='115'][column-name='loan_amount'][name='115']");
		fn_stepThreeLoanAmount("[id='115'][column-name='loan_amount'][name='115']", "200000");
		fn_stepThreeTenureYears("input[name='years']", "3");
		fn_stepThreeTenureMonths("input[name='months']", "0");
		Thread.sleep(1000);
		fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[3]");
		System.out.println("fn_completeThirdStage");
	}

	public void fn_completeFourthStage() throws InterruptedException {

		fn_stepFourOccupation("[column-name='occupation_id'][name='38']", "Salaried");
		fn_stepFourCompanyName("(//*[@id='fieldNumber40'])[1]", "TATA ADVANCED MATERIAL PRIVATE LIMITED");
		fn_stepFourgrossMonthlyIncome("[id='42'][name='42']", "65000");

		fn_stepFourIndustryType("(//*[@name='53'])[1]", "Manufacturing");
		fn_stepFourprofessionType("(//*[@name='54'])[1]", "Others");
		fn_stepTwoPermanentAddress("(//*[@id='70'])[1]", "office address 1");
		fn_stepTwoPermanentAddress("(//*[@id='71'])[1]", "office address 2");

		fn_stepTwoLandmark("input[id='72'][name='72']", "landmark");
		fn_stepTwoSelectCityId("#form_group_9 > div:nth-child(9) > div > select", "Mumbai");
		fn_stepTwoPincode("div.parent-id-38:nth-child(10) > div:nth-child(1) > input:nth-child(2)", "400001");
		fn_stepFourofficePhone("div.parent-id-38:nth-child(11) > div:nth-child(1) > input:nth-child(2)", "022654585");
		fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[4]");
		System.out.println("fn_completeFourthStage");

	}

	public void fn_loaderwait(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
	}

	public void fn_IsProduct() {
		boolean present;
		try {
			driver.findElement(By.id("logoutLink"));
			present = true;
		} catch (NoSuchElementException e) {
			present = false;
		}
	}

	public void fn_completeFifthStage() throws InterruptedException {

		fn_YesNo("[name='86']", "No");
		fn_YesNo("[name='94']", "No");
		Thread.sleep(5000);
		fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[5]");
		System.out.println("fn_completeFifthStage");
	}

	public void fn_completeSixthStage() throws InterruptedException {

		fn_YesNo("[name='96']", "No");
		Thread.sleep(1000);
		fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[6]");

	}

	public void fn_completeSeventhStage(String dbName)
			throws ClassNotFoundException, SQLException, InterruptedException {
		fn_stepSeventhprimaryExistingBank("[name='80']", "ICICI Bank");

		Thread.sleep(30000);
		if (driver.findElement(By.cssSelector("[id='verify-lead']")).isDisplayed()) {
			// String dbUrl =
			// "jdbc:mysql://jun20.cib48esswkls.ap-south-1.rds.amazonaws.com";

			String dbUrl = dbName;

			String username = "root";

			// Database Password
			String password = "Rub!cR00t";

			// Query to Execute
			// String query = "SELECT email_otp FROM rubique_api.login order by
			// id desc limit 1;";
			String query = "SELECT email_otp FROM rubique_api.login  where email = ?;";

			// Load mysql jdbc driver
			Class.forName("com.mysql.jdbc.Driver");

			// Create Connection to DB
			Connection con = DriverManager.getConnection(dbUrl, username, password);

			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, emailid);

			System.out.println("lead email :" + ps);

			ResultSet rs = ps.executeQuery();

			// Create Statement Object
			Statement stmt = con.createStatement();

			// Execute the SQL Query. Store results in ResultSet
			// ResultSet rs= stmt.executeQuery(query);
			while (rs.next()) {
				String VerID = rs.getString("email_otp");

				// String id = rs.getString("id");
				System.out.println("Verification Code is :" + VerID);
				// System.out.println("lead id :" +id);
				// VerifyID.sendKeys(VerID);
				driver.findElement(By.cssSelector("[id='verify-lead']")).sendKeys(VerID);
				System.out.println("Element is Visible");
			}

			Thread.sleep(1000);
			con.close();
			// submit7.click();
			driver.getCurrentUrl();
		}

		fn_stepOneContinueButton("(//*[@type='submit' and @name='submit'])[7]");

	}

	public void fn_dbVerificationcode(String dbName) throws ClassNotFoundException, SQLException, InterruptedException {
		// fn_stepSeventhprimaryExistingBank("[name='80']", "ICICI Bank");

		// driver.findElement(By.cssSelector(locator));
		Thread.sleep(30000);
		if (driver.findElement(By.cssSelector("[id='verify-lead']")).isDisplayed()) {
			// String dbUrl =
			// "jdbc:mysql://jun20.cib48esswkls.ap-south-1.rds.amazonaws.com";
			String dbUrl = dbName;
			// Database Username
			String username = "root";

			// Database Password
			String password = "Rub!cR00t";

			// Query to Execute
			// String query = "SELECT email_otp FROM rubique_api.login order by
			// id desc limit 1;";
			String query = "SELECT email_otp FROM rubique_api.login  where email = ?;";

			// Load mysql jdbc driver
			Class.forName("com.mysql.jdbc.Driver");

			// Create Connection to DB
			Connection con = DriverManager.getConnection(dbUrl, username, password);

			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, emailid);

			System.out.println("lead email :" + ps);

			ResultSet rs = ps.executeQuery();

			// Create Statement Object
			Statement stmt = con.createStatement();

			// Execute the SQL Query. Store results in ResultSet
			// ResultSet rs= stmt.executeQuery(query);
			while (rs.next()) {
				String VerID = rs.getString("email_otp");

				// String id = rs.getString("id");
				System.out.println("Verification Code is :" + VerID);
				// System.out.println("lead id :" +id);
				// VerifyID.sendKeys(VerID);
				driver.findElement(By.cssSelector("[id='verify-lead']")).sendKeys(VerID);
				System.out.println("Element is Visible");
			}

			Thread.sleep(1000);
			con.close();
			// submit7.click();
			driver.getCurrentUrl();
		}
		}

	// function for each product
	public void fn_individualProduct(String locator) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		Thread.sleep(5000);
		Thread.sleep(5000);
		WebElement Product = driver.findElement(By.xpath(locator));
		Product.click();
	}

	public void fn_basicInfo() throws InterruptedException {
		fn_stepThreeLoanAmount("[id='visible-loan-amount'][name='115']", "200000");
		fn_completeFirstStepEmploymentType("label.btn:nth-child(3)");
		fn_stepFourCompanyName("//*[@name='40']", "TATA ADVANCED MATERIAL PRIVATE LIMITED");
		fn_stepFourgrossMonthlyIncome("[id='visible-monthly-income'][name='42']", "65000");
		fn_stepThreeTenureYears("input.fieldNumber:nth-child(1)", "3");
		Thread.sleep(2000);
		fn_stepThreeTenureMonths("div.input-group:nth-child(2) > input:nth-child(2)", "0");
		fn_stepOneContinueButton("//*[@id='search-step-one-submit-button']");

	}

	public void fn_productMatch(String locator) throws InterruptedException {
		fn_loaderwait("tr.rowShadow:nth-child(1) > td:nth-child(5) > div:nth-child(1) > button:nth-child(1)");
		WebElement ProductMatch = driver.findElement(By.cssSelector(locator));
		ProductMatch.click();
	}

	public void fn_completeThirdStage1() throws InterruptedException {

		fn_stepThreeloanBasis("[column-name='other_details.loan_basis'][name='595']", "On Existing Swipe Machine");
		fn_stepThreeswipeMachinesince("[column-name='other_details.swipe_machine_since'][name='597'] ", "6 Months");
		fn_stepThreeSwipeMonth("[id='596'][class='form-control value-field custom-class-text'][name='596'] ", "6");

		Thread.sleep(1000);
		fn_stepOneContinueButton("//*[@id='form_group_2']/div[6]/button[2]/span");
	}

	public void fn_completeFourthStage1() throws InterruptedException {

		fn_stepFourOccupation("[column-name='occupation_id'][name='38']", "Salaried");
		fn_stepFourCompanyName("[id='fieldNumber40'][name='40']", "TATA ADVANCED MATERIAL PRIVATE LIMITED");
		fn_stepFourgrossMonthlyIncome("[id='42'][name='42']", "65000");

		fn_stepFourIndustryType("[name='53']", "Manufacturing");
		fn_stepFourprofessionType("[name='54']", "Others");
		fn_stepTwoAddress1("[id='70']", "office address 1");
		fn_stepTwoAddress1("[id='71']", "office address 2");
		// landmark.sendKeys("landmark");
		fn_stepTwoLandmark("input[id='72'][name='72']", "landmark");
		fn_stepTwoSelectCityId("[name='74']", "Mumbai");
		fn_stepTwoPincode("[id='75']", "400001");
		fn_stepFourofficePhone("[id='78']", "022654585");

		fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[4]");
		System.out.println("fn_completeFourthStage");

	}

	public void fn_stepThreeLAP() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='115']")).clear();
		driver.findElement(By.xpath(".//*[@id='115']")).sendKeys("20000");

		driver.findElement(By.xpath("(//*[@id='unified-inputs']/input[1])[2]")).clear();
		driver.findElement(By.xpath("(//*[@id='unified-inputs']/input[1])[2]")).sendKeys("3");
		Thread.sleep(1000);
		driver.findElement(By.xpath("(//*[@id='unified-inputs']/input[2])[2]")).clear();
		driver.findElement(By.xpath("(//*[@id='unified-inputs']/input[2])[2]")).sendKeys("0");

	}

	public void fn_HeaderFlow(String Productlocator, String LoanLocator, String locator) throws InterruptedException {

		Thread.sleep(5000);

		Actions action = new Actions(driver);

		WebElement Product = driver.findElement(By.cssSelector(Productlocator));
		action.moveToElement(Product).click().build().perform();

		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);

		WebElement SMELoan = driver.findElement(By.cssSelector(LoanLocator));

		WebElement LoanType = driver.findElement(By.cssSelector(locator));
		Thread.sleep(5000);
		
		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);
		action.moveToElement(SMELoan).moveToElement(LoanType).click().build().perform();

	}

}
