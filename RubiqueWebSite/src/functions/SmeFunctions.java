package functions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

/**
 * @author bhupendra.nikam
 *
 */
public class SmeFunctions{
	
	
	 static WebDriver driver;	
	 static String workingDir = System.getProperty("user.dir");
		public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	  //static WebEelement homelogin click;
	  
	  public SmeFunctions(WebDriver driver) 
	  {
		  SmeFunctions.driver = driver;
	  }
	  
	  public SmeFunctions() 
	  {
	  }
	
	 // public static WebDriver driver;

		public WebDriver fn_setUpChrome() {

			workingDir = cromeDrivePath;
			System.out.println(workingDir);
			// Setting the property to enable the chrome driver
			System.setProperty("webdriver.chrome.driver", cromeDrivePath);
			// Initialize browser
			driver = new ChromeDriver();

			// wait = new WebDriverWait(driver, 5);
			driver.manage().window().maximize();
			System.out.println("fn_setUpChrome");
			return driver;
		}

		public void fn_openUrl(String URL) {
			driver.get(URL);
			// Maximize browser
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("fn_openUrl");

		}
		
	  
	  public void fn_clickSME(String locator)
	  {
		  WebElement clickSME = driver.findElement(By.xpath(locator));
		  clickSME.click();
		  System.out.println("fn_clickSME");
	  }
	  
	  public void fn_clickApplyHere(String locator)
	  {
		  WebElement clickApplyHere = driver.findElement(By.xpath(locator));
		  clickApplyHere.click();
		  System.out.println("fn_clickApplyHere");
	  }
	  
	  public void fn_EnterName(String Locator, String name) throws InterruptedException
	  {
		  Thread.sleep(2000);
		  WebElement EnterName = driver.findElement(By.xpath(Locator));
		  EnterName.sendKeys(name);
		  System.out.println("fn_EnterName");
	  }
	  
	  public void fn_EnterMail(String locator, String mail)
	  {
		  WebElement EnterMail = driver.findElement(By.xpath(locator));
		  EnterMail.sendKeys(mail);
		  System.out.println("fn_EnterMail");
	  }
	  
	  public void fn_EnterMobile(String locator, String mobile)
	  {
		  WebElement EnterMail = driver.findElement(By.xpath(locator));
		  EnterMail.sendKeys(mobile);
		  System.out.println("fn_EnterMobile");
	  }
	  
	  public void fn_EnterCity(String locator, String city)
	  {
		  WebElement EnterMail = driver.findElement(By.xpath(locator));
		  EnterMail.sendKeys(city);
		  System.out.println("fn_EnterCity");
	  }
	  
	  public void fn_SelectProduct(String locator, String product)
	  {
		  Select drop0 = new Select(driver.findElement(By.xpath(locator)));
			 drop0.selectByVisibleText(product);
			 System.out.println("fn_SelectProduct");
	  }
	  
	  public void fn_EnterComment(String locator, String Comment)
	  {
		  WebElement EnterComment = driver.findElement(By.xpath(locator));
		  EnterComment.sendKeys(Comment);
		  System.out.println("fn_EnterComment");
	  }
	  
	  public void fn_ClickSubmit(String locator)
	  {
		  WebElement EnterComment = driver.findElement(By.xpath(locator));
		  EnterComment.click();
		  System.out.println("fn_ClickSubmit");
	  }
	  
	  public void fn_ClickClosebutton(String locator) throws InterruptedException
	  {
		  Thread.sleep(2000);
		  WebElement EnterComment = driver.findElement(By.xpath(locator));
		  EnterComment.click();
		  System.out.println("fn_ClickClosebutton");
	  }
	  

	
  
	}

