package functions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CcFunctions {

	static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileCC1 = workingDir + "/RubiqueWebSite/inputFiles/bl_productflow_20170620_tb_01.xls";
	static WebDriver driver;
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	public static String screenSnapsPath = workingDir + "/outputFiles/" + "lap" + timeStamp + ".png";

	public void fn_takeScreenshot(WebDriver driver, String path) {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(path));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("fn_takeScreenshot taken check at: " + path);
	}

	public WebDriver fn_setUpChrome() {

		workingDir = cromeDrivePath;
		System.out.println(workingDir);
		// Setting the property to enable the chrome driver
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		// Initialize browser
		driver = new ChromeDriver();

		// wait = new WebDriverWait(driver, 5);
		driver.manage().window().maximize();
		System.out.println("fn_setUpChrome");
		return driver;
	}

	public void fn_openUrl(String URL) {
		driver.get(URL);
		// Maximize browser
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("fn_openUrl");

	}

	public CcFunctions(WebDriver driver) {
		CcFunctions.driver = driver;
	}

	public CcFunctions() {

	}

	public void fn_homeLoginClick(String locator) // click on homepage login
	{
		WebElement Next = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		Next.click();
	}

	public void fn_emailId(String locator, String email) throws InterruptedException// filling
	{
		Thread.sleep(1000);
		System.out.println("email" + email);
		System.out.println("locator" + locator);
		WebElement emailid = driver.findElement(By.cssSelector(locator));
		emailid.sendKeys(email);
	}

	public void fn_password(String locator, String pass) // filli g password
	{
		System.out.println("pass" + pass);
		System.out.println("locator" + locator);
		WebElement password = driver.findElement(By.cssSelector(locator));
		password.sendKeys(pass);
	}

	public void fn_login(String locator) // click on login button
	{
		System.out.println("locator" + locator);
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.click();
	}

	public void fn_Product(String locator) throws InterruptedException {
		Thread.sleep(2000);
		WebElement selectCreditcard = driver.findElement(By.xpath(locator));
		selectCreditcard.click();
		System.out.println("HeaderDropDownProduct");
	}
	public void fn_mainDropDownProduct(String mainProd, String subProd, String prod) throws InterruptedException {
		mainProd = mainProd.replaceAll("^\"|\"$", "");
		subProd = subProd.replaceAll("^\"|\"$", "");
		prod = prod.replaceAll("^\"|\"$", "");
		System.out.println(mainProd + subProd + prod);
		// Default set the locator
		if (mainProd.equals(null)) {
			System.out.println(" mainProd is null ");
			mainProd = "Products";
		}
		if (subProd.equals(null)) {
			System.out.println(" subProd is null ");
			subProd = "Consumer Loans";
		}
		if (prod.equals(null)) {
			System.out.println(" subProd is null ");
			prod = "HDB Financial Services";
		}
	
		Actions actionProduct = new Actions(driver);
		WebElement c1 = driver.findElement(By.partialLinkText(mainProd));
		if (c1.isDisplayed()) {
			System.out.println("Element1 is Visible");
			actionProduct.clickAndHold(c1).perform();
			Thread.sleep(4000);
		} else {
			System.out.println("Element1 is InVisible");
		}


		WebElement c2 = driver.findElement(By.linkText(subProd));
		if (c2.isDisplayed()) {
			System.out.println("Element2 is Visible");
			actionProduct.clickAndHold(c2).perform();
			Thread.sleep(5000);
		} else {
			System.out.println("Element2 is InVisible");
		}

		WebElement c3 = driver.findElement(By.partialLinkText(prod));
		if (c3.isDisplayed()) {
			System.out.println("Element3 is Visible");
			actionProduct.click(c3).perform();
			Thread.sleep(4000);
		} else {
			System.out.println("Element3 is InVisible");
		}
		System.out.println("fn_mainDropDownProduct");
	}

	public void fn_selectCreditcardHeader(String HDFC_card) throws InterruptedException {
		Thread.sleep(2000);
		WebElement selectCreditcard = driver.findElement(By.xpath(HDFC_card));
		selectCreditcard.click();
	}

	public void fn_middleCC(String locator) throws InterruptedException {
		Thread.sleep(3500);
		WebElement middleCC = driver.findElement(By.cssSelector(locator));
		middleCC.click();
	}

	public void fn_selectCreditcardMiddle(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		driver.findElement(By.cssSelector(locator)).click();
	}

	public void fn_adressProvidedCategory(String locator, String category) {
		Select drop15 = new Select(driver.findElement(By.cssSelector(locator)));
		drop15.selectByVisibleText(category);
	}

	public void fn_clickNext(String locator) {
		WebElement clickNext = driver.findElement(By.cssSelector(locator));
		clickNext.click();
	}

	public void fn_clickProduct(String linkText) throws InterruptedException {
		Thread.sleep(1500);
		WebElement clickProduct = driver.findElement(By.cssSelector(linkText));
		clickProduct.click();
	}

	public void fn_selectCreditcard(String locator) throws InterruptedException // click
	{
		Thread.sleep(3500);
		WebElement selectCreditcard = driver.findElement(By.cssSelector(locator));
		selectCreditcard.click();
	}

	public void fn_checkEligibility(String HDFC_card) throws InterruptedException {
		Thread.sleep(2000);
		WebElement checkEligibility = driver.findElement(By.cssSelector(HDFC_card));
		checkEligibility.click();
	}

	public void fn_completeFirstStepMobile(String locator, String Mobile) throws InterruptedException {
		Thread.sleep(2000);
		WebElement completeFirstStepMobile = driver.findElement(By.cssSelector(locator));
		completeFirstStepMobile.clear();
		completeFirstStepMobile.sendKeys(Mobile);
	}

	public void fn_completeFirstStepEmail(String locator, String Email) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeFirstStepEmail = driver.findElement(By.cssSelector(locator));
		completeFirstStepEmail.clear();
		completeFirstStepEmail.sendKeys(Email);
	}

	/*public void fn_completeFirstStepContinue(String locator) throws InterruptedException {
		fn_takeScreenshot(driver, screenSnapsPath);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
		driver.findElement(By.xpath(locator)).click();

	}*/
	
	public void fn_completeFirstStepContinue(String click) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver,30);
		   wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(click)));  
		   driver.findElement(By.xpath(click)).click();

		}

	/*public void fn_completeFirstStepContinueM(String click) throws InterruptedException {
		// Thread.sleep(600);
		WebElement completeFirstStepContinue = driver.findElement(By.xpath(click));
	}*/

	public void fn_completeSecondStepFullName(String locator, String pass) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeSecondStepFullName = driver.findElement(By.cssSelector(locator));
		completeSecondStepFullName.sendKeys(pass);
	}

	public void fn_stepTwoSelectGenderId(String locator, String i) {
		Select drop0 = new Select(driver.findElement(By.cssSelector(locator)));
		drop0.selectByVisibleText(i);
	}

	public void stepTwoSelectDOB(String locator, String pss) {
		WebElement stepTwoSelectDOB = driver.findElement(By.cssSelector(locator));
		stepTwoSelectDOB.sendKeys(pss);
	}

	public void stepTwoSelectMarital(String locator, String Marital) {
		Select drop1 = new Select(driver.findElement(By.cssSelector(locator)));
		drop1.selectByVisibleText(Marital);
	}

	public void fn_stepTwoSelectNationality(String locator, String Nationality) {
		Select drop2 = new Select(driver.findElement(By.cssSelector(locator)));
		drop2.selectByVisibleText(Nationality);
	}

	public void fn_completeSecondStagePan(String locator, String pann) {
		WebElement completeSecondStagePan = driver.findElement(By.cssSelector(locator));
		completeSecondStagePan.sendKeys(pann);
	}

	public void fn_stepTwoSelectAddress_Line1(String locator, String pss) {
		WebElement stepTwoSelectAddress_Line1 = driver.findElement(By.cssSelector(locator));
		stepTwoSelectAddress_Line1.sendKeys(pss);
	}

	public void fn_StepTwoSelectAddress_Line2(String locator, String pss) {
		WebElement stepTwoSelectAddress_Line2 = driver.findElement(By.cssSelector(locator));
		stepTwoSelectAddress_Line2.sendKeys(pss);
	}
	

	public void stepTwoSelectCity(String locator, String City) throws InterruptedException {
		Thread.sleep(2500);
		Select drop3 = new Select(driver.findElement(By.cssSelector(locator)));
		drop3.selectByVisibleText(City);
	}

	public void stepTwoSelectPincode(String locator, String pss) {
		WebElement stepTwoSelectPincode = driver.findElement(By.cssSelector(locator));
		stepTwoSelectPincode.sendKeys(pss);
	}

	public void stepTwoSelectYears_at_Current_Residence(String locator, String pss) {
		WebElement stepTwoSelectYears_at_Current_Residence = driver.findElement(By.cssSelector(locator));
		stepTwoSelectYears_at_Current_Residence.sendKeys(pss);
	}

	public void stepTwoSelectContinue(String locator) {
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement stepTwoSelectContinue = driver.findElement(By.xpath(locator));
		stepTwoSelectContinue.click();
	}

	public void fn_completeThirdStageOccuoation(String locator, String occupation) throws InterruptedException {
		Thread.sleep(1000);
		Select drop4 = new Select(driver.findElement(By.cssSelector(locator)));
		drop4.selectByVisibleText(occupation);
	}

	public void fn_completeFourthStageOccupation(String locator, String Occupation) throws InterruptedException {

		Thread.sleep(1000);
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Occupation);
	}

	public void fn_completeFourthStageCompany_Name(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeFourthStageCompany_Name = driver.findElement(By.cssSelector(locator));
		completeFourthStageCompany_Name.sendKeys(ps);
	}

	public void fn_completeFourthStageGross_Monthly_Income(String locator, String ps) {
		WebElement completeFourthStageGross_Monthly_Income = driver.findElement(By.cssSelector(locator));
		completeFourthStageGross_Monthly_Income.sendKeys(ps);
	}
	
	public void fn_EnterNetMonthlyIncome(String locator, String i) throws InterruptedException {
		Thread.sleep(2000);
		WebElement EnterNetMonthlyIncome = driver.findElement(By.cssSelector(locator));
		EnterNetMonthlyIncome.sendKeys(i);
	}


	public void fn_completeForthStageModeOF_salary(String locator, String Salary) throws InterruptedException {
		//Thread.sleep(3000);
		Select drop5 = new Select(driver.findElement(By.xpath(locator)));
		drop5.selectByVisibleText(Salary);
	}

	public void fn_forthStageProfessionType(String locator, String Profession) {
		Select drop6 = new Select(driver.findElement(By.xpath(locator)));
		drop6.selectByVisibleText(Profession);
	}

	public void fn_completeFourthStageNumber_of_Years_in_Current_Work(String locator, String pss) {
		WebElement completeFourthStageNumber_of_Years_in_Current_Work = driver.findElement(By.xpath(locator));
		completeFourthStageNumber_of_Years_in_Current_Work.sendKeys(pss);
	}

	public void fn_completeFourthStageTotal_Number_of_Years_in_Work(String locator, String pss) {
		WebElement completeFourthStageTotal_Number_of_Years_in_Work = driver.findElement(By.xpath(locator));
		completeFourthStageTotal_Number_of_Years_in_Work.sendKeys(pss);
	}

	public void fn_completeForthStageOffice_Address1(String locator, String pss) {
		WebElement completeForthStageOffice_Address1 = driver.findElement(By.xpath(locator));
		completeForthStageOffice_Address1.sendKeys(pss);
	}

	public void fn_completeForthStageOffice_Address2(String locator, String pss) {
		WebElement completeForthStageOffice_Address2 = driver.findElement(By.xpath(locator));
		completeForthStageOffice_Address2.sendKeys(pss);
	}

	public void fn_completeForthStageOffice_city(String locator, String city) throws InterruptedException {
		Thread.sleep(1000);
		Select drop7 = new Select(driver.findElement(By.xpath(locator)));
		drop7.selectByVisibleText(city);
	}

	public void fn_completeForthStageOffice_Pincode(String locator, String pss) {
		WebElement completeForthStageOffice_Pincode = driver.findElement(By.xpath(locator));
		completeForthStageOffice_Pincode.sendKeys(pss);
	}

	public void fn_completeForthStageOffice_Phone(String locator, String pss) {
		WebElement completeForthStageOffice_Phone = driver.findElement(By.xpath(locator));
		completeForthStageOffice_Phone.sendKeys(pss);
	}

	public void fn_completeFourthStageContinue(String locator) {
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement completeFourthStageContinue = driver.findElement(By.xpath(locator));
		completeFourthStageContinue.click();
	}

	public void fn_completeFifthStageDoYouHave_Any_Existing_CC(String locator, String CC) throws InterruptedException {
		Thread.sleep(2000);
		Select drop9 = new Select(driver.findElement(By.xpath(locator)));
		drop9.selectByVisibleText(CC);
	}

	public void fn_completeFinalSubmit(String locator) {
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement completeFinalSubmit = driver.findElement(By.cssSelector(locator));
		completeFinalSubmit.click();
	}
}
