package functions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import drivers.TwlDrivers;

public class TwlFunctions extends TwlDrivers {

	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileTWL1 = workingDir + "/RubiqueWebSite/inputFiles/twl_productflow_20170620_pk_01.xls";
	 public static WebDriver driver;
		public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		public static String screenSnapsPath = workingDir +"/outputFiles/"+"twl"+timeStamp+".png";

	public  static  void fn_takeScreenshot(WebDriver driver, String path) {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(path));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("fn_takeScreenshot taken check at: "+path);
	}
	
	
	public WebDriver fn_setUpChrome() {

		workingDir = cromeDrivePath;
		System.out.println(workingDir);
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		System.out.println("fn_setUpChrome");
		return driver;
	    }

	public void fn_openUrl(String URL) {
		driver.get(URL);
		System.out.println("fn_openUrl");
	}


	public void fn_emailId(String locator, String email) throws InterruptedException 
	{
		WebElement emailid = driver.findElement(By.xpath(locator));
		Thread.sleep(3000);
		emailid.sendKeys(email);
		System.out.println("Pass Email/userID");
	}

	public void fn_password(String locator, String pss) throws InterruptedException
	{
		WebElement passwrd = driver.findElement(By.xpath(locator));
		Thread.sleep(3000);
		passwrd.sendKeys(pss);
		System.out.println("Pass Password");
	}

	public void fn_login(String string) throws InterruptedException {
		WebElement login = driver.findElement(By.xpath(string));
		login.click();
		Thread.sleep(3000);
		System.out.println("Logged In");
	}

	public void fn_clickProduct(String linkText) throws InterruptedException {
		WebElement clickProduct = driver.findElement(By.cssSelector(linkText));
		Thread.sleep(2000);
		clickProduct.click();
		System.out.println("Clicked TwoWheelerLoan");
	}
	public void fn_clickMiddleProduct(String linkText) throws InterruptedException {
		WebElement clickProduct = driver.findElement(By.cssSelector(linkText));
		Thread.sleep(2000);
		clickProduct.click();
		System.out.println("fn_clickMiddleProduct");
	}

	public void fn_selectTwoWheelerLoan(String hinduja_loan) throws InterruptedException {
		WebElement selectTwoWheelerLoan = driver.findElement(By.cssSelector(hinduja_loan));
		Thread.sleep(3000);
		selectTwoWheelerLoan.click();
		System.out.println("fn_selectTwoWheelerLoan");
	}

	public void fn_clickEligibility(String location) throws InterruptedException {
		WebElement clickEligibility = driver.findElement(By.cssSelector(location));
		Thread.sleep(1000);
		clickEligibility.click();
		System.out.println("fn_clickEligibility");
	}

	public void fn_completeFirstStepMobile(String locator, String Mobile) {
		WebElement completeFirstStepMobile = driver.findElement(By.cssSelector(locator));
		completeFirstStepMobile.sendKeys(Mobile);
		System.out.println("fn_completeFirstStepMobile");
	}

	public void fn_completeFirstStepEmail(String locator, String Email) {
		WebElement completeFirstStepEmail = driver.findElement(By.cssSelector(locator));
		completeFirstStepEmail.sendKeys(Email);
		System.out.println("fn_completeFirstStepEmail");
	}

	public void fn_completeFirstStepContinue(String click) {
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement completeFirstStepContinue = driver.findElement(By.xpath(click));
		completeFirstStepContinue.click();
		System.out.println("fn_completeFirstStepContinue");
	}

	public void fn_completeSecondStepFullName(String locator, String pass) throws InterruptedException {
		Thread.sleep(2000);
		WebElement completeSecondStepFullName = driver.findElement(By.cssSelector(locator));
		completeSecondStepFullName.sendKeys(pass);
		System.out.println("fn_completeSecondStepFullName");
	}

	public void fn_stepTwoSelectGenderId(String locator, String gender) throws InterruptedException {
		
		Select drp0 = new Select(driver.findElement(By.cssSelector(locator)));
		drp0.selectByVisibleText(gender);
		System.out.println("fn_stepTwoSelectGenderId");
	}

	public void fn_stepTwoSelectDOB(String locator, String pss) {
		WebElement stepTwoSelectDOB = driver.findElement(By.cssSelector(locator));
		stepTwoSelectDOB.sendKeys(pss);
		System.out.println("fn_stepTwoSelectDOB");
	}

	public void fn_stepTwoSelectMarital(String locator, String Marital) {
		Select drp1 = new Select(driver.findElement(By.cssSelector(locator)));
		drp1.selectByVisibleText(Marital);
		System.out.println("fn_stepTwoSelectMarital");
	}

	public void fn_stepTwoSelectNationality(String locator, String Nationality) {
		Select drp2 = new Select(driver.findElement(By.cssSelector(locator)));
		drp2.selectByVisibleText(Nationality);
		System.out.println("fn_stepTwoSelectNationality");
	}

	public void fn_completeSecondStagePan(String locator, String pann) {
		WebElement completeSecondStagePan = driver.findElement(By.cssSelector(locator));
		completeSecondStagePan.sendKeys(pann);
		System.out.println("fn_completeSecondStagePan");
	}

	public void fn_stepTwoSelectAddress_Line_1(String locator, String pss) {
		WebElement stepTwoSelectAddress_Line_1 = driver.findElement(By.cssSelector(locator));
		stepTwoSelectAddress_Line_1.sendKeys(pss);
		System.out.println("fn_stepTwoSelectAddress_Line_1");
	}

	public void fn_stepTwoSelectAddress_Line_2(String locator, String pss) {
		WebElement stepTwoSelectAddress_Line_2 = driver.findElement(By.cssSelector(locator));
		stepTwoSelectAddress_Line_2.sendKeys(pss);
		System.out.println("fn_stepTwoSelectAddress_Line_2");
	}

	public void fn_stepTwoSelectCity(String locator, String Owned) {
		Select drp3 = new Select(driver.findElement(By.cssSelector(locator)));
		drp3.selectByVisibleText(Owned);
		System.out.println("fn_stepTwoSelectCity");
	}

	public void fn_stepTwoSelectPincode(String locator, String pss) {
		WebElement stepTwoSelectPincode = driver.findElement(By.cssSelector(locator));
		stepTwoSelectPincode.sendKeys(pss);
		System.out.println("fn_stepTwoSelectPincode");
	}

	public void fn_stepTwoSelectYears_at_current_residence(String locator, String ps) {
		WebElement stepTwoSelectPincode = driver.findElement(By.cssSelector(locator));
		stepTwoSelectPincode.sendKeys(ps);
		System.out.println("fn_stepTwoSelectYears_at_current_residence");
	}

	public void fn_stepTwoSelectType_of_Accommodation(String locator, String Owned) {
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Owned);
		System.out.println("fn_stepTwoSelectType_of_Accommodation");
	}

	public void fn_stepTwoSelectContinue(String locator) {
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement stepTwoSelectPincode = driver.findElement(By.xpath(locator));
		stepTwoSelectPincode.click();
		System.out.println("fn_stepTwoSelectContinue");
	}

	public void fn_completeThirdStageLoan_Amount_Required(String locator, String ps) throws InterruptedException {
		Thread.sleep(3000);
		WebElement completeThirdStageLoan_Amount_Required = driver.findElement(By.cssSelector(locator));
		completeThirdStageLoan_Amount_Required.clear();
		completeThirdStageLoan_Amount_Required.sendKeys(ps);
		System.out.println("fn_completeThirdStageLoan_Amount_Required");
	}

	public void fn_completeThirdStageTenure(String locator, String ps) throws InterruptedException {
		Thread.sleep(3000);
		WebElement completeThirdStageTenures = driver.findElement(By.xpath(locator));
		completeThirdStageTenures.clear();
		completeThirdStageTenures.sendKeys(ps);
		System.out.println("fn_completeThirdStageTenure");
	}

	public void fn_completeThirdStageContinue(String locator) throws InterruptedException {
		fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(4000);
		WebElement completeThirdStageContinue = driver.findElement(By.xpath(locator));
		completeThirdStageContinue.click();
		System.out.println("fn_completeThirdStageContinue");
	}

	public void fn_completeFourthStageOccupation(String locator, String Occupation) throws InterruptedException {
		Thread.sleep(2000);
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Occupation);
		System.out.println("fn_completeFourthStageOccupation");
	}
	public void fn_completeFourthStageCompany_Name(String locator, String ps) throws InterruptedException {
		
		Thread.sleep(2000);
		WebElement completeFourthStageCompany_Name = driver.findElement(By.xpath(locator));
		completeFourthStageCompany_Name.clear();
		Thread.sleep(1000);
		completeFourthStageCompany_Name.sendKeys(ps);
		System.out.println("fn_completeFourthStageCompany_Name");
	}

	public void fn_completeFourthStageGross_Monthly_Income(String locator, String ps) {

		WebElement completeFourthStageGross_Monthly_Income = driver.findElement(By.xpath(locator));
		completeFourthStageGross_Monthly_Income.clear();
		completeFourthStageGross_Monthly_Income.sendKeys(ps);
		System.out.println("fn_completeFourthStageGross_Monthly_Income");
	}

	public void fn_completeFourthStageType_of_Company(String locator, String Owned) {
		Select drp5 = new Select(driver.findElement(By.xpath(locator)));
		drp5.selectByVisibleText(Owned);
		System.out.println("fn_completeFourthStageType_of_Company");
	}

	public void fn_completeFourthStageProfession_Type(String locator, String Owned) {
		Select drp5 = new Select(driver.findElement(By.xpath(locator)));
		drp5.selectByVisibleText(Owned);
		System.out.println("fn_completeFourthStageProfession_Type");
	}

	public void fn_completeFourthStageDo_you_have_income_proof_document(String locator, String Owned) {
		Select drp10 = new Select(driver.findElement(By.xpath(locator)));
		drp10.selectByVisibleText(Owned);
		System.out.println("fn_completeFourthStageDo_you_have_income_proof_document");
	}

	public void fn_completeFourthStageNumber_of_Years_in_Current_Work(String locator, String work) {
		WebElement completeFourthStageNumber_of_Years_in_Current_Work = driver.findElement(By.xpath(locator));
		completeFourthStageNumber_of_Years_in_Current_Work.sendKeys(work);
		System.out.println("fn_completeFourthStageNumber_of_Years_in_Current_Work");
	}

	public void fn_completeFourthStageTotal_Number_of_Years_in_Work(String locator, String TotalWork) {
		WebElement completeFourthStageTotal_Number_of_Years_in_Work = driver.findElement(By.xpath(locator));
		completeFourthStageTotal_Number_of_Years_in_Work.sendKeys(TotalWork);
		System.out.println("fn_completeFourthStageTotal_Number_of_Years_in_Work");
	}

	public void fn_completeFourthStageContinue(String locator) throws InterruptedException {
		fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(1000);
		WebElement completeFourthStageContinue = driver.findElement(By.xpath(locator));
		completeFourthStageContinue.click();
		System.out.println("fn_completeFourthStageContinue");
	}

	public void fn_completeFifthStageAny_Existing_Loan_with_Bank(String locator, String No) throws InterruptedException {
		Thread.sleep(3000);
		Select drp6 = new Select(driver.findElement(By.cssSelector(locator)));
		drp6.selectByVisibleText(No);
		System.out.println("fn_completeFifthStageAny_Existing_Loan_with_Bank");
	}

	public void fn_completeFifthStageContinue(String locator) throws InterruptedException {
		Thread.sleep(1000);
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement completeFourthStageContinue = driver.findElement(By.xpath(locator));
		completeFourthStageContinue.click();
		System.out.println("fn_completeFifthStageContinue");
	}

	public void fn_completeSixthStagePrimary_Existing_Bank_Name(String locator, String Bank_name) throws InterruptedException {
		Thread.sleep(1000);
		Select drp8 = new Select(driver.findElement(By.cssSelector(locator)));
		drp8.selectByVisibleText(Bank_name);
		System.out.println("fn_completeSixthStagePrimary_Existing_Bank_Name");
	}

	public void fn_completeSixthStageBanking_Since(String locator, String year) throws InterruptedException {
		Thread.sleep(10000);
		WebElement completeSixthStagePrimary_Existing_Bank_Name = driver.findElement(By.xpath(locator));
		completeSixthStagePrimary_Existing_Bank_Name.sendKeys(year);
	}

	public void fn_completeSixthStageContinue(String locator) throws InterruptedException {
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement completeFourthStageContinue = driver.findElement(By.xpath(locator));
		completeFourthStageContinue.click();
		System.out.println("fn_completeSixthStageContinue");
	}

	public void fn_completeSeventhStageManufacturer(String locator, String pss) throws InterruptedException {
		Thread.sleep(2000);
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(pss);
		System.out.println("fn_completeSeventhStageManufacturer");
	}

	public void fn_completeSeventhStageModel(String locator, String pss) throws InterruptedException {
		Thread.sleep(2000);

		Select drp20 = new Select(driver.findElement(By.cssSelector(locator)));
		drp20.selectByVisibleText(pss);
		System.out.println("fn_completeSeventhStageModel");
	}

	public void fn_completeSeventhStageEx_showroom_Price(String locator, String pss) throws InterruptedException {
		Thread.sleep(2000);

		WebElement Ex_showroom_Price = driver.findElement(By.cssSelector(locator));
		Ex_showroom_Price.sendKeys(pss);
		System.out.println("fn_completeSeventhStageEx_showroom_Price");
	}

	public void fn_completeSeventhStageSubmit(String locator) throws InterruptedException {
		Thread.sleep(1000);

		WebElement completeFinalStage = driver.findElement(By.cssSelector(locator));
		completeFinalStage.click();
		System.out.println("fn_completeSeventhStageSubmit");
	}

	
	
	/*
	Middle level functions

	*/
	public void fn_completeFourthStageProfession_TypeM(String locator, String Owned) {
		Select drp5 = new Select(driver.findElement(By.xpath(locator)));
		drp5.selectByVisibleText(Owned);
		System.out.println("fn_completeFourthStageProfession_TypeM");
	}
	public void fn_completeSixthStageBanking_SinceM(String locator, String year) throws InterruptedException {
		WebElement completeSixthStagePrimary_Existing_Bank_Name = driver.findElement(By.cssSelector(locator));
		completeSixthStagePrimary_Existing_Bank_Name.sendKeys(year);
		System.out.println("fn_completeSixthStageBanking_SinceM");
	}
	public void fn_completeFourthStageTotal_Number_of_Years_in_WorkM(String locator, String TotalWork) {
		WebElement completeFourthStageTotal_Number_of_Years_in_Work = driver.findElement(By.xpath(locator));
		completeFourthStageTotal_Number_of_Years_in_Work.sendKeys(TotalWork);
		System.out.println("fn_completeFourthStageTotal_Number_of_Years_in_WorkM");
	}
	
	public void fn_completeFourthStageGross_Monthly_IncomeM(String locator, String ps) {

		WebElement completeFourthStageGross_Monthly_Income = driver.findElement(By.xpath(locator));
		completeFourthStageGross_Monthly_Income.clear();
		completeFourthStageGross_Monthly_Income.sendKeys(ps);
		System.out.println("fn_completeFourthStageGross_Monthly_IncomeM");
	}
	public void fn_completeThirdStageTenureM(String locator, String ps) {
		WebElement completeThirdStageTenure = driver.findElement(By.cssSelector(locator));
		completeThirdStageTenure.clear();
		completeThirdStageTenure.sendKeys(ps);
		System.out.println("fn_completeThirdStageTenureM");
	}

	public void fn_homeLoginClick(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator))); 
		driver.findElement(By.xpath(locator)).click();
		System.out.println("fn_homeLoginClick");
	}

	public void fn_selectFirstStageLoanAmount(String locator, String amount) throws InterruptedException {
        Thread.sleep(1000);
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.clear();
		login.sendKeys(amount);
		System.out.println("fn_selectFirstStageLoanAmount");

	}

	public void fn_selectFirstStageEmploymentType(String locator) throws InterruptedException {
        Thread.sleep(1000);
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.click();
		System.out.println("fn_selectFirstStageEmploymentType");

	}
	public void fn_selectFirstStageCompanyOrganization(String locator, String companyName) throws InterruptedException {
        Thread.sleep(1000);
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.sendKeys(companyName);
		System.out.println("fn_selectFirstStageCompanyOrganization");
	}

	public void fn_selectFirstStageMonthlIncome(String locator, String companyName) {

		WebElement login = driver.findElement(By.cssSelector(locator));
		login.sendKeys(companyName);
		System.out.println("fn_selectFirstStageMonthlIncome");
	}

	public void fn_selectFirstStageMonthlyIncome(String locator, String income) {

		WebElement login = driver.findElement(By.cssSelector(locator));
		login.sendKeys(income);
		System.out.println("fn_selectFirstStageMonthlyIncome");
	}

	public void fn_selectFirstStageTenure(String locator, String years) {

		WebElement Tenure = driver.findElement(By.cssSelector(locator));
		Tenure.sendKeys(years);
		System.out.println("fn_selectFirstStageTenure");
	}

	public void fn_selectFirstStageNext(String locator) throws InterruptedException {
		WebElement next = driver.findElement(By.cssSelector(locator));
		next.click();
		Thread.sleep(1000);
		System.out.println("fn_selectFirstStageNext");
	}

	public void fn_selectFirstStageProductMatch(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		driver.findElement(By.cssSelector(locator)).click();
		System.out.println("fn_selectFirstStageProductMatch");
	}

	public void fn_selectSecondStagemobile(String locator, String mobile) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		driver.findElement(By.cssSelector(locator));

		WebElement Tenure = driver.findElement(By.cssSelector(locator));
		Tenure.clear();
		Tenure.sendKeys(mobile);
		System.out.println("fn_selectSecondStagemobile");
	}

	public void fn_selectSecondStageEmailId(String locator, String email) {

		WebElement emailid = driver.findElement(By.cssSelector(locator));
		emailid.clear();
		emailid.sendKeys(email);
		System.out.println("fn_selectSecondStageEmailId");
	}

	public void fn_selectSecondStageNext(String locator) {

		WebElement next = driver.findElement(By.xpath(locator));
		next.click();
		System.out.println("fn_selectSecondStageNext");
	}

	public void fn_completeFinalStage(String locator) {
		WebElement completeFinalStage = driver.findElement(By.cssSelector(locator));
		completeFinalStage.click();
		System.out.println("fn_completeFinalStage");
	}
	public void fn_completeFourthStageDo_you_have_income_proof_documentM(String locator, String Owned) throws InterruptedException {
		Thread.sleep(1000);
		Select drp10 = new Select(driver.findElement(By.xpath(locator)));
		drp10.selectByVisibleText(Owned);
		System.out.println("fn_completeFourthStageDo_you_have_income_proof_documentM");
	}
	public void fn_completeFourthStageNumber_of_Years_in_Current_WorkM(String locator, String work) {
		WebElement completeFourthStageNumber_of_Years_in_Current_Work = driver.findElement(By.xpath(locator));
		completeFourthStageNumber_of_Years_in_Current_Work.sendKeys(work);
		System.out.println("fn_completeFourthStageNumber_of_Years_in_Current_WorkM");
	}
	public void fn_completeThirdStageLoan_Amount_RequiredM(String locator, String ps) throws InterruptedException {
		Thread.sleep(2000);
		WebElement completeThirdStageLoan_Amount_Required = driver.findElement(By.cssSelector(locator));
		completeThirdStageLoan_Amount_Required.clear();
		completeThirdStageLoan_Amount_Required.sendKeys(ps);
		System.out.println("fn_completeThirdStageLoan_Amount_RequiredM");
	}
		
	public void fn_completeStageCoApplicantInformation(String locator, String applicant) throws InterruptedException {
		Thread.sleep(1000);
		Select drp10 = new Select(driver.findElement(By.cssSelector(locator)));
		drp10.selectByVisibleText(applicant);
		System.out.println("fn_completeStageCoApplicantInformation");
		}
	
	public void fn_completeStageCoApplicantInformationNext(String locator) throws InterruptedException{
		Thread.sleep(1000);
		WebElement next = driver.findElement(By.xpath(locator));
		next.click();
		System.out.println("fn_completeStageCoApplicantInformationNext");
	}
	
	/*
	 * HeaderFlow level function  
	*/
	public void fn_completeFourthStageDo_you_have_income_proof_documentHD(String locator, String Owned) {
		Select drp10 = new Select(driver.findElement(By.cssSelector(locator)));
		drp10.selectByVisibleText(Owned);
		System.out.println("fn_completeFourthStageDo_you_have_income_proof_documentHD");
	}
	public void fn_completeFourthStageProfession_TypeHD(String locator, String Owned) {
		Select drp5 = new Select(driver.findElement(By.xpath(locator)));
		drp5.selectByVisibleText(Owned);
		System.out.println("fn_completeFourthStageProfession_TypeHD");
	}
	public void fn_Product(String locator) throws InterruptedException{
		Thread.sleep(5000);
		WebElement subMenu = driver.findElement(By.cssSelector(locator));
		subMenu.click();
		System.out.println("fn_Product");
		}
	
	public void fn_ProductTWLheader(String carloan) throws InterruptedException{
		Thread.sleep(1000);
		WebElement subMenu = driver.findElement(By.cssSelector(carloan));
		Actions act =new Actions(driver);
	    act.moveToElement(subMenu);
	    act.moveToElement(subMenu).build().perform();
	    System.out.println("fn_ProductTWLheader");
		}
	
	public void fn_ProductCarloanHDFCBank(String carloan) throws InterruptedException{
		Thread.sleep(1000);
			WebElement subMenu = driver.findElement(By.cssSelector(carloan));
			subMenu.click(); 
			System.out.println("fn_ProductCarloanHDFCBank");
			}
	public void fn_AddCoapplicant(String locator, String Owned) throws InterruptedException {
		Thread.sleep(1000);
		Select drp5 = new Select(driver.findElement(By.xpath(locator)));
		drp5.selectByVisibleText(Owned);
		System.out.println("fn_AddCoapplicant");
}
	public void fn_RelationwithPrimaryApplicant(String locator, String Owned) throws InterruptedException {
		Thread.sleep(1000);
		Select drp5 = new Select(driver.findElement(By.xpath(locator)));
		drp5.selectByVisibleText(Owned);
		System.out.println("fn_RelationwithPrimaryApplicant");
}
	public void fn_CoapplicantsMonthlyincome(String locator, String Owned) {
		WebElement subMenu = driver.findElement(By.xpath(locator));
		subMenu.sendKeys(Owned);
		System.out.println("fn_CoapplicantsMonthlyincome");
}
	}