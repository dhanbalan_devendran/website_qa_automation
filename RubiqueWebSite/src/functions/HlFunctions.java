package functions;

/** Created by Sangeetha Shetty on 2017-06-20 D:\testingFrameWork_20170626\testqaframework1\RubiqueWebSite
C:\Users\sangeetha.shetty\Desktop\WORKSPACE\Rubique Website SS\src\testDataSS : locator
**/

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class HlFunctions {
	
   public static WebDriver cd ;
   static FileInputStream fs ;
   static XSSFWorkbook wb ;
   static XSSFSheet s1 , s2 , s3 , s4 ;
   static String workingDir = System.getProperty("user.dir");
    public static WebDriver driver;
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	public static String screenSnapsPath = workingDir +"/outputFiles/"+"hl"+timeStamp+".png";
	
  
	public  static  void fn_takeScreenshot(WebDriver driver, String path) throws InterruptedException {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(path));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("fn_takeScreenshot taken check at: "+path);
		Thread.sleep(5000);
	}
	
	public static WebDriver fn_setupChromeDriver() 	{   
		System.setProperty("webdriver.chrome.driver",workingDir+"/utility/chromedriver.exe");
		cd = new ChromeDriver();
		cd.manage().window().maximize();
		cd.manage().deleteAllCookies();
		System.out.println("fn_setUpChrome");
		return cd;
	}

		public static  void fn_openWebsite(String url) throws Exception {
		fn_setupChromeDriver();
		cd.navigate().to(url);
		Thread.sleep(2000);
		System.out.println("fn_openWebsite");
	}
	
	public static void fn_Home(String locator ) throws InterruptedException {
		Thread.sleep(2000);
		WebElement Home = cd.findElement(By.cssSelector(locator));
		// WebElement Home = cd.findElement(By.xpath(locator));
		Home.click();
		Thread.sleep(2000);
		System.out.println(cd.getCurrentUrl());
	    System.out.println("fn_Home");
	    }
	
	
	public static  void fn_refresh() throws Exception {
		cd.navigate().refresh();
		Thread.sleep(2000);
		
	}
	
	public static  void fn_closePopup(String locator) throws Exception {
		WebElement closePopup = cd.findElement(By.xpath(locator));
        if((closePopup).isDisplayed()){
		closePopup.click(); }
		Thread.sleep(2000);
		System.out.println("fn_closePopup");
	}
	
	public static void fn_login(String locator) {
		WebElement login = cd.findElement(By.cssSelector(locator));
		login.click();
				}
	
	public static void fn_loginUsername(String locator , String Username) throws InterruptedException {
		Thread.sleep(2000);
		WebElement loginUsername = cd.findElement(By.cssSelector(locator));
		loginUsername.sendKeys(Username);
			}
	
	public static void fn_loginPassword(String locator , String Password) throws InterruptedException {
		Thread.sleep(2000);
		WebElement loginPassword = cd.findElement(By.cssSelector(locator));
		loginPassword.sendKeys(Password);
		Thread.sleep(2000);
			}
	
	public static void fn_loginClick(String locator) throws InterruptedException {
		WebElement loginClick = cd.findElement(By.cssSelector(locator));
		loginClick.click();
		System.out.println(cd.getCurrentUrl());
		Thread.sleep(5000);
		System.out.println("fn_loginClick");
		}
	
	public void fn_headerhlflowclick(String mainProd, String subProd, String prod) throws InterruptedException {
		
		System.out.println(mainProd + subProd + prod);
		// Default set the locator
		if (mainProd.equals(null)) {
			System.out.println(" mainProd is null ");
			mainProd = "Products";
		}
		if (subProd.equals(null)) {
			System.out.println(" subProd is null ");
			subProd = "Consumer Loans";
		}
		if (prod.equals(null)) {
			System.out.println(" subProd is null ");
			prod = "Aditya Birla Finance Limited";
		}
		
		// step 1
		Actions actionProduct = new Actions(cd);
		WebElement c1 = cd.findElement(By.partialLinkText(mainProd));
		if (c1.isDisplayed()) {
			System.out.println("Element1 is Visible");
			actionProduct.clickAndHold(c1).perform();
			Thread.sleep(4000);
		} else {
			System.out.println("Element1 is InVisible");
		}

		// step 2
		WebElement c2 = cd.findElement(By.linkText(subProd));
		if (c2.isDisplayed()) {
			System.out.println("Element2 is Visible");
			actionProduct.clickAndHold(c2).perform();
			Thread.sleep(5000);
		} else {
			System.out.println("Element2 is InVisible");
		}
		// step 3
		WebElement c3 = cd.findElement(By.partialLinkText(prod));
		if (c3.isDisplayed()) {
			System.out.println("Element3 is Visible");
			actionProduct.click(c3).perform();
			Thread.sleep(4000);
		} else {
			System.out.println("Element3 is InVisible");
		}

		System.out.println("fn_mainDropDownProduct");
	}
	
	public static void fn_middlehlflowclick(String locator) {
	WebElement middleIcon = cd.findElement(By.cssSelector(locator));
	 middleIcon.click();
	 System.out.println(cd.getCurrentUrl());
	 System.out.println("fn_middlehlflowclick"+cd.getCurrentUrl());
	}
	
	public static void fn_footerhlflowclick(String locator) {
	WebElement footerlink = cd.findElement(By.cssSelector(locator));
	footerlink.click();
	 System.out.println(cd.getCurrentUrl());
	 System.out.println("fn_footerhlflowclick");
	}
	
	public static void fn_goToTop(String locator) throws InterruptedException  {
		cd.findElement(By.cssSelector(locator)).click();
		Thread.sleep(2000);
		
	}
	
	public static void fn_Continue(String locator) throws InterruptedException  {
		fn_takeScreenshot(driver, screenSnapsPath);
		cd.findElement(By.linkText(locator)).click();
		Thread.sleep(2000);
		System.out.println("fn_Continue");
	}
	
	
	public static void fn_stepone_Continue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		cd.findElement(By.cssSelector(locator)).click();
		Thread.sleep(2000);
		System.out.println("fn_stepone_Continue");
	}
	
	public static void fn_steptwo_Continue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(5000);
		cd.findElement(By.xpath(locator)).click();
		
		System.out.println("fn_steptwo_Continue");
	}
	
	public static void fn_stepthree_Continue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(5000);
		cd.findElement(By.xpath(locator)).click();
		System.out.println("fn_stepthree_Continue");
	}
	
	public static void fn_stepthree_Continue_Middle(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(5000);
		cd.findElement(By.xpath(locator)).click();
		System.out.println("fn_stepthree_Continue_Middle");
	}
	
	public static void fn_stepfour_Continue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(5000);
		cd.findElement(By.xpath(locator)).click();
		Thread.sleep(2000);
		System.out.println("fn_stepfour_Continue");
	}
	
	public static void fn_stepfive_Continue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(5000);
		cd.findElement(By.xpath(locator)).click();
		
		System.out.println("fn_stepfive_Continue");
	}
	
	public static void fn_stepsix_Continue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(5000);
		cd.findElement(By.xpath(locator)).click();
		
		System.out.println("fn_stepsix_Continue");
	}
	
	public static void fn_stepseven_Continue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(5000);
		cd.findElement(By.xpath(locator)).click();
		
		System.out.println("fn_stepsix_Continue");
	}
	
	public static void fn_Previous(String locator) throws InterruptedException  {
		cd.findElement(By.cssSelector(locator)).click();
		Thread.sleep(2000);
		System.out.println("fn_stepsix_Continue");
	}
	
	public static void fn_mBILoanAmount(String locator , String LoanAmount) throws InterruptedException {
		Thread.sleep(2000);
		WebElement mBILoanAmount = cd.findElement(By.cssSelector(locator));
		mBILoanAmount.sendKeys(LoanAmount);
		System.out.println("fn_mBILoanAmount");
	}
	// not working	
	public static void fn_mBILoanAmountinwords(String locator) throws InterruptedException {
			WebElement mBILoanAmountinwords = cd.findElement(By.cssSelector(locator));
			System.out.println(mBILoanAmountinwords.getText());
			System.out.println("");
			 	}
	// not working
	public static void fn_mBIEmploymentType(String locator) throws InterruptedException {
		Thread.sleep(2000);
		WebElement mBIemploymentType = cd.findElement(By.cssSelector(locator));
		mBIemploymentType.click();
		System.out.println("");
		 	}
	
	public static void fn_mBICompany(String locator,String cmpnyname) throws InterruptedException {
		WebElement company = cd.findElement(By.cssSelector(locator));
		company.sendKeys(cmpnyname);
		System.out.println("");
		 	}
	
	public static void fn_mBIMonthlyIncome(String locator,String incomeamount) throws InterruptedException {
		WebElement monthlyIncome = cd.findElement(By.cssSelector(locator));
		monthlyIncome.sendKeys(incomeamount);
		System.out.println("");
		 	}
	// not working
	public static void fn_mBIMonthlyIncomeinwords(String locator) throws InterruptedException {
		WebElement monthlyIncomeinwords = cd.findElement(By.cssSelector(locator));
		System.out.println(monthlyIncomeinwords.getText());
		System.out.println("");
		}
	
	public static void fn_mBITenureyears(String locator,String years) throws InterruptedException {
		WebElement monthlyIncome = cd.findElement(By.cssSelector(locator));
		monthlyIncome.sendKeys(years);
		System.out.println("");
		}
	
	public static void fn_mBITenuremonths(String locator,String months) throws InterruptedException {
		WebElement monthlyIncome = cd.findElement(By.cssSelector(locator));
		monthlyIncome.sendKeys(months);
		System.out.println(""); 	}

	public static void fn_mBINext(String locator) throws InterruptedException {
		WebElement mbinext = cd.findElement(By.cssSelector(locator));
		mbinext.click();
		Thread.sleep(2000);
		System.out.println("");
	}
	
	public static void fn_apply(String locator) throws InterruptedException {
		Thread.sleep(5000);
		WebElement apply = cd.findElement(By.cssSelector(locator));
		apply.click();
		Thread.sleep(1000);
		System.out.println("");	}
	
	public static void fn_middleflowBasicInfo() throws InterruptedException {
	
	fn_middlehlflowclick("img[src='/assets/images/websitev2/hl-icon.svg']");
	fn_mBILoanAmount("input[id='visible-loan-amount']","5000000");
	fn_mBILoanAmountinwords("p[id='home-english-number']");
	//fn_mBIEmploymentType("input[id='option4'][class='value-field']");
	//fn_mBIEmploymentType("//*[@id=employment-type-group]/div[1]/div/div/label[4]");
	//fn_mBIEmploymentType("input[class='value-field'][id='option2']");
	//fn_mBIEmploymentType("//*[@id=employment-type-group]/div[1]/div/div/label[2]");
	fn_mBICompany("input[id='current-company'][name='40']","Google");
	fn_mBIMonthlyIncome("input[id='visible-monthly-income']","200000");
	fn_mBIMonthlyIncomeinwords("p[id='onboard-english-number']");
	fn_mBITenureyears("input[class='form-control fieldNumber'][name='years']","10");
	fn_mBITenuremonths("input[class='form-control fieldNumber'][name='months']","2");
	fn_mBINext("button[class='btn btn-next nxt-btn '][id= 'search-step-one-submit-button']");
	Thread.sleep(2000);
}
	
public static void fn_olGridView(String locator) throws InterruptedException {
	WebElement olGridView = cd.findElement(By.cssSelector(locator));
	olGridView.click();
	Thread.sleep(2000);
}

public static void fn_olListView(String locator) throws InterruptedException {
	WebElement olListView = cd.findElement(By.cssSelector(locator));
	olListView.click();
	Thread.sleep(2000);
	System.out.println(""); }

public static void fn_olViewDetails(String locator) throws InterruptedException {
	WebElement olViewDetails = cd.findElement(By.linkText(locator));
	olViewDetails.click();
	Thread.sleep(2000);
	System.out.println("");
	}

public static void fn_olOfferCount(String locator) throws InterruptedException {
	WebElement olOfferCount = cd.findElement(By.cssSelector(locator));
	String oc = olOfferCount.getText();
	System.out.println(oc);
	Thread.sleep(2000);
	System.out.println("");}

public static void fn_olapply(String locator) throws InterruptedException {
	
	/* WebElement olapply = cd.findElement(By.xpath(locator));
	olapply.click();*/
	cd.navigate().to("https://beta.rubique.com/search/home-loan");
	Thread.sleep(2000);
	System.out.println("");}

public static void fn_viewDetails(String locator) throws InterruptedException {
	WebElement viewDetails = cd.findElement(By.cssSelector(locator));
	viewDetails.click();
		Thread.sleep(2000);
		System.out.println("");}

public static void fn_checkEligibility(String locator) throws InterruptedException {
	
	WebElement checkEligibility = cd.findElement(By.cssSelector(locator));
	checkEligibility.click();
		Thread.sleep(2000);
		System.out.println("");}

public static void fn_offerList() throws InterruptedException {
	fn_olGridView("i[class='fa fa-th GridView visible-md visible-lg']");
	fn_olListView("i[class='fa fa-bars ListView visible-md visible-lg']");
	fn_olViewDetails("View Details");
	fn_olViewDetails("View Details");
	fn_olOfferCount("span[class='offerNumber']");
	fn_olapply("//*[@id=user-onboarding]/div/div/div[1]/div/div/fieldset[2]/div[4]/div[1]/div/table/tbody/tr[1]/td[5]/div[1]/button/span");
	
}

public static void fn_steponeMobile(String locator , String mobile) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steponeMobile = cd.findElement(By.cssSelector(locator));
	steponeMobile.sendKeys(mobile);
	System.out.println("");}

public static void fn_steponeEmailId(String locator , String EmailId) throws InterruptedException {
	WebElement steponeEmail_Id = cd.findElement(By.cssSelector(locator));
	steponeEmail_Id.sendKeys(EmailId);
	System.out.println("");}

public static void fn_steponeContinue(String locator) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steponeContinue = cd.findElement(By.cssSelector(locator));
	steponeContinue.click();
	System.out.println("");}

public static void fn_steptwo_FullName(String locator,String FullName) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_FullName = cd.findElement(By.cssSelector(locator));
	// WebElement steptwo_FullName = cd.findElement(By.xpath(locator));
	steptwo_FullName.sendKeys(FullName);
	System.out.println("");}

public static void fn_steptwo_Gender(String locator , String Gender) throws InterruptedException {
	Select steptwo_Gender =  new Select(cd.findElement(By.cssSelector(locator)));
	steptwo_Gender.selectByVisibleText(Gender);
	System.out.println("");}


public static void fn_steptwo_dob(String locator , String dob ) throws InterruptedException {
	WebElement steptwo_dob = cd.findElement(By.cssSelector(locator));
	steptwo_dob.sendKeys(dob);
	System.out.println("");}

/* public static void fn_steptwo_dobMonth(String locator , String dobMonth) throws InterruptedException {
	WebElement steptwo_dobMonth = cd.findElement(By.cssSelector(locator));
	steptwo_dobMonth.sendKeys(dobMonth);
	}

public static void fn_steptwo_dobYear(String locator , String dobYear ) throws InterruptedException {
	WebElement steptwo_dobYear = cd.findElement(By.cssSelector(locator));
	steptwo_dobYear.sendKeys(dobYear);
	}

public static void fn_steptwo_dobdate(String locator , String dobdate ) throws InterruptedException {
	WebElement steptwo_dobdate = cd.findElement(By.linkText(locator));
	steptwo_dobdate.sendKeys(dobdate);
	} */

public static void fn_steptwo_MaritalStatus(String locator , String MaritalStatus) throws InterruptedException {
	Select steptwo_MaritalStatus =  new Select(cd.findElement(By.cssSelector(locator)));
	steptwo_MaritalStatus.selectByVisibleText(MaritalStatus);
	System.out.println("");}

public static void fn_steptwo_Nationality(String locator , String Nationality) throws InterruptedException {
	Select steptwo_Nationality =  new Select(cd.findElement(By.cssSelector(locator)));
	steptwo_Nationality.selectByVisibleText(Nationality);
	System.out.println("");}

public static void fn_steptwo_PAN(String locator , String PAN) throws InterruptedException {
	WebElement steptwo_PAN = cd.findElement(By.cssSelector(locator));
	steptwo_PAN.sendKeys(PAN);
	System.out.println("");}

public static void fn_steptwo_PassportNo(String locator , String PassportNo) throws InterruptedException {
	WebElement steptwo_PassportNo = cd.findElement(By.cssSelector(locator));
	steptwo_PassportNo.sendKeys(PassportNo);
	System.out.println("");}

public static void fn_steptwo_AadhaarNo(String locator , String AadhaarNo) throws InterruptedException {
	WebElement steptwo_AadhaarNo = cd.findElement(By.cssSelector(locator));
	steptwo_AadhaarNo.sendKeys(AadhaarNo);
	System.out.println("");}

public static void fn_steptwo_AddressLine1(String locator , String AddressLine1) throws InterruptedException {
	WebElement steptwo_AddressLine1 = cd.findElement(By.cssSelector(locator));
	steptwo_AddressLine1.sendKeys(AddressLine1);
	System.out.println("");}

public static void fn_steptwo_AddressLine2(String locator , String AddressLine2) throws InterruptedException {
	WebElement steptwo_AddressLine2 = cd.findElement(By.cssSelector(locator));
	steptwo_AddressLine2.sendKeys(AddressLine2);
	System.out.println("");}

public static void fn_steptwo_Landmark(String locator , String Landmark) throws InterruptedException {
	WebElement steptwo_Landmark = cd.findElement(By.cssSelector(locator));
	steptwo_Landmark.sendKeys(Landmark);
	System.out.println("");}

public static void fn_City(String locator , String City) throws InterruptedException {
	Thread.sleep(2000);
	Select steptwo_City =  new Select(cd.findElement(By.cssSelector(locator)));
	steptwo_City.selectByVisibleText(City);
	System.out.println("");}


public static void fn_steptwo_Pincode(String locator , String Pincode) throws InterruptedException {
	WebElement steptwo_Pincode = cd.findElement(By.cssSelector(locator));
	steptwo_Pincode.sendKeys(Pincode);
	System.out.println("");}

public static void fn_steptwo_LandlineNumber(String locator , String LandlineNumber) throws InterruptedException {
	WebElement steptwo_LandlineNumber = cd.findElement(By.cssSelector(locator));
	steptwo_LandlineNumber.sendKeys(LandlineNumber);
	System.out.println("");}

public static void fn_steptwo_YearsCurrentResidence(String locator , String YearsCurrentResidence) throws InterruptedException {
	WebElement steptwo_YearsCurrentResidence = cd.findElement(By.cssSelector(locator));
	steptwo_YearsCurrentResidence.sendKeys(YearsCurrentResidence);
	System.out.println("");}

public static void fn_steptwo_TypeofAccommodation(String locator , String TypeofAccommodation) throws InterruptedException {
	Select steptwo_TypeofAccommodation =  new Select(cd.findElement(By.cssSelector(locator)));
	steptwo_TypeofAccommodation.selectByVisibleText(TypeofAccommodation);
	System.out.println("");}

public static void fn_stepthree_LoanAmountRequired(String locator , String LoanAmount) throws InterruptedException {
	Thread.sleep(2000);
	WebElement LoanAmountRequired = cd.findElement(By.cssSelector(locator));
	LoanAmountRequired.sendKeys(LoanAmount);
	System.out.println("");}

public static void fn_stepthree_TenureYears(String locator,String years) throws InterruptedException {
	WebElement TenureYears = cd.findElement(By.cssSelector(locator));
	TenureYears.sendKeys(years);
	System.out.println("");}

public static void fn_stepthree_TenureMonths(String locator,String months) throws InterruptedException {
	WebElement TenureMonths = cd.findElement(By.cssSelector(locator));
	TenureMonths.sendKeys(months);
	System.out.println("");	}


public static void fn_stepfour_Occupation (String locator , String Occupation) throws InterruptedException {
	Thread.sleep(2000);
	Select stepfour_Occupation =  new Select(cd.findElement(By.cssSelector(locator)));
	stepfour_Occupation.selectByVisibleText(Occupation);
	System.out.println("");}

public static void fn_stepfour_CompanyName(String locator , String CompanyName) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_CompanyName = cd.findElement(By.cssSelector(locator));
	stepfour_CompanyName.sendKeys(CompanyName);
	System.out.println("");}

public static void fn_stepfour_GrossMonthlyIncome(String locator , String GrossMonthlyIncome) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_GrossMonthlyIncome = cd.findElement(By.cssSelector(locator));
	stepfour_GrossMonthlyIncome.sendKeys(GrossMonthlyIncome);
	System.out.println("");}


public static void fn_stepfour_ModeofSalary(String locator , String ModeofSalary) throws InterruptedException {
	Thread.sleep(2000);
	Select stepfour_ModeofSalary =  new Select(cd.findElement(By.cssSelector(locator)));
	stepfour_ModeofSalary.selectByVisibleText(ModeofSalary);
	System.out.println("");}

public static void fn_stepfour_TypeofCompany(String locator , String TypeofCompany) throws InterruptedException {
	Select stepfour_TypeofCompany =  new Select(cd.findElement(By.cssSelector(locator)));
	stepfour_TypeofCompany.selectByVisibleText(TypeofCompany);
	System.out.println("");}

public static void fn_stepfour_ProfessionType(String locator , String ProfessionType) throws InterruptedException {
	Select stepfour_ProfessionType =  new Select(cd.findElement(By.cssSelector(locator)));
	stepfour_ProfessionType.selectByVisibleText(ProfessionType);
	System.out.println("");}

public static void fn_stepfour_NumberofYearsinCurrentWork(String locator , String NumberofYearsinCurrentWork) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_NumberofYearsinCurrentWork = cd.findElement(By.cssSelector(locator));
	stepfour_NumberofYearsinCurrentWork.sendKeys(NumberofYearsinCurrentWork);
	System.out.println("");}

public static void fn_stepfour_TotalNumberofYearsinWork(String locator , String TotalNumberofYearsinWork) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_TotalNumberofYearsinWork = cd.findElement(By.cssSelector(locator));
	stepfour_TotalNumberofYearsinWork.sendKeys(TotalNumberofYearsinWork);
	System.out.println("");}

public static void fn_stepfive_AnyExistingLoan(String locator , String AnyExistingLoan) throws InterruptedException {
	Thread.sleep(5000);
	Select stepfive_AnyExistingLoan =  new Select(cd.findElement(By.cssSelector(locator)));
	stepfive_AnyExistingLoan.selectByVisibleText(AnyExistingLoan);
	System.out.println("");	}

public static void fn_stepsix_AddCoapplicant(String locator , String AddCoapplicant) throws InterruptedException {
	Select stepsix_AddCoapplicant =  new Select(cd.findElement(By.cssSelector(locator)));
	stepsix_AddCoapplicant.selectByVisibleText(AddCoapplicant);
	System.out.println("");	}


public static void fn_stepseven_BankAccountType(String locator , String BankAccountType) throws InterruptedException {
	Thread.sleep(3000);
	Select stepseven_BankAccountType =  new Select(cd.findElement(By.cssSelector(locator)));
	stepseven_BankAccountType.selectByVisibleText(BankAccountType);
	System.out.println("");}

public static void fn_stepseven_BankingSinceYears(String locator,String BankingSinceYears) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepseven_BankingSinceYears = cd.findElement(By.xpath(locator));
	stepseven_BankingSinceYears.sendKeys(BankingSinceYears);
	System.out.println("");	}

public static void fn_stepseven_BankingSinceMonths(String locator , String BankingSinceMonths) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepseven_BankingSinceMonths = cd.findElement(By.xpath(locator));
	stepseven_BankingSinceMonths.sendKeys(BankingSinceMonths);
	System.out.println("");}


public static void fn_stepseven_haveCurrentAccountsincelastoneyear(String locator , String haveCurrentAccountsincelastoneyear) throws InterruptedException {
	Select stepseven_haveCurrentAccountsincelastoneyear =  new Select(cd.findElement(By.cssSelector(locator)));
	stepseven_haveCurrentAccountsincelastoneyear.selectByVisibleText(haveCurrentAccountsincelastoneyear);
	System.out.println("");	}

public static void fn_stepseven_AnyChequeBounce(String locator , String AnyChequeBounce) throws InterruptedException {
	Select fn_stepseven_AnyChequeBounce =  new Select(cd.findElement(By.cssSelector(locator)));
	fn_stepseven_AnyChequeBounce.selectByVisibleText(AnyChequeBounce);
	System.out.println("");}

public static void fn_stepeight_TypeofProperty(String locator , String TypeofProperty) throws InterruptedException {
	Select stepeight_TypeofProperty =  new Select(cd.findElement(By.cssSelector(locator)));
	stepeight_TypeofProperty.selectByVisibleText(TypeofProperty);
	System.out.println("");}



public static void fn_stepeight_ClassificationofProperty(String locator , String ClassificationofProperty) throws InterruptedException {
	Select stepeight_ClassificationofProperty =  new Select(cd.findElement(By.xpath(locator)));
	stepeight_ClassificationofProperty.selectByVisibleText(ClassificationofProperty);
	System.out.println("");Thread.sleep(2000);}

public static void fn_stepeight_PurposeofLoan(String locator , String PurposeofLoan) throws InterruptedException {
	Thread.sleep(2000);
	Select stepeight_PurposeofLoan =  new Select(cd.findElement(By.xpath(locator)));
	stepeight_PurposeofLoan.selectByValue(PurposeofLoan);
	System.out.println("");}

public static void fn_stepeight_StatusofProperty(String locator , String StatusofProperty) throws InterruptedException {
	Thread.sleep(2000);
	Select stepeight_StatusofProperty =  new Select(cd.findElement(By.xpath(locator)));
	stepeight_StatusofProperty.selectByValue(StatusofProperty);
	System.out.println("");}

public static void fn_stepeight_PropertyName(String locator , String PropertyName) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepeight_PropertyName = cd.findElement(By.xpath(locator));
	stepeight_PropertyName.sendKeys(PropertyName);
	System.out.println("fn_stepeight_PropertyName");} 


public static void fn_stepeight_BuilderDeveloper(String locator , String BuilderDeveloper) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepeight_BuilderDeveloper = cd.findElement(By.xpath(locator));
	stepeight_BuilderDeveloper.sendKeys(BuilderDeveloper);
	System.out.println("fn_stepeight_BuilderDeveloper");}


public static void fn_stepeight_PropertyCity(String locator , String PropertyCity) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepeight_PropertyCity =  cd.findElement(By.xpath(locator));
	stepeight_PropertyCity.sendKeys(PropertyCity);
	System.out.println("fn_stepeight_PropertyCity");}

public static void fn_stepeight_MarketValueOfProperty(String locator , String MarketValueOfProperty) throws InterruptedException {
	Thread.sleep(2000);WebElement stepeight_MarketValueOfProperty = cd.findElement(By.xpath(locator));
    stepeight_MarketValueOfProperty.sendKeys(MarketValueOfProperty);
    System.out.println("fn_stepeight_MarketValueOfProperty");}

public static void fn_stepeight_Submit(String locator ) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepeight_Submit = cd.findElement(By.xpath(locator));
	stepeight_Submit.click();
    System.out.println("fn_stepeight_Submit");}

public static void fn_TrackingID(String locator ) throws InterruptedException {
	Thread.sleep(2000);
	WebElement TrackingID = cd.findElement(By.xpath(locator));
	TrackingID.getText();
    System.out.println(TrackingID.getText());}

	public static void fn_closeDriver() {
		cd.close();
		cd.quit();
	}
	


}

